# Lemonway module for Dz Framework

This module provides an easy way to integrate with Lemonway REST API

## Installation

Add these lines to composer.json file:

```shell
"require": {
    ...
    "dezero/lemonway": "dev-master"
    ...
},
"repositories":[
    ...
    {
        "type": "vcs",
        "url" : "git@bitbucket.org:dezero/lemonway.git"
    }
    ...
]
```

## Configuration

1) Define the module in the configuration file `/app/config/common/modules.php`
```shell

    // Lemonway module
    'lemonway' => [
        'class' => '\dzlab\lemonway\Module',
        // 'class' => '\lemonway\Module',
    ],
```

2) Add a new alias path in `/app/config/common/aliases.php`
```shell
    'dzlab.lemonway'  => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'dezero' . DIRECTORY_SEPARATOR . 'lemonway',
```

3) Set new component in `/app/config/common/components.php`
```shell
    // Lemonway components
    'lemonway' => [
        'class' => '\dzlab\lemonway\components\LemonwayComponent',
        // 'class' => '\lemonway\components\LemonwayComponent'
    ],
    'lemonwayApi' => [
        'class' => '\dzlab\lemonway\components\LemonwayApi',
        // 'class' => '\lemonway\components\LemonwayApi'
    ],
    'lemonwayManager' => [
        'class' => '\dzlab\lemonway\components\LemonwayManager',
        // 'class' => '\lemonway\components\LemonwayManager'
    ],
```

4) Register a log file for Lemonway module in `/app/config/components/logs.php`
```shell
    // Logs for Lemonway module
    [
        'class' => '\dz\log\FileLogRoute',
        'logFile' => 'lemonway.log',
        'categories' => 'lemonway',
    ],
    [
        'class' => '\dz\log\FileLogRoute',
        'logFile' => 'lemonway_error.log',
        'categories' => 'lemonway_error',
    ],
    [
        'class' => '\dz\log\FileLogRoute',
        'logFile' => 'lemonway_warning.log',
        'categories' => 'lemonway_warning',
    ],
    [
        'class' => '\dz\log\FileLogRoute',
        'logFile' => 'lemonway_dev.log',
        'categories' => 'lemonway_dev',
    ],
```

5) Configurate Mailchimp via environment variables from the `.env` file.
```shell
# The API key for PRODUCTION environment
LEMONWAY_API_KEY="REPLACE_ME"

# The API key for TEST (SANDBOX) environment
LEMONWAY_SANDBOX_API_KEY="REPLACE_ME"
```

6) Copy file `config/lemonway.php` to your project directory `/app/config/components/lemonway.php`. Open the copied file and add your config values, if needed.

7) Optional. If it exists, copy the translation file from `messages/<language_id>/lemonway.php` to `/app/<language_id>/lemonway.php`

8) If you want to receive notifictions from Lemonway Webkit server, you must to allow PUBLIC notifications on /lemonway/webkit/...
To do it, open "\auth\components\DbAuthManager" component and add this line at then end of checkPublicAccess method:
```shell
    // Webkit controller of Lemonway module has public access
    if ( $vec_route['module'] === 'lemonway' && $vec_route['controller'] === 'webkit' )
    {
        return true;
    }
```