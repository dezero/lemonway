<?php
/**
 * Module to integrate with Lemonway for DZ Framework
 */

namespace dzlab\lemonway;

use Yii;

class Module extends \dz\web\Module
{
    /**
     * @var array mapping from controller ID to controller configurations.
     */
    public $controllerMap = [
        'account' => [
            'class' => 'dzlab\lemonway\controllers\AccountController',
        ],
        'document' => [
            'class' => 'dzlab\lemonway\controllers\DocumentController',
        ],
        'iban' => [
            'class' => 'dzlab\lemonway\controllers\IbanController',
        ],
        'transaction' => [
            'class' => 'dzlab\lemonway\controllers\TransactionController',
        ],
        'webkit' => [
            'class' => 'dzlab\lemonway\controllers\WebkitController',
        ],
    ];


    /**
     * Default controller
     */
    public $defaultController = 'account';


    /**
     * Load specific CSS or JS files for this module
     */
    public $cssFiles = ['lemonway.css'];
    public $jsFiles = [
        'lemonway.js',
        'lemonwayAccount.js',
        'lemonwayTransaction.js',
    ];


    /**
     * This method is called when the module is being created
     * you may place code here to customize the module or the application
     */
    public function init()
    {
        // Init this module with current path
        $this->init_module(__DIR__);

        // Going on with the init process
        parent::init();
    }
}