/**
 * Scripts for LEMONWAY module
 */
 (function(document, window, $) {

  // LemonwayAccount grid view - Refresh filters
  // -------------------------------------------------------------------------------------------
  $.lemonwayAccountGridUpdate = function() {
    $('#LemonwayAccount_status_type').select2({
      templateResult: $.lemonwayAccount.select2_format,
      templateSelection: $.lemonwayAccount.select2_format,
      width: '100%'
    });
  };


  // LemonwayTransaction grid view - Refresh filters
  // -------------------------------------------------------------------------------------------
  $.lemonwayTransactionGridUpdate = function() {
    $('#LemonwayTransaction_transaction_type, #LemonwayTransaction_status_type').select2({
      templateResult: $.LemonwayTransaction.select2_format,
      templateSelection: $.LemonwayTransaction.select2_format,
      width: '100%'
    });

    var date_options = {
      autoclose: true,
      clearBtn: true,
      format: 'dd/mm/yyyy',
      language: js_globals.language
    };
    jQuery('#LemonwayTransaction_transaction_date').datepicker(date_options);
  };


  // DOCUMENT READY
  // -------------------------------------------------------------------------------------------
  $(document).ready(function() {
    // Lemonway Account grid - Select2 custom style
    if ( $('#lemonway-account-grid').size() > 0 ) {
      $.lemonwayAccountGridUpdate();
    }

    // Lemonway Transaction grid - Select2 custom style
    if ( $('#lemonway-transaction-grid').size() > 0 ) {
      $.lemonwayTransactionGridUpdate();
    }
  });

})(document, window, jQuery);