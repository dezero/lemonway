(function(document, window, $) {

  // LemonwayAccount global object
  // -------------------------------------------------------------------------------------------  
  $.lemonwayAccount = {

    // LEMONWAY ACCOUNT TYPE - Special function to render format for SELECT2 widget
    // -------------------------------------------------------------------------------------------
    select2_format: function(item) {
      if (!item.id) {
        return $('<span>- All -</span>');
      }

      var que_color = '';
      switch ( item.id ) {
        case 'kyc_confirmed':
          que_color = 'green-800';
        break;

        case 'kyc_pending':
          que_color = 'orange-800';
        break;

        case 'blocked_error':
          que_color = 'red-800';
        break;

        case 'closed':
          que_color = 'grey-500';
        break;
              
        case 'technical':
          que_color = 'blue-800';
        break;
      }

      if ( que_color !== '' ) {
        return $('<span><i class=\'wb-medium-point '+ que_color +'\' aria-hidden=\'true\'></i> '+ item.text +'</span>');
      }

      return $('<span>'+ item.text +' (all)</span>');
    }
  };
})(document, window, jQuery);