(function(document, window, $) {

  // LemonwayTransaction global object
  // -------------------------------------------------------------------------------------------  
  $.LemonwayTransaction = {

    // LEMONWAY TRANSACTION TYPE - Special function to render format for SELECT2 widget
    // -------------------------------------------------------------------------------------------
    select2_format: function(item) {
      if (!item.id) {
        return $('<span>- All -</span>');
      }

      var que_color = '';
      switch ( item.id ) {
        case 'in':
          que_color = 'green-800';
        break;

        case 'p2p':
          que_color = 'orange-800';
        break;

        case 'out':
          que_color = 'red-800';
        break;

        case 'refund':
          que_color = 'blue-800';
        break;
      }

      if ( que_color !== '' ) {
        return $('<span><i class=\'wb-medium-point '+ que_color +'\' aria-hidden=\'true\'></i> '+ item.text +'</span>');
      }

      return $('<span>'+ item.text +'</span>');
    }
  };
})(document, window, jQuery);