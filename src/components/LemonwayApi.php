<?php
/**
 * Lemonway API client
 * 
 * Helper classes to work with Lemonway REST API
 * 
 * @see https://apidoc.lemonway.com/
 */

namespace dzlab\lemonway\components;

// use dz\base\ApplicationComponent;
use dz\rest\RestClient;
use dz\helpers\DateHelper;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\helpers\Url;
use dz\modules\asset\models\AssetFile;
use user\models\User;
use Yii;

class LemonwayApi extends RestClient
{
    /**
     * @var string. Auth URL to create API tokens
     */ 
    public $auth_url;


    /**
     * @var string. Webkit URL where users pay via bank card
     */ 
    public $webkit_url;


    /**
     * @var File
     */
    public $token_file;


    /**
     * @var bool Sanbdox environment
     */
    public $is_sandbox = false;


    /**
     * @var string Last used endpoint
     */
    public $last_endpoint;


    /**
     * @var array Lemonway configuration
     */
    protected $vec_config = [];


    /**
     * Init function
     */
    public function init()
    {
        // Lemonway configuration
        $this->vec_config = Yii::app()->config->get('components.lemonway');

        // Debug mode?
        if ( isset($this->vec_config['is_debug']) )
        {
            $this->is_debug = $this->vec_config['is_debug'];
        }

        // Sandbox environment?
        if ( isset($this->vec_config['is_sandbox']) )
        {
            $this->is_sandbox = $this->vec_config['is_sandbox'];
        }

        if ( $this->is_sandbox )
        {
            // API key for SANDBOX
            $this->api_key = getenv('LEMONWAY_SANDBOX_API_KEY');

            // Auth URL to create API tokens
            $this->auth_url = 'https://sandbox-api.lemonway.fr/oauth/api/v1/oauth/token';

            // Base URL
            $this->base_url = $this->vec_config['sandbox_base_url'];

            // Webkit URL
            $this->webkit_url = $this->vec_config['sandbox_webkit_url'];
        }
        else
        {
            // API key for production
            $this->api_key = getenv('LEMONWAY_API_KEY');

            // Auth URL to create API tokens
            $this->auth_url = 'https://auth.lemonway.com/oauth/api/v1/oauth/token';

            // Base URL
            $this->base_url = $this->vec_config['base_url'];

            // Webkit URL
            $this->webkit_url = $this->vec_config['webkit_url'];
        }

        parent::init();
    }


    /**
     * Custom event before sending HTTP request
     */
    public function beforeSend()
    {
        $this->client->setHeaders([
            'Authorization'     => $this->get_access_token(),
            'PSU-IP-Address'    => Yii::app()->request->getUserHostAddress(),
            'accept'            => 'application/json;charset=UTF-8',
            'Content-Type'      => 'application/json;charset=UTF-8',
        ]);

        return parent::beforeSend();
    }


    /**
     * Custom event after sending HTTP request
     */
    public function afterSend()
    {
        // If some error -> SAVE LOG
        if ( ! $this->is_last_action_success() )
        {
            $this->save_log('lemonway');
        }

        // Register success requests
        else if ( $this->is_debug )
        {
            $this->save_log('lemonway_dev');
        }

        // Save all POST and PUT requests on database
        if ( $this->method !== 'GET' )
        {
            $this->save_db_log('lemonway', [
                'entity_id' 
            ]);
        }
    }


    /**
     * Return last Lemonway error
     */
    public function get_last_error()
    {
        // HTTP response is OK
        if ( empty($this->last_error) )
        {
            if ( $this->response && $this->response->isSuccessful() )
            {
                /**
                 | ------------------------------------------------------
                 | ERROR IN RESPONSE
                 | ------------------------------------------------------
                 |
                 | Check if error has been returned into the response.
                 | Example (JSON format):
                 |  {
                 |      "error":
                 |      {
                 |          "code": 275,
                 |          "message": "Mandatory fields required"
                 |      }
                 |  }
                 */
                $data_json = $this->response->getBody();
                if  ( !empty($data_json) )
                {
                    $vec_data = Json::decode($data_json);
                    if ( !empty($vec_data) )
                    {
                        if ( isset($vec_data['error']) )
                        {
                            $this->last_error = $vec_data['error'];
                        }
                        elseif ( isset($vec_data['code']) && isset($vec_data['message']) && count($vec_data) === 2 )
                        {
                            $this->last_error = $vec_data;
                        }
                    }
                }
            }
        }
        else if ( !is_array($this->last_error) && ! $this->is_last_action_success() && $this->response )
        {
            // HTTP error - For example {code: 403, message: "Forbidden"}
            $this->last_error = [
                'code'      => $this->response->getStatus(),
                'message'   => $this->response->getMessage()
            ];
        }

        return $this->last_error;
    }


    /**
     * Create a new individual or legal account
     * 
     * - API REST Endpoint "POST /v2/accounts/individual"
     * - API REST Endpoint "POST /v2/accounts/legal"
     */
    public function post_account($vec_input = [], $is_legal = false)
    {
        // Endpoint and entity information
        $this->entity_type = 'LemonwayAccount';
        $endpoint_uri = '/v2/accounts/individual';
        if ( $is_legal )
        {
            $endpoint_uri = '/v2/accounts/legal';
        }

        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->post($endpoint_uri, $vec_input);
    }



    /**
     * Updates an individual or legal account
     * 
     * - API REST Endpoint "PUT /v2/accounts/individual"
     * - API REST Endpoint "PUT /v2/accounts/legal"
     */
    public function put_account($lemonway_account_id, $is_legal = false, $vec_input = [])
    {
        // Endpoint and entity information
        $this->entity_type = 'LemonwayAccount';
        $this->entity_id = $lemonway_account_id;
        $endpoint_uri = '/v2/accounts/individual/'. $lemonway_account_id;
        if ( $is_legal )
        {
            $endpoint_uri = '/v2/accounts/legal/'. $lemonway_account_id;
        }

        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->put($endpoint_uri, $vec_input);
    }


    /**
     * Return detailed account data
     * 
     * - API REST Endpoint "GET /v2/accounts/{accountid}"
     * 
     * @see https://apidoc.lemonway.com/#operation/Accounts_AccountSingleGet
     */
    public function get_account($lemonway_account_id)
    {
        // Endpoint and entity information
        $this->entity_type = 'LemonwayAccount';
        $this->entity_id = $lemonway_account_id;
        $endpoint_uri = '/v2/accounts/'. $lemonway_account_id;

        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->get($endpoint_uri);
    }


    /**
     * Return payment account balance history form a Lemonway account
     * 
     * - API REST Endpoint "GET /v2/accounts/{accountid}/balances/history"
     * 
     * @see https://apidoc.lemonway.com/#operation/Accounts_BalancesHistoryGet
     */
    public function get_balance_history($lemonway_account_id, $unix_date)
    {
        // Endpoint and entity information
        $this->entity_type = 'LemonwayAccount';
        $this->entity_id = $lemonway_account_id;
        $endpoint_uri = '/v2/accounts/'. $lemonway_account_id .'/balances/history';

        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->get($endpoint_uri, [
            'atDate' => $unix_date
        ]);
    }


    /**
     * Update Payment Account Status
     * 
     * - API REST Endpoint "PUT /v2/accounts/kycstatus/{accountid}"
     * 
     * @see https://apidoc.lemonway.com/#operation/Accounts_KycstatusPut
     */
    public function update_status($lemonway_account_id, $status)
    {
        // Endpoint and entity information
        $this->entity_type = 'LemonwayAccount';
        $this->entity_id = $lemonway_account_id;
        $endpoint_uri = '/v2/accounts/kycstatus/'. $lemonway_account_id;

        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->put($endpoint_uri, [
            'status' => $status
        ]);
    }


    /**
     * Blocks a Lemonway account
     * 
     * - API REST Endpoint "PUT /v2/accounts/{accountid}/blocked"
     * 
     * @see https://apidoc.lemonway.com/#operation/Accounts_BlockedPut
     */
    public function block_account($lemonway_account_id, $comment = null, $is_blocked = true)
    {
        // Endpoint and entity information
        $this->entity_type = 'LemonwayAccount';
        $this->entity_id = $lemonway_account_id;
        $endpoint_uri = '/v2/accounts/'. $lemonway_account_id .'/blocked';

        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->put($endpoint_uri, [
            'isblocked' => $is_blocked,
            'comment'   => $comment
        ]);
    }


    /**
     * Unbblocks a Lemonway account
     * 
     * - API REST Endpoint "PUT /v2/accounts/{accountid}/blocked"
     * 
     * @see https://apidoc.lemonway.com/#operation/Accounts_BlockedPut
     */
    public function unblock_account($lemonway_account_id, $comment = null)
    {
        return $this->block_account($lemonway_account_id, $comment, false);
    }


    /**
     * Upload a document for KYC (Know Your Customers)
     * 
     * 
     * Upload documents to a payment account. A file should be less than 10 Mb.
     * For security reasons, only the following documents types are accepted: PDF, JPG, JPEG and PNG
     * 
     * - API REST Endpoint "POST /v2/accounts/{accountid}/documents/upload"
     * 
     * @see https://apidoc.lemonway.com/#operation/Accounts_DocumentsUploadPost
     */
    public function post_document($lemonway_account_id, $vec_input)
    {
        // Endpoint and entity information
        $this->entity_type = 'LemonwayDocument';
        $endpoint_uri = '/v2/accounts/'. $lemonway_account_id .'/documents/upload';

        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->post($endpoint_uri, $vec_input);
    }


    /**
     * Return documents associated with a Lemonway account
     * 
     * - API REST Endpoint "GET /v2/accounts/{accountid}/documents"
     * 
     * @see https://apidoc.lemonway.com/#operation/Accounts_DocumentGet
     */
    public function get_documents($lemonway_account_id)
    {   
        // Endpoint and entity information
        $this->entity_type = 'LemonwayAccount';
        $this->entity_id = $lemonway_account_id;
        $endpoint_uri = '/v2/accounts/'. $lemonway_account_id .'/documents';

        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->get($endpoint_uri);
    }


    /**
     * Add an IBAN to a payment account for Money-Outs
     * 
     * 
     * To send Money-Out from the Lemonway payment account to
     * the bank account of your clients, you need to register their bank account details.
     * This method lets our system to link an IBAN to a payment account.
     * 
     * - API REST Endpoint "POST /v2/moneyouts/iban"
     * 
     * @see https://apidoc.lemonway.com/#operation/MoneyOuts_IbanPost
     */
    public function post_iban($lemonway_account_id, $vec_input)
    {
        // Endpoint and entity information
        $this->entity_type = 'LemonwayIban';
        $endpoint_uri = '/v2/moneyouts/iban';

        // Parameter 'accountId' is required
        if ( !isset($vec_input['accountId']) )
        {
            $vec_input['accountId'] = $lemonway_account_id;
        }

        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->post($endpoint_uri, $vec_input);
    }


    /**
     * Return IBAN data associated with a payment account
     * 
     * - API REST Endpoint "GET /v2/moneyouts/{accountid}/iban"
     * 
     * @see https://apidoc.lemonway.com/#operation/MoneyOuts_IbanGet
     */
    public function get_ibans($lemonway_account_id)
    {
        // Endpoint and entity information
        $this->entity_type = 'LemonwayAccount';
        $this->entity_id = $lemonway_account_id;
        $endpoint_uri = '/v2/moneyouts/'. $lemonway_account_id .'/iban';

        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->get($endpoint_uri);
    }


    /**
     * Disable Bank Information (IBAN) from a payment account
     * 
     * - API REST Endpoint "PUT /v2/moneyouts/iban/{IbanId}/unregister"
     * 
     * @see https://apidoc.lemonway.com/#operation/MoneyOuts_IbanUnregisterPut
     */
    public function disable_iban($lemonway_iban_id, $lemonway_account_id)
    {
        // Endpoint and entity information
        $this->entity_type = 'LemonwayIban';
        $this->entity_id = $lemonway_iban_id;
        $endpoint_uri = '/v2/moneyouts/iban/'. $lemonway_iban_id .'/unregister';
        
        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->put($endpoint_uri, [
            'wallet' => $lemonway_account_id
        ]);
    }


    /**
     * Create Dedicated Virtual IBANs
     * 
     * - API REST Endpoint "POST /v2/moneyins/bankwire/iban/create"
     * 
     * @see https://apidoc.lemonway.com/#operation/MoneyIns_BankwireIbanCreatePost
     */
    public function post_virtual_iban($lemonway_account_id, $vec_input)
    {
        // Endpoint and entity information
        $this->entity_type = 'LemonwayIban';
        $endpoint_uri = '/v2/moneyins/bankwire/iban/create';

        // Required input parameters
        $vec_input['wallet'] = $lemonway_account_id;
        if ( ! isset($vec_input['country']) )
        {
            $vec_input['country'] = 'FR';
        }
        if ( ! isset($vec_input['generatePDFAndQrCode']) )
        {
            $vec_input['generatePDFAndQrCode'] = true;
            $vec_input['pdfLanguage'] = 'en';
        }

        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->post($endpoint_uri, $vec_input);
    }


    /**
     * Disable a Dedicated Virtual IBAN
     * 
     * - API REST Endpoint "POST /v2/moneyins/bankwire/iban/{ibanid}/disable"
     * 
     * @see https://apidoc.lemonway.com/#operation/MoneyIns_BankwireIbanDisablePost
     */
    public function disable_virtual_iban($lemonway_iban_id, $lemonway_account_id)
    {
        // Endpoint and entity information
        $this->entity_type = 'LemonwayIban';
        $this->entity_id = $lemonway_iban_id;
        $endpoint_uri = '/v2/moneyins/bankwire/iban/'. $lemonway_iban_id .'/disable';
        
        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->post($endpoint_uri, [
            'wallet' => $lemonway_account_id
        ]);
    }


    /**
     * Initiate a Web PSP Payment
     * 
     * - API REST Endpoint "POST /v2/moneyins/card/webinit"
     * 
     * @see https://apidoc.lemonway.com/#operation/MoneyIns_CardWebInitPost
     */
    public function post_moneyin_webinit($lemonway_account_id, $vec_input)
    {
        // Endpoint and entity information
        $this->entity_type = 'LemonwayTransaction';
        $endpoint_uri = '/v2/moneyins/card/webinit';

        // Check input parameters
        $vec_input['accountId'] = $lemonway_account_id;
        $vec_input['returnUrl'] = isset($vec_input['returnUrl']) ? $vec_input['returnUrl'] : Url::home();
        $vec_input['errorUrl']  = isset($vec_input['errorUrl']) ? $vec_input['errorUrl'] : Url::home();
        $vec_input['cancelUrl'] = isset($vec_input['cancelUrl']) ? $vec_input['cancelUrl'] : Url::home();
        $vec_input['label']     = isset($vec_input['label']) ? $vec_input['label'] : Yii::app()->name;
    
        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->post($endpoint_uri, $vec_input);
    }


    /**
     * Internal transfer between Lemonway payments accounts
     * 
     * - API REST Endpoint "POST /v2/p2p"
     * 
     * @see https://apidoc.lemonway.com/#operation/P2Ps_P2pPost
     */
    public function post_money_p2p($sender_account_id, $receiver_account_id, $vec_input)
    {   
        // Endpoint and entity information
        $this->entity_type = 'LemonwayTransaction';
        $endpoint_uri = '/v2/p2p';

        // Required input parameters
        $vec_input['debitAccountId'] = $sender_account_id;
        $vec_input['creditAccountId'] = $receiver_account_id;

        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->post($endpoint_uri, $vec_input);
    }


    /**
     * External fund transfer from a payment account to a bank account
     * 
     * - API REST Endpoint "POST /v2/moneyouts"
     * 
     * @see https://apidoc.lemonway.com/#operation/MoneyOuts_MoneyOutPost
     */
    public function post_money_out($lemonway_account_id, $vec_input)
    {
        // Endpoint and entity information
        $this->entity_type = 'LemonwayTransaction';
        $endpoint_uri = '/v2/moneyouts';

        // Required input parameters
        $vec_input['accountId'] = $lemonway_account_id;

        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->post($endpoint_uri, $vec_input);
    }



    /**
     * Search for a Money-In transaction
     * 
     * - API REST Endpoint "GET /v2/moneyins"
     * 
     * @see https://apidoc.lemonway.com/#operation/MoneyIns_MoneyInGet
     */
    public function get_transaction_in($transaction_id, $vec_input = [])
    {
        // Endpoint and entity information
        $this->entity_type = 'LemonwayTransaction';
        $this->entity_id = $transaction_id;
        $endpoint_uri = '/v2/moneyins';

        // Input parameters
        $vec_input['transactionId'] = $transaction_id;

        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->get($endpoint_uri, $vec_input);
    }


    /**
     * Search for a Money-Out transaction
     * 
     * - API REST Endpoint "GET /v2/moneyouts"
     * 
     * @see https://apidoc.lemonway.com/#operation/MoneyOuts_MoneyOutGet
     */
    public function get_transaction_out($transaction_id, $vec_input = [])
    {
        // Endpoint and entity information
        $this->entity_type = 'LemonwayTransaction';
        $this->entity_id = $transaction_id;
        $endpoint_uri = '/v2/moneyouts';

        // Input parameters
        $vec_input['transactionId'] = $transaction_id;

        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->get($endpoint_uri, $vec_input);
    }



    /**
     * Retrieve transaction information between payment accounts (P2P)
     * 
     * - API REST Endpoint "GET /v2/p2p/{transactionid}"
     * 
     * @see https://apidoc.lemonway.com/#operation/P2Ps_P2pGet
     */
    public function get_transaction_p2p($transaction_id, $vec_input = [])
    {
        // Endpoint and entity information
        $this->entity_type = 'LemonwayTransaction';
        $this->entity_id = $transaction_id;
        $endpoint_uri = '/v2/p2p/'. $transaction_id;

        // Input parameters
        $vec_input['transactionId'] = $transaction_id;

        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->get($endpoint_uri, $vec_input);
    }


    /**
     * Get list of all Payment Account Transactions
     * 
     * - API REST Endpoint "GET /v2/accounts/{accountId}/transactions"
     * 
     * @see https://apidoc.lemonway.com/#operation/Accounts_TransactionsGet
     */
    public function get_account_transactions($lemonway_account_id, $vec_input = [])
    {
        // Endpoint and entity information
        $this->entity_type = 'LemonwayAccount';
        $this->entity_id = $lemonway_account_id;
        $endpoint_uri = '/v2/accounts/'. $lemonway_account_id .'/transactions';

        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->get($endpoint_uri, $vec_input);
    }


    /**
     * Load the file "/storage/tmp/lemonway.json"
     */
    public function load_token_file()
    {
        $token_file_path = Yii::app()->path->get('privateTempPath') . DIRECTORY_SEPARATOR . 'lemonway.json';

        if ( empty($this->token_file) )
        {
            $this->token_file = Yii::app()->file->set($token_file_path);
        }

        if ( $this->token_file && ( $this->token_file->getExists() || $this->token_file->create() ) )
        {
            return true;
        }

        return false;
    }


    /**
     * Return access token (OAuth)
     */
    public function get_access_token()
    {
        // Get token information saved on /storage/tmp/lemonway.json file
        if ( $this->load_token_file() )
        {
            $now = time();
            $data_json = $this->token_file->getContents();
            
            $is_request_token = true;
            if ( !empty($data_json) )
            {
                $vec_data = Json::decode($data_json);
                if ( !empty($vec_data) && is_array($vec_data) && isset($vec_data['expiration_date']) && $vec_data['expiration_date'] > $now )
                {
                    $is_request_token = false;
                }
            }

            // Empty file or expired token ---> Request new token and save on the file
            if ( $is_request_token )
            {
                $vec_data = $this->request_new_token();
                if ( isset($vec_data['expires_in']) )
                {
                    $vec_data['expiration_date'] = $vec_data['expires_in'] + $now;
                }

                $this->token_file->setContents(Json::encode($vec_data));
            }

            // Return access token
            if ( isset($vec_data['access_token']) )
            {
                return $vec_data['token_Type'] . ' ' .$vec_data['access_token'];
            }
        }

        return '';
    }


    /**
     * Request new access token via API
     * 
     * @see https://apidoc.lemonway.com/#section/Authentication-(OAuth-2.0)
     */
    public function request_new_token()
    {
        // Build URL and create HTTP Client
        $client = new \EHttpClient($this->auth_url);
        $client->setMethod('POST');
        $client->setHeaders([
            'Authorization' => 'Basic ' . $this->api_key,
            'accept'        => 'application/json;charset=UTF-8',
            'Content-Type'  => 'application/x-www-form-urlencoded'
        ]);
        $client->setParameterPost([
            'grant_type'    => 'client_credentials'
        ]);

        $response = $client->request();
        if ( $response->isSuccessful() )
        {
            if ( $this->is_debug )
            {
                Log::lemonway_dev('Requesting new token: OK - '. trim($response->getBody()));
            }

            return Json::decode($response->getBody());
        }
        else
        {
            Log::lemonway_error('Requesting new token: ERROR - '. trim($response->getRawBody()));

            if ( $this->is_debug )
            {
                Log::lemonway_dev('Requesting new token: ERROR - '. trim($response->getRawBody()));
            }

            return null;
        }
    }
}