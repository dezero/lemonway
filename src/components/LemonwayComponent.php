<?php
/**
 * Lemonway Component
 * 
 * Helper classes to work with Lemonway REST API
 * 
 * @see https://apidoc.lemonway.com/
 */

namespace dzlab\lemonway\components;

use dz\base\ApplicationComponent;
use dz\helpers\DateHelper;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\helpers\Url;
use dz\modules\asset\models\AssetFile;
use dz\modules\settings\helpers\CountryHelper;
use dzlab\lemonway\models\LemonwayAccount;
use dzlab\lemonway\models\LemonwayDocument;
use dzlab\lemonway\models\LemonwayIban;
use dzlab\lemonway\models\LemonwayTransaction;
use user\models\User;
use Yii;

class LemonwayComponent extends ApplicationComponent
{
    /**
     * @var bool. Debug mode? (log all responses)
     */ 
    public $is_debug = false;


    /**
     * @var bool Sanbdox environment
     */
    public $is_sandbox = false;


    /**
     * @var string. Lemonway dashboard URL
     */ 
    public $dashboard_url;


    /**
     * @var array Lemonway configuration
     */
    protected $vec_config = [];


    /**
     * @var object LemonwayApi
     */
    protected $api;


    /**
     * Init function
     */
    public function init()
    {
        // Lemonway configuration
        $this->vec_config = Yii::app()->config->get('components.lemonway');

        // Debug mode?
        if ( isset($this->vec_config['is_debug']) )
        {
            $this->is_debug = $this->vec_config['is_debug'];
        }

        // Sandbox environment?
        if ( isset($this->vec_config['is_sandbox']) )
        {
            $this->is_sandbox = $this->vec_config['is_sandbox'];
        }

        // Init the component API
        $this->api = Yii::app()->lemonwayApi;

        // Dashboard URL
        if ( $this->is_sandbox )
        {
            $this->dashboard_url = $this->vec_config['sandbox_dashboard_url'];
        }
        else
        {
            $this->dashboard_url = $this->vec_config['dashboard_url'];
        }

        parent::init();
    }


    /**
     * Get last EHttpResponse object
     */
    public function get_response()
    {
        return $this->api->get_response();
    }



    /**
     * Get last used endpoint
     */
    public function get_last_endpoint()
    {
        return $this->api->last_endpoint;
    }


    /**
     * Return Lemonway Dashboard URL
     */
    public function get_dashboard_url()
    {
        return $this->dashboard_url;
    }


    /**
     * Create a new LemonwayAccount model
     * 
     * - API REST Endpoint "POST /v2/accounts/individual"
     * - API REST Endpoint "POST /v2/accounts/legal"
     * 
     * @see LemonwayApi::post_account()
     */
    public function create_account($user_id, $is_legal = false, $vec_input = [])
    {
        $user_model = User::findOne($user_id);
        if ( $user_model )
        {
            // Send a "POST /v2/accounts/indivudal" or "POST /v2/accounts/legal" request
            $response = $this->api->post_account($vec_input, $is_legal);

            // Create a new LemonwayAccount model
            if ( $this->api->is_last_action_success() )
            {
                $vec_response = $this->api->get_response_body(true);
                
                if ( $this->is_debug )
                {
                    Log::lemonway_dev("LemonwayManager::create_account({$user_id}) - Last action success");
                    Log::lemonway_dev(print_r($vec_response, true));
                }

                $lemonway_account_model = Yii::createObject(LemonwayAccount::class);
                $lemonway_account_model->setAttributes([
                    'user_id'                   => $user_id,
                    'email'                     => $vec_input['email'],
                    'firstname'                 => $vec_input['firstName'],
                    'lastname'                  => $vec_input['lastName'],
                    'address_street'            => isset($vec_input['adresse']['street']) ? $vec_input['adresse']['street'] : null,
                    'address_post_code'         => isset($vec_input['adresse']['postCode']) ? $vec_input['adresse']['postCode'] : null,
                    'address_city'              => isset($vec_input['adresse']['city']) ? $vec_input['adresse']['city'] : null,
                    'address_country_code'      => CountryHelper::alpha2_code($vec_input['adresse']['country']),
                    'birth_date'                => $vec_input['birth']['date'],
                    'birth_city'                => isset($vec_input['birth']['city']) ? $vec_input['birth']['city'] : null,
                    'birth_country_code'        => isset($vec_input['birth']['Country']) ? CountryHelper::alpha2_code($vec_input['birth']['Country']) : null,
                    'nationality_country_code'  => CountryHelper::alpha2_code($vec_input['nationality']),
                    'phone_number'              => isset($vec_input['phoneNumber']) ? $vec_input['phoneNumber'] : null,
                    'mobile_number'             => isset($vec_input['mobileNumber']) ? $vec_input['mobileNumber'] : null,
                    'is_debtor'                 => isset($vec_input['isDebtor']) && $vec_input['isDebtor'] ? 1 : 0,
                    'is_payer_or_beneficiary'   => $vec_input['payerOrBeneficiary'] === 1 ? 1 : 0,
                    'is_one_time_customer'      => isset($vec_input['isOneTimeCustomerAccount']) && $vec_input['isOneTimeCustomerAccount'] ? 1 : 0,
                    'is_technical'              => isset($vec_input['isTechnicalAccount']) && $vec_input['isTechnicalAccount'] ? 1 : 0,
                    'last_sync_date'            => time(),
                    'last_sync_endpoint'        => 'POST___'. $this->api->last_endpoint
                ]);

                // INDIVIDUAL account
                if ( ! $is_legal && !empty($vec_response) && isset($vec_response['account']) && isset($vec_response['account']['id']) && isset($vec_response['account']['internalId']) )
                {
                    $lemonway_account_model->lemonway_internal_id = $vec_response['account']['internalId'];
                    $lemonway_account_model->lemonway_account_id = $vec_response['account']['id'];
                    $lemonway_account_model->is_legal = 0;
                }
                
                // LEGAL account
                else if ( $is_legal && !empty($vec_response) && isset($vec_response['legalAccount']) && isset($vec_response['legalAccount']['id']) && isset($vec_response['legalAccount']['lemonwayId']) )
                {
                    // Company information
                    $lemonway_account_model->company_name = $vec_input['company']['name'];
                    $lemonway_account_model->company_description = $vec_input['company']['description'];
                    $lemonway_account_model->company_website = isset($vec_input['company']['websiteUrl']) ? $vec_input['company']['websiteUrl'] : null;
                    $lemonway_account_model->company_vat_code = isset($vec_input['company']['identificationNumber']) ? $vec_input['company']['identificationNumber'] : null;

                    // ID information
                    $lemonway_account_model->lemonway_internal_id = $vec_response['legalAccount']['lemonwayId'];
                    $lemonway_account_model->lemonway_account_id = $vec_response['legalAccount']['id'];
                    $lemonway_account_model->is_legal = 1;
                }

                // Account created succesfully in Lemonway
                if ( !empty($lemonway_account_model->lemonway_internal_id) )
                {
                    if ( $lemonway_account_model->save() )
                    {
                        // Save entity information (useful for logs)
                        $this->api->save_entity_info('LemonwayAccount', $lemonway_account_model->lemonway_internal_id);
                    }
                    else
                    {
                        Log::save_model_error($lemonway_account_model);
                    }

                    // Return LemonwayAccount model
                    return $lemonway_account_model;
                }

                else if ( $this->is_debug )
                {
                    Log::lemonway_dev("LemonwayManager::create_account({$user_id}) - Incorrect response: ". print_r($vec_response, true));
                }
            }
            else if ( $this->is_debug )
            {
                Log::lemonway_dev("LemonwayManager::create_account({$user_id}) - Last action error: ". print_r($response, true));
            }
        }
        else
        {
            Log::lemonway_error("LemonwayManager::create_account({$user_id}) - USER does not exist: {$user_id}");
        }

        return null;
    }


    /**
     * Create a new individual account
     * 
     * - API REST Endpoint "POST /v2/accounts/individual"
     * 
     * @see https://apidoc.lemonway.com/#operation/Accounts_IndividualPost
     */
    public function create_individual_account($user_id, $vec_input = [])
    {
        return $this->create_account($user_id, false, $vec_input);
    }


    /**
     * Create a new legal account
     * 
     * - API REST Endpoint "POST /v2/accounts/legal"
     * 
     * @see https://apidoc.lemonway.com/#operation/Accounts_LegalPost
     */
    public function create_legal_account($user_id, $vec_input = [])
    {
        return $this->create_account($user_id, true, $vec_input);
    }


    /**
     * Updates an LemonwayAccount model
     * 
     * - API REST Endpoint "PUT /v2/accounts/individual"
     * - API REST Endpoint "PUT /v2/accounts/legal"
     * 
     * @see LemonwayApi::put_account()
     */
    public function update_account($user_id, $is_legal = false, $vec_input = [])
    {
        // User model       
        $user_model = User::findOne($user_id);

        // LemonwayAccount model
        $lemonway_account_model = LemonwayAccount::findOne($user_id);
        
        if ( $user_model && $lemonway_account_model )
        {
            // Send a "PUT /v2/accounts/indivudal" or "PUT /v2/accounts/legal" request
            $response = $this->api->put_account($lemonway_account_model->lemonway_account_id, $is_legal, $vec_input);

            // Update the LemonwayAccount model
            if ( $this->api->is_last_action_success() )
            {
                $vec_response = $this->api->get_response_body(true);
                
                if ( $this->is_debug )
                {
                    Log::lemonway_dev("LemonwayManager::update_account({$lemonway_account_model->lemonway_account_id}) - Last action success");
                    Log::lemonway_dev(print_r($vec_response, true));
                }

                // Update account
                if ( !empty($vec_response) && isset($vec_response['account']) && isset($vec_response['account']['id']) && ( isset($vec_response['account']['internalId']) || isset($vec_response['account']['lemonwayId'])) )
                {
                    $lemonway_account_model->setAttributes([
                        'lemonway_internal_id'      => isset($vec_response['account']['internalId']) ? $vec_response['account']['internalId'] : $vec_response['account']['lemonwayId'],
                        'lemonway_account_id'       => $vec_response['account']['id'],
                        'user_id'                   => $user_id,
                        'email'                     => $vec_input['email'],
                        'firstname'                 => $vec_input['firstName'],
                        'lastname'                  => $vec_input['lastName'],
                        'address_street'            => isset($vec_input['adresse']['street']) ? $vec_input['adresse']['street'] : null,
                        'address_post_code'         => isset($vec_input['adresse']['postCode']) ? $vec_input['adresse']['postCode'] : null,
                        'address_city'              => isset($vec_input['adresse']['city']) ? $vec_input['adresse']['city'] : null,
                        'address_country_code'      => CountryHelper::alpha2_code($vec_input['adresse']['country']),
                        'birth_date'                => $vec_input['birth']['date'],
                        'birth_city'                => isset($vec_input['birth']['city']) ? $vec_input['birth']['city'] : null,
                        'birth_country_code'        => isset($vec_input['birth']['Country']) ? CountryHelper::alpha2_code($vec_input['birth']['Country']) : null,
                        'nationality_country_code'  => CountryHelper::alpha2_code($vec_input['nationality']),
                        'phone_number'              => isset($vec_input['phoneNumber']) ? $vec_input['phoneNumber'] : null,
                        'mobile_number'             => isset($vec_input['mobileNumber']) ? $vec_input['mobileNumber'] : null,
                        'is_debtor'                 => isset($vec_input['isDebtor']) && $vec_input['isDebtor'] ? 1 : 0,
                        'is_payer_or_beneficiary'   => $vec_input['payerOrBeneficiary'] === 1 ? 1 : 0,
                        'last_sync_date'            => time(),
                        'last_sync_endpoint'        => 'PUT___'. $this->api->last_endpoint
                    ]);

                    if ( $is_legal )
                    {
                        // Company information
                        $lemonway_account_model->company_name = $vec_input['company']['name'];
                        $lemonway_account_model->company_description = $vec_input['company']['description'];
                        $lemonway_account_model->company_website = isset($vec_input['company']['websiteUrl']) ? $vec_input['company']['websiteUrl'] : null;
                        $lemonway_account_model->company_vat_code = isset($vec_input['company']['identificationNumber']) ? $vec_input['company']['identificationNumber'] : null;
                    }

                    if ( ! $lemonway_account_model->save() )
                    {
                        Log::save_model_error($lemonway_account_model);
                    }

                    // Return updated LemonwayAccoun model
                    return $lemonway_account_model;
                }
                else if ( $this->is_debug )
                {
                    Log::lemonway_dev("LemonwayManager::update_account({$lemonway_account_model->lemonway_account_id}) - Incorrect response: ". print_r($vec_response, true));
                }
            }
            else if ( $this->is_debug )
            {
                Log::lemonway_dev("LemonwayManager::update_account({$lemonway_account_model->lemonway_account_id}) - Last action error: ". print_r($response, true));
            }
        }
        else
        {
            if ( ! $user_model )
            {
                Log::lemonway_error("LemonwayManager::update_account({$user_id}) - USER does not exist: {$user_id}");
            }

            if ( ! $lemonway_account_model )
            {
                Log::lemonway_error("LemonwayManager::update_account({$user_id}) - LEMONWAY ACCOUNT does not exist: {$user_id}");
            }
        }

        return null;
    }


    /**
     * Update an individual account
     * 
     * - API REST Endpoint "PUT /v2/accounts/individual/{accountid}"
     * 
     * @see https://apidoc.lemonway.com/#operation/Accounts_IndividualPut
     */
    public function update_individual_account($user_id, $vec_input = [])
    {
        return $this->update_account($user_id, false, $vec_input);
    }


    /**
     * Update an legal account
     * 
     * - API REST Endpoint "PUT /v2/accounts/legal/{accountid}"
     * 
     * @see https://apidoc.lemonway.com/#operation/Accounts_LegalSinglePut
     */
    public function update_legal_account($user_id, $vec_input = [])
    {
        return $this->update_account($user_id, true, $vec_input);
    }


    /**
     * Return a Lemonway account with last API information
     * 
     * - API REST Endpoint "GET /v2/accounts/{accountid}"
     * 
     * @see LemonwayApi::get_account()
     */
    public function get_account($user_id)
    {
        // User model
        $user_model = User::findOne($user_id);

        // LemonwayAccount model
        $lemonway_account_model = LemonwayAccount::findOne($user_id);
        
        if ( $user_model && $lemonway_account_model )
        {
            // Send a "GET /v2/accounts/{accountid}" request
            $response = $this->api->get_account($lemonway_account_model->lemonway_account_id);

            // Update the LemonwayAccount model with last received data
            if ( $this->api->is_last_action_success() )
            {
                $vec_response = $this->api->get_response_body(true);
                
                if ( $this->is_debug )
                {
                    Log::lemonway_dev("LemonwayManager::get_account({$lemonway_account_model->lemonway_account_id}) - Last action success");
                    Log::lemonway_dev(print_r($vec_response, true));
                }

                // Update account
                if ( !empty($vec_response) && isset($vec_response['account']) && isset($vec_response['account']['id']) && isset($vec_response['account']['internalId']) )
                {
                    $lemonway_account_model->setAttributes([
                        'lemonway_internal_id'      => $vec_response['account']['internalId'],
                        'lemonway_account_id'       => $vec_response['account']['id'],
                        'status_type'               => $vec_response['account']['status'],
                        'balance_amount'            => $vec_response['account']['balance'],
                        'is_blocked'                => $vec_response['account']['isblocked'] ? 1 : 0,
                        'is_debtor'                 => $vec_response['account']['isDebtor'] ? 1 : 0,
                        'is_payer_or_beneficiary'   => $vec_response['account']['payerOrBeneficiary'] === 1 ? 1 : 0,
                        'last_sync_date'            => time(),
                        'last_sync_endpoint'        => 'GET___'. $this->api->last_endpoint
                    ]);
                    if ( ! $lemonway_account_model->save() )
                    {
                        Log::save_model_error($lemonway_account_model);
                    }

                    return $lemonway_account_model;
                }
                else if ( $this->is_debug )
                {
                    Log::lemonway_dev("LemonwayManager::get_account({$lemonway_account_model->lemonway_account_id}) - Incorrect response: ". print_r($vec_response, true));
                }
            }
            else if ( $this->is_debug )
            {
                Log::lemonway_dev("LemonwayManager::get_account({$lemonway_account_model->lemonway_account_id}) - Last action error: ". print_r($response, true));
            }
        }

        else
        {
            if ( ! $user_model )
            {
                Log::lemonway_error("LemonwayManager::get_account({$user_id}) - USER does not exist: {$user_id}");
            }

            if ( ! $lemonway_account_model )
            {
                Log::lemonway_error("LemonwayManager::get_account({$user_id}) - LEMONWAY ACCOUNT does not exist: {$user_id}");
            }
        }

        return null;
    }


    /**
     * Updates Payment Account status
     * 
     * - API REST Endpoint "PUT /v2/accounts/kycstatus/{accountid}"
     * 
     * @see LemonwayApi::update_status()
     */
    public function update_status($user_id, $status)
    {
        // User model       
        $user_model = User::findOne($user_id);

        // LemonwayAccount model
        $lemonway_account_model = LemonwayAccount::findOne($user_id);
        
        if ( $user_model && $lemonway_account_model )
        {
            // Send a "PUT /v2/accounts/kycstatus/{accountid}" request
            $response = $this->api->update_status($lemonway_account_model->lemonway_account_id, $status);

            // Update the LemonwayAccount model
            if ( $this->api->is_last_action_success() )
            {
                $vec_response = $this->api->get_response_body(true);
                
                if ( $this->is_debug )
                {
                    Log::lemonway_dev("LemonwayManager::update_status({$lemonway_account_model->lemonway_account_id}, {$status}) - Last action success");
                    Log::lemonway_dev(print_r($vec_response, true));
                }

                // Update account
                if ( !empty($vec_response) && isset($vec_response['account']) && isset($vec_response['account']['id']) && isset($vec_response['account']['status'])  )
                {
                    $lemonway_account_model->setAttributes([
                        'status_type'           => $vec_response['account']['status'],
                        'last_sync_date'        => time(),
                        'last_sync_endpoint'    => 'PUT___'. $this->api->last_endpoint
                    ]);

                    if ( ! $lemonway_account_model->save() )
                    {
                        Log::save_model_error($lemonway_account_model);
                    }

                    // Return updated LemonwayAccoun model
                    return $lemonway_account_model;
                }
                else if ( $this->is_debug )
                {
                    Log::lemonway_dev("LemonwayManager::update_status({$lemonway_account_model->lemonway_account_id}, {$status}) - Incorrect response: ". print_r($vec_response, true));
                }
            }
            else if ( $this->is_debug )
            {
                Log::lemonway_dev("LemonwayManager::update_status({$lemonway_account_model->lemonway_account_id}, {$status}) - Last action error: ". print_r($response, true));
            }
        }
        else
        {
            if ( ! $user_model )
            {
                Log::lemonway_error("LemonwayManager::update_status({$user_id}, {$status}) - USER does not exist: {$user_id}");
            }

            if ( ! $lemonway_account_model )
            {
                Log::lemonway_error("LemonwayManager::update_status({$user_id}, {$status}) - LEMONWAY ACCOUNT does not exist: {$user_id}");
            }
        }

        return null;
    }


    /**
     * Return payment account balance history form a Lemonway account
     * 
     * - API REST Endpoint "GET /v2/accounts/{accountid}/balances/history"
     * 
     * @see LemonwayApi::get_balance_history()
     */
    public function get_balance_history($user_id, $unix_date)
    {
        // User model
        $user_model = User::findOne($user_id);

        // LemonwayAccount model
        $lemonway_account_model = LemonwayAccount::findOne($user_id);
        
        if ( $user_model && $lemonway_account_model )
        {
            // Send a "GET /v2/accounts/{accountid}/balances/history" request
            $response = $this->api->get_balance_history($lemonway_account_model->lemonway_account_id, $unix_date);

            // Check returned data
            if ( $this->api->is_last_action_success() )
            {
                $vec_response = $this->api->get_response_body(true);
                
                if ( $this->is_debug )
                {
                    Log::lemonway_dev("LemonwayManager::get_balance_history({$lemonway_account_model->lemonway_account_id}, {$unix_date}) - Last action success");
                    Log::lemonway_dev(print_r($vec_response, true));
                }

                // Return data
                if ( !empty($vec_response) && isset($vec_response['balance']) )
                {
                    if ( !empty($vec_response['balance']) )
                    {
                        return $vec_response['balance'] / 100;
                    }
                }
                else if ( $this->is_debug )
                {
                    Log::lemonway_dev("Incorrect response: ". print_r($vec_response, true));
                }
            }
            else if ( $this->is_debug )
            {
                Log::lemonway_dev("LemonwayManager::get_balance_history({$lemonway_account_model->lemonway_account_id}, {$unix_date}) - Last action error: ". print_r($response, true));
            }
        }
        else
        {
            if ( ! $user_model )
            {
                Log::lemonway_error("LemonwayManager::get_balance_history({$user_id}, {$unix_date}) - USER does not exist: {$user_id}");
            }

            if ( ! $lemonway_account_model )
            {
                Log::lemonway_error("LemonwayManager::get_balance_history({$user_id}, {$unix_date}) - LEMONWAY ACCOUNT does not exist: {$user_id}");
            }
        }

        return null;   
    }


    /**
     * Blocks or unblocks a Lemonway model
     * 
     * - API REST Endpoint "PUT /v2/accounts/{accountid}/blocked"
     * 
     * @see LemonwayApi::block_account()
     */
    public function block_account($user_id, $comment = null, $is_blocked = true)
    {
        // User model
        $user_model = User::findOne($user_id);

        // LemonwayAccount model
        $lemonway_account_model = LemonwayAccount::findOne($user_id);
        
        if ( $user_model && $lemonway_account_model )
        {
            // Send a "GET /v2/accounts/{accountid}/blocked" request
            $response = $this->api->block_account($lemonway_account_model->lemonway_account_id, $comment, $is_blocked);

            // Update the LemonwayAccount model with last received data
            if ( $this->api->is_last_action_success() )
            {
                $vec_response = $this->api->get_response_body(true);
                
                if ( $this->is_debug )
                {
                    Log::lemonway_dev("LemonwayManager::block_account({$lemonway_account_model->lemonway_account_id}) - Last action success");
                    Log::lemonway_dev(print_r($vec_response, true));
                }

                // Update account
                if ( !empty($vec_response) && isset($vec_response['account']) && isset($vec_response['account']['id']) && isset($vec_response['account']['isBlocked']) )
                {
                    $lemonway_account_model->setAttributes([
                        'is_blocked'            => $vec_response['account']['isBlocked'] ? 1 : 0,
                        'last_sync_date'        => time(),
                        'last_sync_endpoint'    => 'PUT___'. $this->api->last_endpoint
                    ]);
                    if ( $lemonway_account_model->save() )
                    {
                        return true;
                    }
                    else
                    {
                        Log::save_model_error($lemonway_account_model);
                    }
                }
                else if ( $this->is_debug )
                {
                    Log::lemonway_dev("LemonwayManager::block_account({$lemonway_account_model->lemonway_account_id}) - Incorrect response: ". print_r($vec_response, true));
                }
            }
            else if ( $this->is_debug )
            {
                Log::lemonway_dev("LemonwayManager::block_account({$lemonway_account_model->lemonway_account_id}) - Last action error: ". print_r($response, true));
            }
        }
        else
        {
            if ( ! $user_model )
            {
                Log::lemonway_error("LemonwayManager::block_account({$user_id}, {$is_blocked}) - USER does not exist: {$user_id}");
            }

            if ( ! $lemonway_account_model )
            {
                Log::lemonway_error("LemonwayManager::block_account({$user_id}, {$is_blocked}) - LEMONWAY ACCOUNT does not exist: {$user_id}");
            }
        }

        return false;   
    }


    /**
     * Unbblocks a Lemonway model
     * 
     * - API REST Endpoint "PUT /v2/accounts/{accountid}/blocked"
     * 
     * @see https://apidoc.lemonway.com/#operation/Accounts_BlockedPut
     */
    public function unblock_account($user_id, $comment = null)
    {
        return $this->block_account($user_id, $comment, false);
    }


    /**
     * Upload a document for KYC (Know Your Customers)
     * 
     * 
     * Upload documents to a payment account. A file should be less than 10 Mb.
     * For security reasons, only the following documents types are accepted: PDF, JPG, JPEG and PNG
     * 
     * - API REST Endpoint "POST /v2/accounts/{accountid}/documents/upload"
     * 
     * @see LemonwayApi::post_document()
     */
    public function upload_document($user_id, $file_id, $document_type = 0)
    {
        // User model       
        $user_model = User::findOne($user_id);

        // LemonwayAccount model
        $lemonway_account_model = LemonwayAccount::findOne($user_id);

        // AssetFile model
        $asset_file_model = AssetFile::findOne($file_id);
        
        if ( $user_model && $lemonway_account_model && $asset_file_model && $asset_file_model->load_file() )
        {
            // Input parameters
            $vec_input = [
                'name'      => $asset_file_model->file_name,
                'type'      => $document_type,
                'buffer'    => base64_encode($asset_file_model->file->getContents()),
            ];

            // Send a "POST /v2/accounts/{accountid}/documents/upload" request
            $response = $this->api->post_document($lemonway_account_model->lemonway_account_id, $vec_input);

            // Create a LemonwayDocument model with last received data
            if ( $this->api->is_last_action_success() )
            {
                $vec_response = $this->api->get_response_body(true);

                if ( $this->is_debug )
                {
                    Log::lemonway_dev("LemonwayManager::upload_document({$lemonway_account_model->lemonway_account_id}, {$file_id}, {$document_type}) - Last action success");
                    Log::lemonway_dev(print_r($vec_response, true));
                }

                // Update account
                if ( !empty($vec_response) && isset($vec_response['uploadDocument']) && isset($vec_response['uploadDocument']['id']) )
                {
                    // Avoid to create 2 LemonwayDocument with same Primary Key
                    $lemonway_document_model = LemonwayDocument::get()->where([
                        'user_id' => $user_id,
                        'file_id' => $file_id,
                    ])->one();

                    if ( ! $lemonway_document_model )
                    {
                        $lemonway_document_model = Yii::createObject(LemonwayDocument::class);
                        $lemonway_document_model->user_id = $user_id;
                        $lemonway_document_model->file_id = $file_id;
                    }

                    $lemonway_document_model->setAttributes([
                        'lemonway_id'           => $vec_response['uploadDocument']['id'],
                        'name'                  => $vec_input['name'],
                        'status_type'           => $vec_response['uploadDocument']['status'],
                        'substatus_type'        => isset($vec_response['uploadDocument']['substatus']) ? $vec_response['uploadDocument']['substatus'] : 0,
                        'document_type'         => $vec_input['type'],
                        'last_sync_date'        => time(),
                        'last_sync_endpoint'    => 'PUT___'. $this->api->last_endpoint
                    ]);
                    if ( ! $lemonway_document_model->save() )
                    {
                        Log::save_model_error($lemonway_document_model);
                    }
                    else
                    {
                        // Save entity information (useful for logs)
                        $this->api->save_entity_info('LemonwayDocument', $lemonway_document_model->lemonway_id);
                    }

                    // Return LemonwayDocument model
                    return $lemonway_document_model;
                }
                else if ( $this->is_debug )
                {
                    Log::lemonway_dev("LemonwayManager::upload_document({$lemonway_account_model->lemonway_account_id}, {$file_id}, {$document_type}) - Incorrect response: ". print_r($vec_response, true));
                }
            }
            else if ( $this->is_debug )
            {
                Log::lemonway_dev("LemonwayManager::upload_document({$lemonway_account_model->lemonway_account_id}, {$file_id}, {$document_type}) - Last action error: ". print_r($response, true));
            }
        }

        else
        {
            if ( ! $user_model )
            {
                Log::lemonway_error("LemonwayManager::upload_document({$user_id}, {$file_id}) - USER does not exist: {$user_id}");
            }

            if ( ! $lemonway_account_model )
            {
                Log::lemonway_error("LemonwayManager::upload_document({$user_id}, {$file_id}) - LEMONWAY ACCOUNT does not exist: {$user_id}");
            }

            if ( ! $asset_file_model )
            {
                Log::lemonway_error("LemonwayManager::upload_document({$user_id}, {$file_id}) - ASSET FILE does not exist: {$file_id}");
            }

            else if ( ! $asset_file_model->load_file() )
            {
                Log::lemonway_error("LemonwayManager::upload_document({$user_id}, {$file_id}) - ASSET FILE could not be loaded: {$file_id}");
            }
        }

        return null;
    }


    /**
     * Return an array of LemonwayDocument models associated with a Lemonway account
     * 
     * - API REST Endpoint "GET /v2/accounts/{accountid}/documents"
     * 
     * @see LemonwayApi::get_documents()
     */
    public function get_documents($user_id)
    {   
        // User model
        $user_model = User::findOne($user_id);

        // LemonwayAccount model
        $lemonway_account_model = LemonwayAccount::findOne($user_id);

        // Array with LemonwayDocument models
        $vec_document_models = [];
        
        if ( $user_model && $lemonway_account_model )
        {
            // Send a "GET /v2/accounts/{accountid}/documents" request
            $response = $this->api->get_documents($lemonway_account_model->lemonway_account_id);

            // Return all the returned documents
            if ( $this->api->is_last_action_success() )
            {
                $vec_response = $this->api->get_response_body(true);
                
                if ( $this->is_debug )
                {
                    Log::lemonway_dev("LemonwayManager::get_documents({$lemonway_account_model->lemonway_account_id}) - Last action success");
                    Log::lemonway_dev(print_r($vec_response, true));
                }

                // Check & save documents status
                if ( !empty($vec_response) && isset($vec_response['documents']) && !empty($vec_response['documents']) && is_array($vec_response['documents']) )
                {
                    if ( $this->is_debug )
                    {
                        Log::lemonway_dev("Douments uploaded for user_id = ". $user_id .": ". print_r($vec_response['documents'], true));
                    }
                    foreach ( $vec_response['documents'] as $que_document )
                    {
                        if ( isset($que_document['id']) )
                        {
                            $lemonway_document_model = LemonwayDocument::get()->where(['lemonway_id' => $que_document['id']])->one();
                            if ( $lemonway_document_model )
                            {
                                $lemonway_document_model->setAttributes([
                                    'status_type'           => $que_document['status'],
                                    // 'document_type'     => $que_document['type'],
                                    'comment'               => null,
                                    'last_sync_date'        => time(),
                                    'last_sync_endpoint'    => 'GET___'. $this->api->last_endpoint
                                ]);

                                // Comments
                                if ( isset($que_document['comment']) && !empty($que_document['comment']) )
                                {
                                    $lemonway_document_model->comment = $que_document['comment'];
                                }

                                // Validity date
                                if ( isset($que_document['validity_date']) )
                                {
                                    $lemonway_document_model->validity_date = $que_document['validity_date'];
                                }

                                if ( ! $lemonway_document_model->save() )
                                {
                                    Log::save_model_error($lemonway_document_model);
                                }

                                // Add document into output array
                                $vec_document_models[$lemonway_document_model->file_id] = $lemonway_document_model;
                            }
                            else if ( $this->is_debug )
                            {
                                Log::lemonway_dev("Document not found - lemonway_id = ". $que_document['id']);
                            }
                        }
                    }
                    
                }
                else if ( $this->is_debug )
                {
                    Log::lemonway_dev("LemonwayManager::get_documents({$lemonway_account_model->lemonway_account_id}) - Incorrect response: ". print_r($vec_response, true));
                }
            }
            else if ( $this->is_debug )
            {
                Log::lemonway_dev("LemonwayManager::get_documents({$lemonway_account_model->lemonway_account_id}) - Last action error: ". print_r($response, true));
            }
        }
        else
        {
            if ( ! $user_model )
            {
                Log::lemonway_error("LemonwayManager::get_documents({$user_id}) - USER does not exist: {$user_id}");
            }

            if ( ! $lemonway_account_model )
            {
                Log::lemonway_error("LemonwayManager::get_documents({$user_id}) - LEMONWAY ACCOUNT does not exist: {$user_id}");
            }
        }

        return $vec_document_models;   
    }


    /**
     * Add an IBAN to a payment account for Money-Outs
     * 
     * - API REST Endpoint "POST /v2/moneyouts/iban"
     * 
     * @see LemonwayApi::post_iban()
     */
    public function create_iban($user_id, $vec_input, $bank_account_id = null)
    {
        // User model       
        $user_model = User::findOne($user_id);

        // LemonwayAccount model
        $lemonway_account_model = LemonwayAccount::findOne($user_id);

        if ( $user_model && $lemonway_account_model )
        {
            // Parameter 'accountId' is required
            if ( !isset($vec_input['accountId']) )
            {
                $vec_input['accountId'] = $lemonway_account_model->lemonway_account_id;
            }

            // Send a "POST /v2/moneyouts/iban" request
            $response = $this->api->post_iban($lemonway_account_model->lemonway_account_id, $vec_input);

            // Create a LemonwayIban model with last received data
            if ( $this->api->is_last_action_success() )
            {
                $vec_response = $this->api->get_response_body(true);
                
                if ( $this->is_debug )
                {
                    Log::lemonway_dev("LemonwayManager::create_iban({$lemonway_account_model->lemonway_account_id}) - Last action success");
                    Log::lemonway_dev(print_r($vec_response, true));
                }

                // Update account
                if ( !empty($vec_response) && isset($vec_response['ibanId']) && isset($vec_response['status']) )
                {
                    $lemonway_iban_model = Yii::createObject(LemonwayIban::class);
                    $lemonway_iban_model->setAttributes([
                        'lemonway_iban_id'      => $vec_response['ibanId'],
                        'user_id'               => $user_id,
                        'bank_account_id'       => $bank_account_id,
                        'status_type'           => $vec_response['status'],
                        'iban_type'             => 1,
                        'holder'                => $vec_input['holder'],
                        'bic'                   => $vec_input['bic'],
                        'iban'                  => $vec_input['iban'],
                        'comment'               => isset($vec_input['comment']) ? $vec_input['comment'] : null,
                        'last_sync_date'        => time(),
                        'last_sync_endpoint'    => 'POST___'. $this->api->last_endpoint
                    ]);
                    if ( ! $lemonway_iban_model->save() )
                    {
                        Log::save_model_error($lemonway_iban_model);
                    }
                    else
                    {
                        // Save entity information (useful for logs)
                        $this->api->save_entity_info('LemonwayIban', $lemonway_iban_model->lemonway_iban_id);
                    }

                    // Return LemonwayIban model
                    return $lemonway_iban_model;
                }
                else if ( $this->is_debug )
                {
                    Log::lemonway_dev("LemonwayManager::create_iban({$lemonway_account_model->lemonway_account_id}) - Incorrect response: ". print_r($vec_response, true));
                }
            }
            else if ( $this->is_debug )
            {
                Log::lemonway_dev("LemonwayManager::create_iban({$lemonway_account_model->lemonway_account_id}) - Last action error: ". print_r($response, true));
            }
        }
        else
        {
            if ( ! $user_model )
            {
                Log::lemonway_error("LemonwayManager::create_iban({$user_id}) - USER does not exist: {$user_id}");
            }

            if ( ! $lemonway_account_model )
            {
                Log::lemonway_error("LemonwayManager::create_iban({$user_id}) - LEMONWAY ACCOUNT does not exist: {$user_id}");
            }
        }

        return null;
    }


    /**
     * Return an array of LemonwayIban models associated with a Lemonway account
     * 
     * - API REST Endpoint "GET /v2/moneyouts/{accountid}/iban"
     * 
     * @see LemonwayApi::get_ibans()
     */
    public function get_ibans($user_id)
    {
        // User model
        $user_model = User::findOne($user_id);

        // LemonwayAccount model
        $lemonway_account_model = LemonwayAccount::findOne($user_id);

        // Array with LemonwayIban models
        $vec_iban_models = [];
        
        if ( $user_model && $lemonway_account_model )
        {
            // Send a "GET /v2/moneyouts/{accountid}/iban" request
            $response = $this->api->get_ibans($lemonway_account_model->lemonway_account_id);

            // Return all the LemonwayIban models
            if ( $this->api->is_last_action_success() )
            {
                $vec_response = $this->api->get_response_body(true);
                
                if ( $this->is_debug )
                {
                    Log::lemonway_dev("LemonwayManager::get_ibans({$lemonway_account_model->lemonway_account_id}) - Last action success");
                    Log::lemonway_dev(print_r($vec_response, true));
                }

                // Update account
                if ( !empty($vec_response) && isset($vec_response['ibans']) )
                {
                    if ( !empty($vec_response['ibans']) && is_array($vec_response['ibans']) )
                    {
                        foreach ( $vec_response['ibans'] as $que_iban )
                        {
                            if ( isset($que_iban['id']) )
                            {
                                $lemonway_iban_model = LemonwayIban::findOne($que_iban['id']);
                                if ( $lemonway_iban_model )
                                {
                                    $lemonway_iban_model->setAttributes([
                                        'status_type'           => $que_iban['status'],
                                        'iban'                  => $que_iban['iban'],
                                        'bic'                   => $que_iban['swift'],
                                        'holder'                => $que_iban['holder'],
                                        'last_sync_date'        => time(),
                                        'last_sync_endpoint'    => 'GET___'. $this->api->last_endpoint
                                    ]);
                                    if ( isset($que_iban['type']) )
                                    {
                                        $lemonway_iban_model->iban_type = $que_iban['type'];
                                    }

                                    if ( ! $lemonway_iban_model->save() )
                                    {
                                        Log::save_model_error($lemonway_iban_model);
                                    }

                                    // Add IBAN into output array
                                    $vec_iban_models[$que_iban['id']] = $lemonway_iban_model;
                                }
                                else if ( $this->is_debug )
                                {
                                    Log::lemonway_dev("LemonwayIban model not found - iban_id = ". $que_iban['id']);
                                }
                            }
                        }
                    }
                }
                else if ( $this->is_debug )
                {
                    Log::lemonway_dev("LemonwayManager::get_ibans({$lemonway_account_model->lemonway_account_id}) - Incorrect response: ". print_r($vec_response, true));
                }
            }
            else if ( $this->is_debug )
            {
                Log::lemonway_dev("LemonwayManager::get_ibans({$lemonway_account_model->lemonway_account_id}) - Last action error: ". print_r($response, true));
            }
        }
        else
        {
            if ( ! $user_model )
            {
                Log::lemonway_error("LemonwayManager::get_ibans({$user_id}) - USER does not exist: {$user_id}");
            }

            if ( ! $lemonway_account_model )
            {
                Log::lemonway_error("LemonwayManager::get_ibans({$user_id}) - LEMONWAY ACCOUNT does not exist: {$user_id}");
            }
        }

        return $vec_iban_models;   
    }


    /**
     * Disable Bank Information (IBAN) from a payment account
     * 
     * - API REST Endpoint "PUT /v2/moneyouts/iban/{IbanId}/unregister"
     * 
     * @see LemownayApi::disable_iban()
     */
    public function disable_iban($iban_id)
    {
        // LemonwayIban model
        $lemonway_iban_model = LemonwayIban::findOne($iban_id);
        
        if ( $lemonway_iban_model && $lemonway_iban_model->lemonwayAccount )
        {
            // Send a "PUT /v2/moneyouts/iban/{IbanId}/unregister" request
            $response = $this->api->disable_iban($lemonway_iban_model->lemonway_iban_id, $lemonway_iban_model->lemonwayAccount->lemonway_account_id);

            // Update the LemonwayIban model with last received data
            if ( $this->api->is_last_action_success() )
            {
                $vec_response = $this->api->get_response_body(true);
                
                if ( $this->is_debug )
                {
                    Log::lemonway_dev("LemonwayManager::disable_iban({$iban_id}) for account {$lemonway_iban_model->lemonwayAccount->lemonway_account_id} - Last action success");
                    Log::lemonway_dev(print_r($vec_response, true));
                }

                // Update & disable LemonwayIban model
                if ( !empty($vec_response) && isset($vec_response['id']) )
                {
                    $lemonway_iban_model->last_sync_date = time();
                    $lemonway_iban_model->last_sync_endpoint = 'PUT___'. $this->api->last_endpoint;
                    if ( $lemonway_iban_model->disable() )
                    {
                        return true;
                    }
                    else
                    {
                        Log::save_model_error($lemonway_iban_model);
                    }
                }
                else if ( $this->is_debug )
                {
                    Log::lemonway_dev("LemonwayManager::disable_iban({$iban_id}) for account {$lemonway_iban_model->lemonwayAccount->lemonway_account_id} - Incorrect response: ". print_r($vec_response, true));
                }
            }
            else if ( $this->is_debug )
            {
                Log::lemonway_dev("LemonwayManager::disable_iban({$iban_id}) for account {$lemonway_iban_model->lemonwayAccount->lemonway_account_id} - Last action error: ". print_r($response, true));
            }

            return $response;
        }
        else
        {
            if ( ! $lemonway_iban_model )
            {
                Log::lemonway_error("LemonwayManager::disable_iban({$iban_id}) - LEMONWAY IBAN does not exist: {$iban_id}");
            }

            else if ( ! $lemonway_iban_model->lemonwayAccount )
            {
                Log::lemonway_error("LemonwayManager::disable_iban({$iban_id}) - LEMONWAY ACCOUNT does not exist: {$lemonway_iban_model->user_id}");
            }
        }

        return false;   
    }


    /**
     * Create a Dedicated Virtual IBAN
     * 
     * - API REST Endpoint "POST /v2/moneyins/bankwire/iban/create"
     * 
     * @see LemonwayApi::post_virtual_iban()
     */
    public function create_virtual_iban($user_id, $vec_input = [])
    {
        // User model       
        $user_model = User::findOne($user_id);

        // LemonwayAccount model
        $lemonway_account_model = LemonwayAccount::findOne($user_id);

        if ( $user_model && $lemonway_account_model )
        {
            // Parameter 'wallet' is required
            if ( !isset($vec_input['wallet']) )
            {
                $vec_input['wallet'] = $lemonway_account_model->lemonway_account_id;
            }

            // Send a "POST /v2/moneyins/bankwire/iban/create" request
            $response = $this->api->post_virtual_iban($lemonway_account_model->lemonway_account_id, $vec_input);

            // Create a LemonwayIban model with last received data
            if ( $this->api->is_last_action_success() )
            {
                $vec_response = $this->api->get_response_body(true);
                
                if ( $this->is_debug )
                {
                    Log::lemonway_dev("LemonwayManager::create_virtual_iban({$lemonway_account_model->lemonway_account_id}) - Last action success");
                    Log::lemonway_dev(print_r($vec_response, true));
                }

                // Update account
                if ( !empty($vec_response) && isset($vec_response['id']) && isset($vec_response['status']) )
                {
                    $vec_options_json = [
                        'maxAvailableIbanPerWallet' => isset($vec_response['maxAvailableIbanPerWallet']) ? $vec_response['maxAvailableIbanPerWallet'] : null,
                        'maxAvailableIban' => isset($vec_response['maxAvailableIban']) ? $vec_response['maxAvailableIban'] : null,
                        'country' => isset($vec_input['country']) ? $vec_input['country'] : null,
                    ];

                    $lemonway_iban_model = Yii::createObject(LemonwayIban::class);
                    $lemonway_iban_model->setAttributes([
                        'lemonway_iban_id'      => $vec_response['id'],
                        'user_id'               => $user_id,
                        'is_virtual'            => 1,
                        'status_type'           => $vec_response['status'],
                        'iban_type'             => 2,
                        'holder'                => $vec_response['holder'],
                        'bic'                   => $vec_response['bic'],
                        'iban'                  => $vec_response['iban'],
                        'domiciliation1'        => isset($vec_response['domiciliation']) ? $vec_response['domiciliation'] : null,
                        'comment'               => 'Virtual IBAN',
                        'options_json'          => Json::encode($vec_options_json),
                        'last_sync_date'        => time(),
                        'last_sync_endpoint'    => 'POST___'. $this->api->last_endpoint
                    ]);
                    if ( ! $lemonway_iban_model->save() )
                    {
                        Log::save_model_error($lemonway_iban_model);
                    }
                    else
                    {
                        // Reload model
                        $lemonway_iban_model = LemonwayIban::model()->cache(0)->findByPk($lemonway_iban_model->lemonway_iban_id);

                        // Save entity information (useful for logs)
                        $this->api->save_entity_info('LemonwayIban', $lemonway_iban_model->lemonway_iban_id);

                        // Upload PDF document file (encoded in base64)
                        if ( isset($vec_response['pdf']) && !empty($vec_response['pdf']) )
                        {
                            $asset_file_model = $lemonway_iban_model->create_pdf_file($vec_response['pdf']);
                        }

                        // Upload QR CODE image file (encoded in base64)
                        if ( isset($vec_response['qrCode']) && !empty($vec_response['qrCode']) )
                        {
                            $asset_image_model = $lemonway_iban_model->create_image_file($vec_response['qrCode']);
                        }
                    }

                    // Return LemonwayIban model
                    return $lemonway_iban_model;
                }
                else if ( $this->is_debug )
                {
                    Log::lemonway_dev("LemonwayManager::create_virtual_iban({$lemonway_account_model->lemonway_account_id}) - Incorrect response: ". print_r($vec_response, true));
                }
            }
            else if ( $this->is_debug )
            {
                Log::lemonway_dev("LemonwayManager::create_virtual_iban({$lemonway_account_model->lemonway_account_id}) - Last action error: ". print_r($response, true));
            }
        }
        else
        {
            if ( ! $user_model )
            {
                Log::lemonway_error("LemonwayManager::create_virtual_iban({$user_id}) - USER does not exist: {$user_id}");
            }

            if ( ! $lemonway_account_model )
            {
                Log::lemonway_error("LemonwayManager::create_virtual_iban({$user_id}) - LEMONWAY ACCOUNT does not exist: {$user_id}");
            }
        }

        return null;
    }


    /**
     * Disable a Dedicated Virtual IBAN
     * 
     * - API REST Endpoint "POST /v2/moneyins/bankwire/iban/{ibanid}/disable"
     * 
     * @see LemownayApi::disable_virtual_iban()
     */
    public function disable_virtual_iban($iban_id)
    {
        // LemonwayIban model
        $lemonway_iban_model = LemonwayIban::findOne($iban_id);
        
        if ( $lemonway_iban_model && $lemonway_iban_model->lemonwayAccount )
        {
            // Send a "POST /v2/moneyins/bankwire/iban/{ibanid}/disable" request
            $response = $this->api->disable_virtual_iban($lemonway_iban_model->lemonway_iban_id, $lemonway_iban_model->lemonwayAccount->lemonway_account_id);

            // Update the LemonwayIban model with last received data
            if ( $this->api->is_last_action_success() )
            {
                $vec_response = $this->api->get_response_body(true);
                
                if ( $this->is_debug )
                {
                    Log::lemonway_dev("LemonwayManager::disable_virtual_iban({$iban_id}) for account {$lemonway_iban_model->lemonwayAccount->lemonway_account_id} - Last action success");
                    Log::lemonway_dev(print_r($vec_response, true));
                }

                // Update & disable LemonwayIban model
                if ( !empty($vec_response) && isset($vec_response['id']) )
                {
                    $lemonway_iban_model->status_type = isset($vec_response['status']) ? $vec_response['status'] : $lemonway_iban_model->status_type;
                    $lemonway_iban_model->last_sync_date = time();
                    $lemonway_iban_model->last_sync_endpoint = 'POST___'. $this->api->last_endpoint;
                    if ( $lemonway_iban_model->disable() )
                    {
                        return true;
                    }
                    else
                    {
                        Log::save_model_error($lemonway_iban_model);
                    }
                }
                else if ( $this->is_debug )
                {
                    Log::lemonway_dev("LemonwayManager::disable_virtual_iban({$iban_id}) for account {$lemonway_iban_model->lemonwayAccount->lemonway_account_id} - Incorrect response: ". print_r($vec_response, true));
                }
            }
            else if ( $this->is_debug )
            {
                Log::lemonway_dev("LemonwayManager::disable_virtual_iban({$iban_id}) for account {$lemonway_iban_model->lemonwayAccount->lemonway_account_id} - Last action error: ". print_r($response, true));
            }

            return $response;
        }
        else
        {
            if ( ! $lemonway_iban_model )
            {
                Log::lemonway_error("LemonwayManager::disable_virtual_iban({$iban_id}) - LEMONWAY IBAN does not exist: {$iban_id}");
            }

            else if ( ! $lemonway_iban_model->lemonwayAccount )
            {
                Log::lemonway_error("LemonwayManager::disable_virtual_iban({$iban_id}) - LEMONWAY ACCOUNT does not exist: {$lemonway_iban_model->user_id}");
            }
        }

        return false;   
    }


    /**
     * Initiate a Web PSP Payment
     * 
     * - API REST Endpoint "POST /v2/moneyins/card/webinit"
     * 
     * @see LemownayApi::post_moneyin_webinit()
     */
    public function webinit_money_in($user_id, $amount, $vec_input = [])
    {
        // User model       
        $user_model = User::findOne($user_id);

        // LemonwayAccount model
        $lemonway_account_model = LemonwayAccount::findOne($user_id);
        
        if ( $user_model && $lemonway_account_model )
        {
            // Create LemonwayTransaction model before send to API
            $lemonway_transaction_model = Yii::createObject(LemonwayTransaction::class);
            $lemonway_transaction_model->setAttributes([
                'receiver_user_id'              => $user_id,
                'receiver_lemonway_account_id'  => $lemonway_account_model->lemonway_account_id,
                'total_amount'                  => $amount * 100,
                'transaction_type'              => 'in',
                'transaction_date'              => time(),
                'comment'                       => isset($vec_input['comment']) ? $vec_input['comment'] : null,
                'is_auto_commission'            => isset($vec_input['autoCommission']) ? $vec_input['autoCommission'] : 0,
            ]);
            if ( ! $lemonway_transaction_model->save() )
            {
                Log::save_model_error($lemonway_transaction_model);
                Log::lemonway_error("LemonwayManager::moneyin_webinit({$lemonway_account_model->lemonway_account_id}) - LemonwayTransaction could not be created");
            }
            else
            {
                // Save entity information (useful for logs)
                $this->api->save_entity_info('LemonwayTransaction', $lemonway_transaction_model->transaction_id);

                // Transaction reference sent to LemonwayAPI (our ID)
                $transaction_reference = $lemonway_transaction_model->transaction_id;

                // Prepare input parameters
                $vec_input['returnUrl']         = $this->get_webkit_redirect_url('return', $lemonway_transaction_model->transaction_id);
                $vec_input['errorUrl']          = $this->get_webkit_redirect_url('error', $lemonway_transaction_model->transaction_id);
                $vec_input['cancelUrl']         = $this->get_webkit_redirect_url('cancel', $lemonway_transaction_model->transaction_id);
                $vec_input['label']             = isset($vec_input['label']) ? $vec_input['label'] : Yii::app()->name;
                $vec_input['accountId']         = $lemonway_account_model->lemonway_account_id;
                $vec_input['totalAmount']       = $lemonway_transaction_model->total_amount;
                $vec_input['autoCommission']    = $lemonway_transaction_model->is_auto_commission > 0 ? true : false;
                $vec_input['reference']         = $transaction_reference;
                $vec_input['comment']           = $lemonway_transaction_model->comment;

                // Send a "POST /v2/moneyins/card/webinit" request
                $response = $this->api->post_moneyin_webinit($lemonway_account_model->lemonway_account_id, $vec_input);

                // Update the LemonwayIban model with last received data
                if ( $this->api->is_last_action_success() )
                {
                    $vec_response = $this->api->get_response_body(true);
                
                    if ( $this->is_debug )
                    {
                        Log::lemonway_dev("LemonwayManager::moneyin_webinit({$lemonway_account_model->lemonway_account_id}) - Last action success: ". print_r($vec_response, true));
                    }

                    // Transaction information is returned
                    if ( !empty($vec_response) && isset($vec_response['id']) && isset($vec_response['webKitToken']) )
                    {
                        $lemonway_transaction_model->setAttributes([
                            'lemonway_id'               => $vec_response['id'],
                            'reference'                 => $transaction_reference,
                            'request_token'             => $vec_response['webKitToken'],
                            'response_json'             => Json::encode($vec_response),
                            'request_webkit_json'       => Json::encode($vec_response),
                            'last_sync_date'            => time(),
                            'last_sync_endpoint'        => 'POST___'. $this->api->last_endpoint
                        ]);
                        if ( ! $lemonway_transaction_model->save() )
                        {
                            Log::save_model_error($lemonway_transaction_model);
                            Log::lemonway_error("LemonwayManager::moneyin_webinit({$lemonway_account_model->lemonway_account_id}) - LemonwayTransaction could not be updated after API response: ". print_r($lemonway_transaction_model->getAttributes()));
                        }
                    }
                    else if ( $this->is_debug )
                    {
                        Log::lemonway_error("LemonwayManager::moneyin_webinit({$lemonway_account_model->lemonway_account_id}) - Incorrect response: ". print_r($vec_response, true));
                    }
                }

                // Error information is returned
                else
                {
                    $vec_error = $this->api->get_last_error();
                    if ( !empty($vec_error) )
                    {
                        $vec_response = $this->api->get_response_body(true);
                        $lemonway_transaction_model->setAttributes([
                            'error_code'            => isset($vec_error['code']) ? $vec_error['code'] : null,
                            'error_message'         => isset($vec_error['message']) ? $vec_error['message'] : null,
                            'psp_message'           => isset($vec_error['psp']) && isset($vec_error['psp']['message']) ? $vec_error['psp']['message'] : null,
                            'response_json'         => Json::encode($vec_response),
                            'last_sync_date'        => time(),
                            'last_sync_endpoint'    => 'POST___'. $this->api->last_endpoint
                        ]);
                        if ( ! $lemonway_transaction_model->save() )
                        {
                            Log::save_model_error($lemonway_transaction_model);
                            Log::lemonway_error("LemonwayManager::moneyin_webinit({$lemonway_account_model->lemonway_account_id}) - LemonwayTransaction could not be updated after API response (with error): ". print_r($lemonway_transaction_model->getAttributes()));
                        }
                    }

                    if ( $this->is_debug )
                    {
                        Log::lemonway_dev("LemonwayManager::moneyin_webinit({$lemonway_account_model->lemonway_account_id}) - Last action error: ". print_r($response, true));
                    }
                }

                return $lemonway_transaction_model;
            }
        }
        else
        {
            if ( ! $user_model )
            {
                Log::lemonway_error("LemonwayManager::moneyin_webinit({$user_id}, {$amount}) - USER does not exist: {$user_id}");
            }

            if ( ! $lemonway_account_model )
            {
                Log::lemonway_error("LemonwayManager::moneyin_webinit({$user_id}, {$amount}) - LEMONWAY ACCOUNT does not exist: {$user_id}");
            }
        }

        return null;
    }


    /**
     * Returns the URL called by Lemonway Webkit after submitting the payment
     */
    public function get_webkit_redirect_url($response_type, $transaction_id = '')
    {
        // Lemonway configuration
        if ( empty($this->vec_config) )
        {
            $this->vec_config = Yii::app()->config->get('components.lemonway');
        }

        switch ($response_type)
        {
            // Redirected URL after cancel
            case 'cancel':
                return Url::to($this->vec_config['cancel_webkit_url'], ['transaction_id' => $transaction_id]);
            break;

            // Redirected URL after error
            case 'error':
                return Url::to($this->vec_config['error_webkit_url'], ['transaction_id' => $transaction_id]);
            break;

            // Redirected URL after success
            default:
                return Url::to($this->vec_config['return_webkit_url'], ['transaction_id' => $transaction_id]);
            break;
        }
    }


    /**
     * Generate the Lemonway Webkit URL where user will pay via bank card
     */
    public function generate_webkit_url($user_id, $amount, $vec_input = [])
    {
        $lemonway_transaction_model = $this->webinit_money_in($user_id, $amount, $vec_input);
        if ( $lemonway_transaction_model && !empty($lemonway_transaction_model->request_token) )
        {
            return $this->api->webkit_url .'?moneyintoken='. $lemonway_transaction_model->request_token;
        }

        return null;
    }


    /**
     * Internal transfer between Lemonway payments accounts
     * 
     * - API REST Endpoint "POST /v2/p2p"
     * 
     * @see LemownayApi::post_money_p2p()
     */
    public function send_money_p2p($sender_user_id, $receiver_user_id, $amount, $vec_input = [])
    {   
        // Sender user model       
        $sender_user_model = User::findOne($sender_user_id);

        // Sender LemonwayAccount model
        $sender_account_model = LemonwayAccount::findOne($sender_user_id);

        // Receiver user model       
        $receiver_user_model = User::findOne($receiver_user_id);

        // Receiver LemonwayAccount model
        $receiver_account_model = LemonwayAccount::findOne($receiver_user_id);
        
        if ( $sender_user_model && $sender_account_model && $receiver_user_model && $receiver_account_model )
        {
            // Create LemonwayTransaction model before send to API
            $lemonway_transaction_model = Yii::createObject(LemonwayTransaction::class);
            $lemonway_transaction_model->setAttributes([
                'receiver_user_id'              => $receiver_user_id,
                'receiver_lemonway_account_id'  => $receiver_account_model->lemonway_account_id,
                'sender_user_id'                => $sender_user_id,
                'sender_lemonway_account_id'    => $sender_account_model->lemonway_account_id,
                'total_amount'                  => $amount * 100,
                'transaction_type'              => 'p2p',
                'transaction_date'              => time(),
                'comment'                       => isset($vec_input['comment']) ? $vec_input['comment'] : null,
            ]);
            if ( ! $lemonway_transaction_model->save() )
            {
                Log::save_model_error($lemonway_transaction_model);
                Log::lemonway_error("LemonwayManager::send_money_p2p({$sender_account_model->lemonway_account_id}, {$receiver_account_model->lemonway_account_id}) - LemonwayTransaction could not be created");
            }
            else
            {
                // Save entity information (useful for logs)
                $this->api->save_entity_info('LemonwayTransaction', $lemonway_transaction_model->transaction_id);
                
                // Transaction reference sent to LemonwayAPI (our ID)
                $transaction_reference = $lemonway_transaction_model->transaction_id;

                // Prepare input parameters
                $vec_input['debitAccountId']    = $sender_account_model->lemonway_account_id;
                $vec_input['creditAccountId']   = $receiver_account_model->lemonway_account_id;
                $vec_input['amount']            = $lemonway_transaction_model->total_amount;
                $vec_input['reference']         = $transaction_reference;
                $vec_input['comment']           = $lemonway_transaction_model->comment;

                // Send a "POST /v2/p2p" request
                $response = $this->api->post_money_p2p($sender_account_model->lemonway_account_id, $receiver_account_model->lemonway_account_id, $vec_input);

                // Update the LemonwayIban model with last received data
                if ( $this->api->is_last_action_success() )
                {
                    $vec_response = $this->api->get_response_body(true);

                    if ( $this->is_debug )
                    {
                        Log::lemonway_dev("LemonwayManager::send_money_p2p({$sender_account_model->lemonway_account_id}, {$receiver_account_model->lemonway_account_id}) - Last action success: ". print_r($vec_response, true));
                    }

                    // Transaction information is returned
                    if ( !empty($vec_response) && isset($vec_response['transaction']) && isset($vec_response['transaction']['id']) )
                    {                        
                        $lemonway_transaction_model->setAttributes([
                            'lemonway_id'               => $vec_response['transaction']['id'],
                            'reference'                 => isset($vec_response['transaction']['reference']) ? $vec_response['transaction']['reference'] :null,
                            'total_amount'              => $vec_response['transaction']['creditAmount'],
                            'commission_amount'         => isset($vec_response['transaction']['commissionAmount']) ? $vec_response['transaction']['commissionAmount'] : 0,
                            'method_type'               => isset($vec_response['transaction']['method']) ? $vec_response['transaction']['method'] : null,
                            'status_type'               => isset($vec_response['transaction']['status']) ? $vec_response['transaction']['status'] : null,
                            'transaction_date'          => isset($vec_response['transaction']['date']) ? $vec_response['transaction']['date'] : time(),
                            'execution_date'            => isset($vec_response['transaction']['executionDate']) ? $vec_response['transaction']['executionDate'] : null,
                            'comment'                   => isset($vec_response['transaction']['comment']) && !empty($vec_response['transaction']['comment']) ? $vec_response['transaction']['comment'] : null,
                            'commission_transaction_id' => isset($vec_response['transaction']['feeReference']) ? $vec_response['transaction']['feeReference'] : null,
                            'response_json'             => Json::encode($vec_response),
                            'last_sync_date'            => time(),
                            'last_sync_endpoint'        => 'POST___'. $this->api->last_endpoint
                        ]);
                        if ( ! $lemonway_transaction_model->save() )
                        {
                            Log::save_model_error($lemonway_transaction_model);
                            Log::lemonway_error("LemonwayManager::send_money_p2p({$sender_account_model->lemonway_account_id}, {$receiver_account_model->lemonway_account_id}) - LemonwayTransaction could not be updated after API response: ". print_r($lemonway_transaction_model->getAttributes()));
                        }
                    }
                    else if ( $this->is_debug )
                    {
                        Log::lemonway_error("LemonwayManager::send_money_p2p({$sender_account_model->lemonway_account_id}, {$receiver_account_model->lemonway_account_id}) - Incorrect response: ". print_r($vec_response, true));
                    }
                }

                // Error information is returned
                else
                {
                    $vec_error = $this->api->get_last_error();
                    if ( !empty($vec_error) )
                    {
                        $vec_response = $this->api->get_response_body(true);
                        $lemonway_transaction_model->setAttributes([
                            'error_code'            => isset($vec_error['code']) ? $vec_error['code'] : null,
                            'error_message'         => isset($vec_error['message']) ? $vec_error['message'] : null,
                            'psp_message'           => isset($vec_error['psp']) && isset($vec_error['psp']['message']) ? $vec_error['psp']['message'] : null,
                            'response_json'         => Json::encode($vec_response),
                            'last_sync_date'        => time(),
                            'last_sync_endpoint'    => 'POST___'. $this->api->last_endpoint
                        ]);
                        if ( ! $lemonway_transaction_model->save() )
                        {
                            Log::save_model_error($lemonway_transaction_model);
                            Log::lemonway_error("LemonwayManager::send_money_p2p({$sender_account_model->lemonway_account_id}, {$receiver_account_model->lemonway_account_id}) - LemonwayTransaction could not be updated after API response (with error): ". print_r($lemonway_transaction_model->getAttributes()));
                        }
                    }

                    if ( $this->is_debug )
                    {
                        Log::lemonway_dev("LemonwayManager::send_money_p2p({$sender_account_model->lemonway_account_id}, {$receiver_account_model->lemonway_account_id}) - Last action error: ". print_r($response, true));
                    }
                }

                return $lemonway_transaction_model;
            }
        }
        else
        {
            if ( ! $sender_user_model )
            {
                Log::lemonway_error("LemonwayManager::send_money_p2p({$sender_user_id}, {$receiver_user_id}) - Sender USER does not exist: {$sender_user_id}");
            }

            if ( ! $sender_account_model )
            {
                Log::lemonway_error("LemonwayManager::send_money_p2p({$sender_user_id}, {$receiver_user_id}) - Sender LEMONWAY ACCOUNT does not exist: {$sender_user_id}");
            }

            if ( ! $receiver_user_model )
            {
                Log::lemonway_error("LemonwayManager::send_money_p2p({$sender_user_id}, {$receiver_user_id}) - Receiver USER does not exist: {$receiver_user_id}");
            }

            if ( ! $receiver_account_model )
            {
                Log::lemonway_error("LemonwayManager::send_money_p2p({$sender_user_id}, {$receiver_user_id}) - Receiver LEMONWAY ACCOUNT does not exist: {$receiver_user_id}");
            }
        }

        return null;
    }


    /**
     * External fund transfer from a payment account to a bank account
     * 
     * - API REST Endpoint "POST /v2/moneyouts"
     * 
     * @see LemownayApi::post_moneyout
     */
    public function send_money_out($user_id, $amount, $iban_id = null, $vec_input = [])
    {
        // User model       
        $user_model = User::findOne($user_id);

        // LemonwayAccount model
        $lemonway_account_model = LemonwayAccount::findOne($user_id);
        
        if ( $user_model && $lemonway_account_model )
        {
            // Check if given "iban_id" exists
            $lemonway_iban_model = ( $iban_id !== null ) ? LemonwayIban::findOne($iban_id) : null;

            // Create LemonwayTransaction model before send to API
            $lemonway_transaction_model = Yii::createObject(LemonwayTransaction::class);
            $lemonway_transaction_model->setAttributes([
                'receiver_user_id'              => $user_id,
                'receiver_lemonway_account_id'  => $lemonway_account_model->lemonway_account_id,
                'receiver_lemonway_iban_id'     => $lemonway_iban_model !== null ? $lemonway_iban_model->lemonway_iban_id : null,
                'sender_user_id'                => $user_id,
                'sender_lemonway_account_id'    => $lemonway_account_model->lemonway_account_id,
                'total_amount'                  => $amount * 100,
                'transaction_type'              => 'out',
                'transaction_date'              => time(),
                'comment'                       => isset($vec_input['comment']) ? $vec_input['comment'] : null,
                'is_auto_commission'            => isset($vec_input['autoCommission']) ? $vec_input['autoCommission'] : 0,
            ]);
            if ( ! $lemonway_transaction_model->save() )
            {
                Log::save_model_error($lemonway_transaction_model);
                Log::lemonway_error("LemonwayManager::send_money_out({$lemonway_account_model->lemonway_account_id}) - LemonwayTransaction could not be created");
            }
            else
            {
                // Save entity information (useful for logs)
                $this->api->save_entity_info('LemonwayTransaction', $lemonway_transaction_model->transaction_id);
                
                // Transaction reference sent to LemonwayAPI (our ID)
                $transaction_reference = $lemonway_transaction_model->transaction_id;

                // Prepare input parameters
                $vec_input['accountId']         = $lemonway_account_model->lemonway_account_id;
                $vec_input['totalAmount']       = $lemonway_transaction_model->total_amount;
                $vec_input['autoCommission']    = $lemonway_transaction_model->is_auto_commission;
                $vec_input['reference']         = $transaction_reference;
                $vec_input['comment']           = $lemonway_transaction_model->comment;
                if ( $lemonway_transaction_model->receiver_lemonway_iban_id !== null )
                {
                    $vec_input['ibanId'] = $lemonway_transaction_model->receiver_lemonway_iban_id;
                }

                // Send a "POST /v2/moneyouts" request
                $response = $this->api->post_money_out($lemonway_account_model->lemonway_account_id, $vec_input);

                // Update the LemonwayTransaction model with last received data
                if ( $this->api->is_last_action_success() )
                {
                    $vec_response = $this->api->get_response_body(true);
                    
                    if ( $this->is_debug )
                    {
                        Log::lemonway_dev("LemonwayManager::send_money_out({$lemonway_account_model->lemonway_account_id}) - Last action success: ". print_r($vec_response, true));
                    }

                    // Transaction information is returned
                    if ( !empty($vec_response) && isset($vec_response['transaction']) && isset($vec_response['transaction']['id']) )
                    {
                        // IBAN receiver? Check if LemonwayIban model exists
                        $receiver_iban_id = null;
                        if ( isset($vec_response['transaction']['IbanId']) && !empty($vec_response['transaction']['IbanId']) )
                        {
                            $lemonway_iban_model = LemonwayIban::findOne($vec_response['transaction']['IbanId']);
                            if ( $lemonway_iban_model )
                            {
                                $receiver_iban_id = $lemonway_iban_model->lemonway_iban_id;
                            }
                        }

                        // Status and method are incorrect on API? The values are crossed
                        $method_type = isset($vec_response['transaction']['method']) ? $vec_response['transaction']['method'] : null;
                        $status_type = isset($vec_response['transaction']['status']) ? $vec_response['transaction']['status'] : null;
                        if ( $method_type !== null && $method_type !== 3 )
                        {
                            // Bank transfer (Money-Out)
                            $method_type = 3; 

                            // Status is really success
                            if ( $status_type !== null && $status_type === 3 )
                            {
                                $status_type = 0; 
                            }

                            // $vec_json_details['original_method'] = $vec_response['transaction']['method'];
                            // $vec_json_details['original_status'] = $vec_response['transaction']['status'];
                        }

                        $lemonway_transaction_model->setAttributes([
                            'lemonway_id'               => $vec_response['transaction']['id'],
                            'receiver_lemonway_iban_id' => $receiver_iban_id,
                            'reference'                 => isset($vec_response['transaction']['reference']) ? $vec_response['transaction']['reference'] :null,
                            'total_amount'              => $vec_response['transaction']['debitAmount'],
                            'commission_amount'         => isset($vec_response['transaction']['commissionAmount']) ? $vec_response['transaction']['commissionAmount'] : 0,
                            'method_type'               => $method_type,
                            'status_type'               => $status_type,
                            'transaction_date'          => isset($vec_response['transaction']['date']) ? $vec_response['transaction']['date'] : time(),
                            'execution_date'            => isset($vec_response['transaction']['executionDate']) ? $vec_response['transaction']['executionDate'] : null,
                            'comment'                   => isset($vec_response['transaction']['comment']) && !empty($vec_response['transaction']['comment']) ? $vec_response['transaction']['comment'] : null,
                            'original_transaction_id'   => isset($vec_response['transaction']['originId']) ? $vec_response['transaction']['originId'] : null,
                            'psp_message'               => isset($vec_response['transaction']['PSP']) && isset($vec_response['transaction']['PSP']['message']) ? $vec_response['transaction']['PSP']['message'] : null,
                            'response_json'             => Json::encode($vec_response),
                            'last_sync_date'            => time(),
                            'last_sync_endpoint'        => 'POST___'. $this->api->last_endpoint
                        ]);
                        if ( ! $lemonway_transaction_model->save() )
                        {
                            Log::save_model_error($lemonway_transaction_model);
                            Log::lemonway_error("LemonwayManager::send_money_out({$lemonway_account_model->lemonway_account_id}) - LemonwayTransaction could not be updated after API response: ". print_r($lemonway_transaction_model->getAttributes()));
                        }
                    }
                    else if ( $this->is_debug )
                    {
                        Log::lemonway_error("LemonwayManager::send_money_out({$lemonway_account_model->lemonway_account_id}) - Incorrect response: ". print_r($vec_response, true));
                    }
                }

                // Error information is returned
                else
                {
                    $vec_error = $this->api->get_last_error();
                    if ( !empty($vec_error) )
                    {
                        $vec_response = $this->api->get_response_body(true);
                        $lemonway_transaction_model->setAttributes([
                            'error_code'            => isset($vec_error['code']) ? $vec_error['code'] : null,
                            'error_message'         => isset($vec_error['message']) ? $vec_error['message'] : null,
                            'psp_message'           => isset($vec_error['psp']) && isset($vec_error['psp']['message']) ? $vec_error['psp']['message'] : null,
                            'response_json'         => Json::encode($vec_response),
                            'last_sync_date'        => time(),
                            'last_sync_endpoint'    => 'POST___'. $this->api->last_endpoint
                        ]);
                        if ( ! $lemonway_transaction_model->save() )
                        {
                            Log::save_model_error($lemonway_transaction_model);
                            Log::lemonway_error("LemonwayManager::send_money_out({$lemonway_account_model->lemonway_account_id}) - LemonwayTransaction could not be updated after API response (with error): ". print_r($lemonway_transaction_model->getAttributes()));
                        }
                    }

                    if ( $this->is_debug )
                    {
                        Log::lemonway_dev("LemonwayManager::send_money_out({$lemonway_account_model->lemonway_account_id}) - Last action error: ". print_r($response, true));
                    }
                }

                return $lemonway_transaction_model;
            }
        }
        else
        {
            if ( ! $user_model )
            {
                Log::lemonway_error("LemonwayManager::send_money_out({$user_id}, {$amount}) - USER does not exist: {$user_id}");
            }

            if ( ! $lemonway_account_model )
            {
                Log::lemonway_error("LemonwayManager::send_money_out({$user_id}, {$amount}) - LEMONWAY ACCOUNT does not exist: {$user_id}");
            }
        }

        return null;
    }


    /**
     * Return a Lemonway transaction (type = "IN") from the API
     * 
     * - API REST Endpoint "GET /v2/moneyins"
     * 
     * @see LemonwayApi::get_transaction_in()
     */
    public function get_transaction_in($transaction_id, $vec_input = [])
    {
        // Send a "GET /v2/moneyins" request
        $response = $this->api->get_transaction_in($transaction_id, $vec_input);

        // Update the LemonwayAccount model with last received data
        if ( $this->api->is_last_action_success() )
        {
            $vec_response = $this->api->get_response_body(true);
            
            if ( $this->is_debug )
            {
                Log::lemonway_dev("LemonwayManager::get_transaction_in({$transaction_id}) - Last action success");
                Log::lemonway_dev(print_r($vec_response, true));
            }

            // Create or update transaction data
            if ( !empty($vec_response) && isset($vec_response['transactions']) && isset($vec_response['transactions']['value']) && !empty($vec_response['transactions']['value']) )
            {
                foreach ( $vec_response['transactions']['value'] as $que_transaction )
                {
                    $lemonway_transaction_model = LemonwayTransaction::model()->findByLemonwayId($transaction_id, 'in');
                    if ( $lemonway_transaction_model )
                    {
                        return $lemonway_transaction_model->sync_from_lemonway($que_transaction);
                    }
                    else
                    {
                        Log::lemonway_warning("LemonwayManager::get_transaction_in({$transaction_id}) - Transaction IN does not exist on database with the ID {$transaction_id}. Try to create it");

                        // Try to register new transaction in our database
                        return $this->create_transaction('in', $que_transaction);
                    }
                }
            }
            else if ( $this->is_debug )
            {
                Log::lemonway_dev("LemonwayManager::get_transaction_in({$transaction_id}) - Incorrect response: ". print_r($vec_response, true));
            }
        }
        else if ( $this->is_debug )
        {
            Log::lemonway_dev("LemonwayManager::get_transaction_in({$transaction_id}) - Last action error: ". print_r($response, true));
        }

        return null;
    }


    /**
     * Return a Lemonway transaction (type = "OUT") from the API
     * 
     * - API REST Endpoint "GET /v2/moneyouts"
     * 
     * @see LemonwayApi::get_transaction_out()
     */
    public function get_transaction_out($transaction_id, $vec_input = [])
    {
        // Send a "GET /v2/moneyouts" request
        $response = $this->api->get_transaction_out($transaction_id, $vec_input);

        // Update the LemonwayAccount model with last received data
        if ( $this->api->is_last_action_success() )
        {
            $vec_response = $this->api->get_response_body(true);
            
            if ( $this->is_debug )
            {
                Log::lemonway_dev("LemonwayManager::get_transaction_out({$transaction_id}) - Last action success");
                Log::lemonway_dev(print_r($vec_response, true));
            }

            // Create or update transaction data
            if ( !empty($vec_response) && isset($vec_response['transactions']) && isset($vec_response['transactions']['value']) && !empty($vec_response['transactions']['value']) )
            {
                foreach ( $vec_response['transactions']['value'] as $que_transaction )
                {
                    $lemonway_transaction_model = LemonwayTransaction::model()->findByLemonwayId($transaction_id, 'out');
                    if ( $lemonway_transaction_model )
                    {
                        return $lemonway_transaction_model->sync_from_lemonway($que_transaction);
                    }
                    else
                    {  
                        Log::lemonway_warning("LemonwayManager::get_transaction_out({$transaction_id}) - Transaction OUT does not exist on database with the ID {$transaction_id}. Try to create it.");

                        // Try to register new transaction in our database
                        return $this->create_transaction('out', $que_transaction);
                    }
                }
            }
            else if ( $this->is_debug )
            {
                Log::lemonway_dev("LemonwayManager::get_transaction_out({$transaction_id}) - Incorrect response: ". print_r($vec_response, true));
            }
        }
        else if ( $this->is_debug )
        {
            Log::lemonway_dev("LemonwayManager::get_transaction_out({$transaction_id}) - Last action error: ". print_r($response, true));
        }

        return null;
    }



    /**
     * Return a Lemonway transaction (type = "P2P") from the API
     * 
     * - API REST Endpoint "GET /v2/p2p/{transactionid}"
     * 
     * @see LemonwayApi::get_transaction_p2p()
     */
    public function get_transaction_p2p($transaction_id, $vec_input = [])
    {
        // Send a "GET /v2/p2p/{transactionid}" request
        $response = $this->api->get_transaction_p2p($transaction_id, $vec_input);

        // Update the LemonwayAccount model with last received data
        if ( $this->api->is_last_action_success() )
        {
            $vec_response = $this->api->get_response_body(true);
            
            if ( $this->is_debug )
            {
                Log::lemonway_dev("LemonwayManager::get_transaction_p2p({$transaction_id}) - Last action success");
                Log::lemonway_dev(print_r($vec_response, true));
            }

            // Create or update transaction data
            if ( !empty($vec_response) && isset($vec_response['transactions']) && isset($vec_response['transactions']['value']) && !empty($vec_response['transactions']['value']) )
            {
                foreach ( $vec_response['transactions']['value'] as $que_transaction )
                {
                    $lemonway_transaction_model = LemonwayTransaction::model()->findByLemonwayId($transaction_id, 'p2p');
                    if ( $lemonway_transaction_model )
                    {
                        return $lemonway_transaction_model->sync_from_lemonway($que_transaction);
                    }
                    else
                    {
                        Log::lemonway_warning("LemonwayManager::get_transaction_p2p({$transaction_id}) - Transaction P2P does not exist on database with the ID {$transaction_id}. Try to create it.");

                        // Try to register new transaction in our database
                        return $this->create_transaction('p2p', $que_transaction);
                    }

                    return $lemonway_transaction_model->sync_from_lemonway($que_transaction);
                }
            }
            else if ( $this->is_debug )
            {
                Log::lemonway_dev("LemonwayManager::get_transaction_p2p({$transaction_id}) - Incorrect response: ". print_r($vec_response, true));
            }
        }
        else if ( $this->is_debug )
        {
            Log::lemonway_dev("LemonwayManager::get_transaction_p2p({$transaction_id}) - Last action error: ". print_r($response, true));
        }

        return null;
    }


    /**
     * Get list of all Payment Account Transactions
     * 
     * - API REST Endpoint "GET /v2/accounts/{accountId}/transactions"
     * 
     * @see LemonwayApi::get_account_transactions()
     */
    public function get_account_transactions($user_id, $vec_input = [])
    {
        // User model
        $user_model = User::findOne($user_id);

        // LemonwayAccount model
        $lemonway_account_model = LemonwayAccount::findOne($user_id);
        
        if ( $user_model && $lemonway_account_model )
        {
            // Send a "GET /v2/accounts/{accountId}/transactions" request
            $response = $this->api->get_account_transactions($lemonway_account_model->lemonway_account_id, $vec_input);

            // Update all the LemonwayTransaction models with last received data
            if ( $this->api->is_last_action_success() )
            {
                $vec_response = $this->api->get_response_body(true);
                
                if ( $this->is_debug )
                {
                    Log::lemonway_dev("LemonwayManager::get_account_transactions({$lemonway_account_model->lemonway_account_id}) - Last action success");
                    Log::lemonway_dev(print_r($vec_response, true));
                }

                // Update all transactions data
                if ( !empty($vec_response) && isset($vec_response['transactions']) && isset($vec_response['transactions']['value']) && !empty($vec_response['transactions']['value']) )
                {
                    $vec_transaction_models = [];

                    foreach ( $vec_response['transactions']['value'] as $que_transaction )
                    {
                        // Transaction IN
                        if ( isset($que_transaction['transactionIn']) && isset($que_transaction['transactionIn']['id']) )
                        {
                            $lemonway_transaction_model = LemonwayTransaction::model()->findByLemonwayId($que_transaction['transactionIn']['id'], 'in');
                            if ( $lemonway_transaction_model )
                            {
                                $vec_transaction_models[] = $lemonway_transaction_model->sync_from_lemonway($que_transaction['transactionIn']);
                            }
                            else
                            {
                                Log::lemonway_warning("LemonwayManager::get_account_transactions({$user_id}) - Transaction {$que_transaction['transactionIn']['id']}) (type = IN) does not exist on database. Try to create it.");

                                // Try to register new transaction in our database
                                $lemonway_transaction_model = $this->create_transaction('in', $que_transaction['transactionIn']);
                                if ( $lemonway_transaction_model )
                                {
                                    $vec_transaction_models[] = $lemonway_transaction_model;
                                }
                            }
                        }

                        // Transaction OUT
                        if ( isset($que_transaction['transactionOut']) && isset($que_transaction['transactionOut']['id']) )
                        {
                            $lemonway_transaction_model = LemonwayTransaction::model()->findByLemonwayId($que_transaction['transactionOut']['id'], 'out');
                            if ( $lemonway_transaction_model )
                            {
                                $vec_transaction_models[] = $lemonway_transaction_model->sync_from_lemonway($que_transaction['transactionOut']);
                            }
                            else
                            {
                                Log::lemonway_warning("LemonwayManager::get_account_transactions({$user_id}) - Transaction {$que_transaction['transactionOut']['id']}) (type = OUT) does not exist on database. Try to create it.");

                                // Try to register new transaction in our database
                                $lemonway_transaction_model = $this->create_transaction('out', $que_transaction['transactionOut']);
                                if ( $lemonway_transaction_model )
                                {
                                    $vec_transaction_models[] = $lemonway_transaction_model;
                                }
                            }
                        }

                        // Transaction P2P
                        if ( isset($que_transaction['transactionP2P']) && isset($que_transaction['transactionP2P']['id']) )
                        {
                            $lemonway_transaction_model = LemonwayTransaction::model()->findByLemonwayId($que_transaction['transactionP2P']['id'], 'p2p');
                            if ( $lemonway_transaction_model )
                            {
                                $vec_transaction_models[] = $lemonway_transaction_model->sync_from_lemonway($que_transaction['transactionP2P']);
                            }
                            else
                            {
                                Log::lemonway_warning("LemonwayManager::get_account_transactions({$user_id}) - Transaction {$que_transaction['transactionP2P']['id']}) (type = P2P) does not exist on database. Try to create it.");

                                // Try to register new transaction in our database
                                $lemonway_transaction_model = $this->create_transaction('p2p', $que_transaction['transactionP2P']);
                                if ( $lemonway_transaction_model )
                                {
                                    $vec_transaction_models[] = $lemonway_transaction_model;
                                }
                            }
                        }
                    }

                    return $vec_transaction_models;
                }
                else if ( $this->is_debug )
                {
                    Log::lemonway_dev("LemonwayManager::get_account_transactions({$lemonway_account_model->lemonway_account_id}) - Incorrect response: ". print_r($vec_response, true));
                }
            }
            else if ( $this->is_debug )
            {
                Log::lemonway_dev("LemonwayManager::get_account_transactions({$lemonway_account_model->lemonway_account_id}) - Last action error: ". print_r($response, true));
            }
        }

        else
        {
            if ( ! $user_model )
            {
                Log::lemonway_error("LemonwayManager::get_account_transactions({$user_id}) - USER does not exist: {$user_id}");
            }

            if ( ! $lemonway_account_model )
            {
                Log::lemonway_error("LemonwayManager::get_account_transactions({$user_id}) - LEMONWAY ACCOUNT does not exist: {$user_id}");
            }
        }

        return null;   
    }


    /**
     * Process Webkit (PSP) response
     */
    public function process_webkit_response($response_type, $vec_input)
    {
        if ( !empty($vec_input) && isset($vec_input['response_wkToken']) )
        {
            switch ( $response_type )
            {
                // Success response
                case 'success':
                    if ( isset($vec_input['response_transactionId']) && isset($vec_input['response_code']) && $vec_input['response_code'] == '0000' )
                    {
                        $lemonway_transaction_model = LemonwayTransaction::model()->findByLemonwayId($vec_input['response_transactionId'], 'in');

                        // Save webkit response
                        if ( $lemonway_transaction_model )
                        {
                            // Add current date into the webkit response stored on database
                            $vec_input['_date'] = date('d/m/Y - H:i');
                            $lemonway_transaction_model->response_webkit_json = Json::encode($vec_input);
                            $lemonway_transaction_model->saveAttributes([
                                'response_webkit_json' => $lemonway_transaction_model->response_webkit_json
                            ]);

                            // Get last transaction information from Lemonway API
                            $response = Yii::app()->lemonway->get_transaction_in($lemonway_transaction_model->lemonway_id);
                        }
                        else
                        {
                            Log::lemonway_error("LemonwayManager::process_webkit_response() - Received response from Lemonway Webkit via POST but LEMONWAY TRANSACTION is not found: {$vec_input['response_transactionId']}");
                        }
                    }
                break;

                // Error or cancel response
                default:
                    // Try to get transaction from "transaction_id" attribute  or our reference (internal ID from DZ core framework)
                    if ( !empty($vec_input['response_wkToken']) && is_numeric($vec_input['response_wkToken']) )
                    {
                        $lemonway_transaction_model = LemonwayTransaction::findOne($vec_input['response_wkToken']);
                        if ( $lemonway_transaction_model && $lemonway_transaction_model->transaction_type === 'in' && !empty($lemonway_transaction_model->request_webkit_json) )
                        {
                            // Add current date into the webkit response stored on database
                            $vec_input['_date'] = date('d/m/Y - H:i');
                            $lemonway_transaction_model->response_webkit_json = Json::encode($vec_input);
                            $lemonway_transaction_model->saveAttributes([
                                'response_webkit_json' => $lemonway_transaction_model->response_webkit_json
                            ]);

                            // Get last transaction information from Lemonway API
                            $response = Yii::app()->lemonway->get_transaction_in($lemonway_transaction_model->lemonway_id);
                        }
                    }
                break;
            }
        }

        // Save all the responses from Webkit into a log
        $this->save_webkit_response($response_type, $vec_input);

        return true;
    }


    /**
     * Save Webkit response into a log
     */
    public function save_webkit_response($response_type, $vec_input)
    {
        // Log recevied data
        $message  = "\nNEW DATA RECEIVED FROM Lemonway Webkit URL:";
        $message .= "\n - RESPONSE TYPE: ". $response_type;
        $message .= "\n - REMOTE IP: ";
        $message .= isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : "-";
        $message .= "\n - POST: ". print_r($vec_input, true);

        Log::lemonway($message);
    }


    /**
     * Create a new LemonwayTransaction model from data received via Lemonway API
     */
    public function create_transaction($transaction_type, $vec_data)
    {
        switch ( $transaction_type )
        {
            // Transaction IN
            case 'in':
                // LemonwayAccount model (receiver)
                $lemonway_account_model = LemonwayAccount::model()->findByAccountId($vec_data['receiverAccountId']);
                if ( $lemonway_account_model )
                {
                    $lemonway_transaction_model = Yii::createObject(LemonwayTransaction::class);
                    $lemonway_transaction_model->setAttributes([
                        'transaction_type'              => 'in',
                        'lemonway_id'                   => $vec_data['id'],
                        'receiver_user_id'              => $lemonway_account_model->user_id,
                        'receiver_lemonway_account_id'  => $lemonway_account_model->lemonway_account_id,
                        'total_amount'                  => $vec_data['creditAmount'],
                        'commission_amount'             => $vec_data['commissionAmount'],
                        'method_type'                   => $vec_data['method'],
                        'status_type'                   => $vec_data['status'],
                        'transaction_date'              => $vec_data['date'],
                        'execution_date'                => $vec_data['executionDate'],
                        'comment'                       => !empty($vec_data['comment']) ? $vec_data['comment'] : null,
                        'reference'                     => $vec_data['reference'],
                        'response_json'                 => Json::encode($vec_data),
                        'last_sync_date'                => time(),
                        'last_sync_endpoint'            => 'GET___'. $this->api->last_endpoint
                    ]);

                    // Lemonway Commission?
                    if ( isset($vec_data['lemonWayCommission']) && isset($vec_data['lemonWayCommission']['idp2p']) && isset($vec_data['lemonWayCommission']['amount']) && !empty($vec_data['lemonWayCommission']['idp2p']) )
                    {
                        $lemonway_transaction_model->commission_transaction_id = $vec_data['lemonWayCommission']['idp2p'];
                        $lemonway_transaction_model->commission_amount = $vec_data['lemonWayCommission']['amount'];
                    }

                    if ( ! $lemonway_transaction_model->save() )
                    {
                        Log::save_model_error($lemonway_transaction_model);
                    }

                    return $lemonway_transaction_model;
                }
                else
                {
                    Log::lemonway_error("LemonwayManager::create_transaction({$transaction_type}) - Receiver LEMONWAY ACCOUNT does not exist: {$vec_data['receiverAccountId']}");
                }
            break;

            // Transaction OUT
            case 'out':
                // LemonwayAccount model (sender)
                $lemonway_account_model = LemonwayAccount::model()->findByAccountId($vec_data['senderAccountId']);
                if ( $lemonway_account_model )
                {
                    // IBAN receiver? Check if LemonwayIban model exists
                    $receiver_iban_id = null;
                    if ( isset($vec_data['IbanId']) && !empty($vec_data['IbanId']) )
                    {
                        $lemonway_iban_model = LemonwayIban::findOne($vec_data['IbanId']);
                        if ( $lemonway_iban_model )
                        {
                            $receiver_iban_id = $lemonway_iban_model->lemonway_iban_id;
                        }
                    }

                    // Status and method are incorrect on API? The values are crossed
                    $method_type = isset($vec_data['method']) ? $vec_data['method'] : null;
                    $status_type = isset($vec_data['status']) ? $vec_data['status'] : null;
                    if ( $method_type !== null && $method_type !== 3 )
                    {
                        // Bank transfer (Money-Out)
                        $method_type = 3; 

                        // Status is really success
                        if ( $status_type !== null && $status_type === 3 )
                        {
                            $status_type = 0; 
                        }
                    }

                    $lemonway_transaction_model = Yii::createObject(LemonwayTransaction::class);
                    $lemonway_transaction_model->setAttributes([
                        'transaction_type'              => 'out',
                        'lemonway_id'                   => $vec_data['id'],
                        'receiver_user_id'              => $lemonway_account_model->user_id,
                        'receiver_lemonway_account_id'  => $lemonway_account_model->lemonway_account_id,
                        'receiver_lemonway_iban_id'     => $receiver_iban_id,
                        'sender_user_id'                => $lemonway_account_model->user_id,
                        'sender_lemonway_account_id'    => $lemonway_account_model->lemonway_account_id,
                        'total_amount'                  => $vec_data['debitAmount'],
                        'commission_amount'             => $vec_data['commissionAmount'],
                        'method_type'                   => $method_type,
                        'status_type'                   => $status_type,
                        'transaction_date'              => $vec_data['date'],
                        'execution_date'                => $vec_data['executionDate'],
                        'comment'                       => !empty($vec_data['comment']) ? $vec_data['comment'] : null,
                        'original_transaction_id'       => isset($vec_data['originId']) ? $vec_data['originId'] : null,
                        'psp_message'                   => isset($vec_data['PSP']) && isset($vec_data['PSP']['message']) ? $vec_data['PSP']['message'] : null,
                        'reference'                     => $vec_data['reference'],
                        'response_json'                 => Json::encode($vec_data),
                        'last_sync_date'                => time(),
                        'last_sync_endpoint'            => 'GET___'. $this->api->last_endpoint
                    ]);

                    // Lemonway Commission?
                    if ( isset($vec_data['lemonWayCommission']) && isset($vec_data['lemonWayCommission']['idp2p']) && isset($vec_data['lemonWayCommission']['amount']) && !empty($vec_data['lemonWayCommission']['idp2p']) )
                    {
                        $lemonway_transaction_model->commission_transaction_id = $vec_data['lemonWayCommission']['idp2p'];
                        $lemonway_transaction_model->commission_amount = $vec_data['lemonWayCommission']['amount'];
                    }

                    if ( ! $lemonway_transaction_model->save() )
                    {
                        Log::save_model_error($lemonway_transaction_model);
                    }

                    return $lemonway_transaction_model;
                }
                else
                {
                    Log::lemonway_error("LemonwayManager::create_transaction({$transaction_type}) - Sender LEMONWAY ACCOUNT does not exist: {$vec_data['senderAccountId']}");
                }
            break;

            // Transaction P2P
            case 'p2p':
                // LemonwayAccount model (sender)
                $sender_account_model = LemonwayAccount::model()->findByAccountId($vec_data['senderAccountId']);

                // LemonwayAccount model (receiver)
                $receiver_account_model = LemonwayAccount::model()->findByAccountId($vec_data['receiverAccountId']);
                
                if ( $sender_account_model && $receiver_account_model )
                {
                    // Status and method are incorrect on API? The values are crossed
                    $method_type = isset($vec_data['method']) ? $vec_data['method'] : null;
                    $status_type = isset($vec_data['status']) ? $vec_data['status'] : null;
                    if ( $method_type !== null && $method_type !== 4 )
                    {
                        // P2P
                        $method_type = 4; 
                    }

                    $lemonway_transaction_model = Yii::createObject(LemonwayTransaction::class);
                    $lemonway_transaction_model->setAttributes([
                        'transaction_type'              => 'p2p',
                        'lemonway_id'                   => $vec_data['id'],
                        'receiver_user_id'              => $receiver_account_model->user_id,
                        'receiver_lemonway_account_id'  => $receiver_account_model->lemonway_account_id,
                        'sender_user_id'                => $sender_account_model->user_id,
                        'sender_lemonway_account_id'    => $sender_account_model->lemonway_account_id,
                        'total_amount'                  => $vec_data['debitAmount'],
                        'commission_amount'             => $vec_data['commissionAmount'],
                        'method_type'                   => $method_type,
                        'status_type'                   => $status_type,
                        'transaction_date'              => $vec_data['date'],
                        'execution_date'                => $vec_data['executionDate'],
                        'comment'                       => !empty($vec_data['comment']) ? $vec_data['comment'] : null,
                        'commission_transaction_id'     => isset($vec_data['feeReference']) ? $vec_data['feeReference'] : null,
                        'reference'                     => $vec_data['reference'],
                        'response_json'                 => Json::encode($vec_data),
                        'last_sync_date'                => time(),
                        'last_sync_endpoint'            => 'GET___'. $this->api->last_endpoint
                    ]);

                    if ( ! $lemonway_transaction_model->save() )
                    {
                        Log::save_model_error($lemonway_transaction_model);
                    }

                    return $lemonway_transaction_model;
                }
                else
                {
                    if ( ! $sender_account_model )
                    {
                        Log::lemonway_error("LemonwayManager::create_transaction({$transaction_type}) - Sender LEMONWAY ACCOUNT does not exist: {$vec_data['senderAccountId']}");
                    }

                    if ( ! $receiver_account_model )
                    {
                        Log::lemonway_error("LemonwayManager::create_transaction({$transaction_type}) - Receiver LEMONWAY ACCOUNT does not exist: {$vec_data['receiverAccountId']}");
                    }
                }
            break;
        }

        return null;
    }
}