<?php
/**
 * Lemonway Manager
 * 
 * Helper classes to work with Lemonway models
 */

namespace dzlab\lemonway\components;

use dz\base\ApplicationComponent;
use dz\helpers\Log;
use dz\helpers\Url;
use dzlab\lemonway\models\LemonwayAccount;
use dzlab\lemonway\models\LemonwayDocument;
use dzlab\lemonway\models\LemonwayIban;
use dzlab\lemonway\models\LemonwayTransaction;
use user\models\User;
use Yii;

class LemonwayManager extends ApplicationComponent
{
    /**
     * Get "transaction_type" labels
     */
    public function transaction_labels()
    {
        return LemonwayTransaction::model()->transaction_type_labels();
    }


    /**
     * Get "status_type" colors
     */
    public function transaction_colors()
    {
        return [
            'in'        => 'green-800',
            'out'       => 'red-800',
            'p2p'       => 'orange-800',
            'refund'    => 'purple-800'
        ];
    }


    /**
     * Get "status_type" labels for LemonwayAccount models
     */
    public function account_status_labels()
    {
        return LemonwayAccount::model()->status_type_labels();
    }


    /**
     * Get "status_type" colors for LemonwayAccount models
     */
    public function account_status_colors()
    {
        return [
            0   => 'red-800',       // Unknown
            1   => 'red-800',       // Not Opened
            2   => 'red-800',       // Opened, need more documents
            3   => 'red-800',       // Opened, document rejected
            5   => 'orange-800',    // Opened, KYC1
            6   => 'green-800',     // Opened, KYC2
            7   => 'green-800',     // Opened, KYC3
            8   => 'orange-800',    // Opened, document expired
            9   => 'red-800',       // Frozen (by backoffice)
            10  => 'red-800',       // Blocked
            11  => 'green-800',     // Locked (by Web Service)
            12  => 'grey-800',      // Closed
            13  => 'orange-800',    // Pending KYC3
            14  => 'green-800',     // One-time customer
            15  => 'blue-800',      // CGE. Special account for crowdlending
            16  => 'blue-800',      // Technical Payment Accoun
        ];
    }


    /**
     * Get "status_filter" labels for LemonwayAccount models
     */
    public function account_status_filter_labels()
    {
        return LemonwayAccount::model()->status_filter_labels();
    }


    /**
     * Get "status_filter" colors for LemonwayAccount models
     */
    public function account_status_filter_colors()
    {
        return [
            'kyc_confirmed' => 'green-800',
            'kyc_pending'   => 'orange-800',
            'blocked_error' => 'red-800',
            'closed'        => 'grey-800',
            'technical'     => 'blue-800',
        ];
    }


    /**
     * Get "status_type" labels from LemonwayDocument model
     */
    public function document_status_labels()
    {
        return LemonwayDocument::model()->status_type_labels();
    }


    /**
     * Get "status_type" colors
     */
    public function document_status_colors()
    {
        return [
            0 => 'grey-800',    // Document put on hold, waiting for another document
            1 => 'orange-800',  // Received, need manual validation
            2 => 'green-800',   // Accepted
            3 => 'red-800',     // Rejected
            4 => 'red-800',     // Rejected. Unreadable by human (Cropped, blur, glare…)
            5 => 'red-800',     // Rejected. Expired (Expiration Date is passed)
            6 => 'red-800',     // Rejected. Wrong Type (Document not accepted)
            7 => 'red-800',     // Rejected. Wrong Name (Name not matching user information)
            8 => 'red-800',     // Rejected. Duplicated Document
        ];
    }


    /**
     * Get "status_type" labels from LemonwayDocument model
     */
    public function iban_status_labels()
    {
        return LemonwayIban::model()->status_type_labels();
    }


    /**
     * Get "status_type" colors
     */
    public function iban_status_colors()
    {
        return [
            1 => 'grey-800',    // None
            2 => 'grey-800',    // Internal (API)
            3 => 'red-800',     // Not used
            4 => 'orange-800',  // Waiting to be verified by Lemon Way
            5 => 'green-800',   // Activated
            6 => 'red-800',     // Rejected by the bank
            7 => 'red-800',     // Rejected, no owner
            8 => 'red-800',     // Deactivated
            9 => 'red-800',     // Rejected by Lemon Way
        ];
    }


    /**
     * Get totals numbers for LemonwayAccount models 
     */
    public function account_totals()
    {
        // Get existing statuses
        $vec_totals = [
            'total'         => 0,

            // Individual / Legal
            'individual'    => 0,
            'legal'         => 0,

            // Status type
            'kyc_confirmed' => 0,
            'kyc_pending'   => 0,
            'blocked_error' => 0,
            'closed'        => 0,
            'technical'     => 0
        ];

        // Get SQL table name
        $account_table = LemonwayAccount::model()->tableName();

        // Total by individual / legal
        $vec_results = Yii::app()->db->createCommand("SELECT COUNT(*) AS `total_accounts`, t.`is_legal` AS is_legal FROM {$account_table} t GROUP BY t.`is_legal` ORDER BY t.`is_legal`")->queryAll();
        if ( !empty($vec_results) )
        {
            foreach ( $vec_results as $que_result )
            {
                if ( $que_result['is_legal'] == 1 )
                {
                    $vec_totals['individual'] = $que_result['total_accounts'];
                }
                else
                {
                    $vec_totals['legal'] = $que_result['total_accounts'];    
                }

                $vec_totals['total'] += $que_result['total_accounts'];
            }
        }

        // Total by status
        $vec_results = Yii::app()->db->createCommand("SELECT COUNT(*) AS `total_accounts`, t.`status_type` AS status_type FROM {$account_table} t GROUP BY t.`status_type` ORDER BY t.`status_type`")->queryAll();
        if ( !empty($vec_results) )
        {
            foreach ( $vec_results as $que_result )
            {
                switch ( $que_result['status_type'] )
                {
                    // Opened, KYC2 (6) / Opened, KYC3 (7)
                    case 6:     
                    case 7:
                        $vec_totals['kyc_confirmed'] += $que_result['total_accounts'];
                    break;

                    // Opened, KYC1 (5) / Pending KYC3 (13)
                    case 5:     
                    case 13:
                        $vec_totals['kyc_pending'] += $que_result['total_accounts'];
                    break;

                    // Closed (12)
                    case 12:
                        $vec_totals['technical'] = $que_result['total_accounts'];
                    break;

                    // Technical (16)
                    case 16:
                        $vec_totals['technical'] = $que_result['total_accounts'];
                    break;

                    // Other are errors or rejected
                    default:
                        $vec_totals['blocked_error'] += $que_result['total_accounts'];
                    break;
                }
            }
        }

        return $vec_totals;
    }



    /**
     * Get totals numbers for LemonwayTransaction models 
     */
    public function transaction_totals()
    {
        // Get existing statuses
        $vec_totals = [
            'total'         => 0
        ];

        // Get SQL table name
        $transaction_table = LemonwayTransaction::model()->tableName();

        // Total by individual / legal
        $vec_results = Yii::app()->db->createCommand("SELECT COUNT(*) AS `total_transactions`, t.`transaction_type` AS transaction_type FROM {$transaction_table} t WHERE t.`transaction_type` <> 'refund' GROUP BY t.`transaction_type` ORDER BY t.`transaction_type`")->queryAll();
        if ( !empty($vec_results) )
        {
            foreach ( $vec_results as $que_result )
            {
                $vec_totals[$que_result['transaction_type']] = $que_result['total_transactions'];
                $vec_totals['total'] += $que_result['total_transactions'];
            }
        }

        return $vec_totals;
    }


    /**
     * Get totals numbers for LemonwayDocument models 
     */
    public function document_totals()
    {
        $vec_totals = [
            'total'         => 0,

            // Document type
            'identity'      => 0,
            'iban'          => 0,
            'sdd_mandate'   => 0,
            'other'         => 0,
            
            // Status type
            'received'      => 0,
            'on_hold'       => 0,
            'accepted'      => 0,
            'rejected'      => 0,
        ];

        // Get SQL table name
        $document_table = LemonwayDocument::model()->tableName();

        // Total by document type
        $vec_results = Yii::app()->db->createCommand("SELECT COUNT(*) AS `total_documents`, t.`document_type` AS document_type FROM {$document_table} t GROUP BY t.`document_type` ORDER BY t.`document_type`")->queryAll();
        if ( !empty($vec_results) )
        {
            foreach ( $vec_results as $que_result )
            {
                switch ( $que_result['document_type'] )
                {
                    // 0 - ID card (both sides in one file)
                    // 3 - Passport (European Union)
                    // 4 - Passport (outside the European Union)
                    case 0:
                    case 3:
                    case 4:
                        $vec_totals['identity'] += $que_result['total_documents'];
                    break;

                    // 2 - Scan of a proof of IBAN
                    case 2:
                        $vec_totals['iban'] = $que_result['total_documents'];
                    break;

                    // 21 - SDD mandate
                    case 21:
                        $vec_totals['ssd_mandate'] = $que_result['total_documents'];
                    break;

                    // Others
                    default:
                        $vec_totals['other'] += $que_result['total_documents'];
                    break;
                }

                $vec_totals['total'] += $que_result['total_documents'];
            }
        }

        // Total by status
        $vec_results = Yii::app()->db->createCommand("SELECT COUNT(*) AS `total_documents`, t.`status_type` AS status_type FROM {$document_table} t GROUP BY t.`status_type` ORDER BY t.`status_type`")->queryAll();
        if ( !empty($vec_results) )
        {
            foreach ( $vec_results as $que_result )
            {
                // Document put on hold, waiting for another document
                if ( $que_result['status_type'] == 0 )
                {
                    $vec_totals['on_hold'] = $que_result['total_documents'];
                }

                // Received, need manual validation
                else if ( $que_result['status_type'] == 1 )
                {
                    $vec_totals['received'] = $que_result['total_documents'];
                }

                // Accepted
                else if ( $que_result['status_type'] == 2 )
                {
                    $vec_totals['accepted'] = $que_result['total_documents'];
                }

                // Rejected
                else
                {
                    $vec_totals['rejected'] += $que_result['total_documents'];
                }
            }
        }

        return $vec_totals;
    }
}