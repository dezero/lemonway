<?php
/**
 * Lemonway configuration
 * 
 * Load it via Yii::app()->config->get('components.lemonway')
 */
return [
    // Debug mode?
    'is_debug' => true,

    // Sandbox environment
    'is_sandbox' => true,

    // Base URL for API endpoints
    'base_url' => 'https://sandbox-api.lemonway.fr/mb/<REPLACE_ME>/dev/directkitrest',

    // Base URL for Webkit
    'webkit_url' => 'https://sandbox-webkit.lemonway.fr/<REPLACE_ME>/dev',

    // Base URL for Dashboard
    'dashboard_url' => 'https://sb-bo.lemonway.com/<REPLACE_ME>',

    // Sandbox - Base URL for API endpoints
    'sandbox_base_url' => 'https://sandbox-api.lemonway.fr/mb/<REPLACE_ME>/dev/directkitrest',

    // Sandbox - Base URL for Webkit
    'sandbox_webkit_url' => 'https://sandbox-webkit.lemonway.fr/<REPLACE_ME>/dev',

    // Sandbox - Base URL for Dashboard
    'sandbox_dashboard_url' => 'https://sb-bo.lemonway.com/<REPLACE_ME>',

    // URL called by Lemonway WebKit when clients terminates the operation (SUCCESS case)
    'return_webkit_url' => '/lemonway/webkit/finish',

    // URL called by Lemonway WebKit when there is an error on the payment (ERROR case)
    'error_webkit_url' => '/lemonway/webkit/error',

    // URL called by Lemonway WebKit when the operation is canceled by the client (CANCEL case)
    'cancel_webkit_url' => '/lemonway/webkit/cancel',
];