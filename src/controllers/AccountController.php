<?php
/*
|--------------------------------------------------------------------------
| Controller class for LemonwayAccount model
|--------------------------------------------------------------------------
*/

namespace dzlab\lemonway\controllers;

use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\helpers\Url;
use dz\web\Controller;
use dzlab\lemonway\models\LemonwayAccount;
use user\models\User;
use Yii;

class AccountController extends Controller
{
    /**
     * List action for LemonwayAccount models
     */
    public function actionIndex()
    {
        $lemonway_account_model = Yii::createObject(LemonwayAccount::class, 'search');
        $lemonway_account_model->unsetAttributes();
        
        if ( isset($_GET['LemonwayAccount']) )
        {
            $lemonway_account_model->setAttributes($_GET['LemonwayAccount']);
        }

        // Update GRID with AJAX
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();
        }

        // Render INDEX view
        $this->render('//lemonway/account/index', [
            'lemonway_account_model'    => $lemonway_account_model,
            'vec_totals'                => Yii::app()->lemonwayManager->account_totals()
        ]);
    }


    /**
     * View action for LemonwayAccount model
     */
    public function actionView($user_id)
    {
        // LemonwayAccount model
        $lemonway_account_model = $this->loadModel($user_id, LemonwayAccount::class);

        $this->render('//lemonway/account/view', [
            'lemonway_account_model'    => $lemonway_account_model
        ]);
    }


    /**
     * Get data from LemonwayAccount models via API
     */
    public function actionSync($user_id)
    {
        // User model
        $user_model = $this->loadModel($user_id, LemonwayAccount::class);

        // Get LemonwayAccount model
        $lemonway_account_model = LemonwayAccount::findOne($user_id);
        if ( ! $lemonway_account_model )
        {
            throw new \CHttpException(404, Yii::t('app', 'Account does not exist.'));
        }

        // Received data from Lemonway API
        $lemonway_account_model = Yii::app()->lemonway->get_account($account_model->user_id);
        
        // Superadmin -- DEBUG mode
        if ( Yii::app()->user->id == 1 )
        {
            d($user_id);
            d($lemonway_account_model);
            d('GET '. Yii::app()->lemonway->get_last_endpoint());
            dd(Yii::app()->lemonway->get_response());
        }
        
        if ( $lemonway_account_model )
        {
            Yii::app()->user->addFlash('success', 'Data synced succesfully from Lemonway API');
        }
        else
        {
            Yii::app()->user->addFlash('error', 'Error syncing with Lemonway API');
        }

        // Redirect to main page
        $this->redirect(['/lemonway/account/view', 'user_id' => $user_id]);
    }


    /**
     * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
     *
     * array('<defined_action>' => '<operation_name_to_check>')
     *
     * @return array
     */
    static public function checkAliasActions()
    {
        return [
            'index' => 'lemonway.account.view',
            'view'  => 'lemonway.account.view',
            'sync'  => 'lemonway.account.update',
        ];
    }
}