<?php
/*
|--------------------------------------------------------------------------
| Controller class for LemonwayIban model
|--------------------------------------------------------------------------
*/

namespace dzlab\lemonway\controllers;

use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\helpers\Url;
use dz\web\Controller;
use dzlab\lemonway\models\LemonwayAccount;
use dzlab\lemonway\models\LemonwayIban;
use user\models\User;
use Yii;

class IbanController extends Controller
{
    /**
     * List action for LemonwayIban models
     */
    public function actionIndex($user_id)
    {
        // User model
        $user_model = $this->loadModel($user_id, User::class);

        // Get LemonwayAccount model
        $lemonway_account_model = LemonwayAccount::findOne($user_id);
        if ( ! $lemonway_account_model )
        {
            throw new \CHttpException(404, Yii::t('app', 'Account does not exist.'));
        }

        $lemonway_iban_model = Yii::createObject(LemonwayIban::class, 'search');
        $lemonway_iban_model->unsetAttributes();
        $lemonway_iban_model->user_id = $user_id;
        
        if ( isset($_GET['LemonwayIban']) )
        {
            $lemonway_iban_model->setAttributes($_GET['LemonwayIban']);
        }

        // Update GRID with AJAX
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();
        }

        // Render INDEX view
        $this->render('//lemonway/iban/index', [
            'user_model'                => $user_model,
            'lemonway_account_model'    => $lemonway_account_model,
            'lemonway_iban_model'       => $lemonway_iban_model
        ]);
    }


    /**
     * Get data from LemonwayIban models via API
     */
    public function actionSync($user_id)
    {
        // User model
        $user_model = $this->loadModel($user_id, LemonwayAccount::class);

        // Get LemonwayAccount model
        $lemonway_account_model = LemonwayAccount::findOne($user_id);
        if ( ! $lemonway_account_model )
        {
            throw new \CHttpException(404, Yii::t('app', 'Account does not exist.'));
        }

        // Received data from Lemonway API
        $vec_iban_models = Yii::app()->lemonway->get_ibans($lemonway_account_model->user_id);
        
        // Superadmin -- DEBUG mode
        if ( Yii::app()->user->id == 1 )
        {
            d($user_id);
            d($vec_iban_models);
            d('GET '. Yii::app()->lemonway->get_last_endpoint());
            dd(Yii::app()->lemonway->get_response());
        }
        
        if ( !empty($vec_iban_models) )
        {
            Yii::app()->user->addFlash('success', 'Data synced succesfully from Lemonway API');
        }
        else
        {
            Yii::app()->user->addFlash('error', 'Error syncing with Lemonway API');
        }

        // Redirect to main page
        $this->redirect(['/lemonway/iban', 'user_id' => $user_id]);
    }


    /**
     * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
     *
     * array('<defined_action>' => '<operation_name_to_check>')
     *
     * @return array
     */
    static public function checkAliasActions()
    {
        return [
            'index' => 'lemonway.account.view',
            'sync'  => 'lemonway.account.update',
        ];
    }
}