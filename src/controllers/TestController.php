<?php
/*
|--------------------------------------------------------------------------
| Controller class for testing purposes
|--------------------------------------------------------------------------
*/

namespace dzlab\lemonway\controllers;

use dz\helpers\Log;
use dz\helpers\Json;
use dz\helpers\StringHelper;
use dz\modules\asset\models\AssetFile;
use dz\modules\asset\models\AssetImage;
use dz\web\Controller;
use dzlab\lemonway\models\LemonwayAccount;
use dzlab\lemonway\models\LemonwayDocument;
use user\models\User;
use Yii;

class TestController extends Controller
{
    const SENDER_ID = 10;
    const RECEIVER_ID = 3;
    const FILE_ID = 16;

    /**
     * Test endpoint to create/update an User account
     */
    public function actionSaveAccount($id = null)
    {
        if ( $id === null )
        {
            $id = self::SENDER_ID;
        }

        $user_model = User::findOne($id);

        // Create / update account
        d(['id' => $id]);
        d($user_model->sync_to_lemonway());
        dd(Yii::app()->lemonway->get_response());
    }


    /**
     * Test endpoint to BLOCK access for an User account
     */
    public function actionBlock($id = null)
    {
        if ( $id === null )
        {
            $id = self::SENDER_ID;
        }

        $user_model = User::findOne($id);

        // Blocks an account
        d(['id' => $id]);
        d($user_model->block());
        dd(Yii::app()->lemonway->get_response());
    }


    /**
     * Test endpoint to UNBLOCK access for an User account
     */
    public function actionUnblock($id = null)
    {
        if ( $id === null )
        {
            $id = self::SENDER_ID;
        }

        $user_model = User::findOne($id);

        // Unblocks an account
        d(['id' => $id]);
        d($user_model->unblock());
        dd(Yii::app()->lemonway->get_response());
    }


    /**
     * Test endpoint to GET ACCOUNT data from an User
     */
    public function actionAccount($id = null)
    {
        if ( $id === null )
        {
            $id = self::SENDER_ID;
        }
        
        // Get Lemonway account
        d(['id' => $id]);
        d(Yii::app()->lemonway->get_account($id));
        dd(Yii::app()->lemonway->get_response());
    }


    /**
     * Test endpoint to GET BALANCE HISTORY data
     */
    public function actionBalance($id = null)
    {
        if ( $id === null )
        {
            $id = self::SENDER_ID;
        }

        // Get balance history
        $lemonway_account_model = LemonwayAccount::findOne($id);
        if ( $lemonway_account_model )
        {
            d(['id' => $id]);
            // dd(Yii::app()->lemonway->get_balance_history($investor_id, $lemonway_account_model->raw_attributes['updated_date'] + 3600 + 3600));
            d(Yii::app()->lemonway->get_balance_history($investor_id, $lemonway_account_model->raw_attributes['updated_date']));
            dd(Yii::app()->lemonway->get_response());
        }
    }


    /**
     * Test endpoint to upload a new document
     */
    public function actionUploadDocument($user_id = null, $file_id = null, $type = null)
    {
        // Author (account)
        if ( $user_id === null )
        {
            $user_id = self::SENDER_ID;
        }

        // File
        if ( $file_id === null )
        {
            $file_id = self::FILE_ID;
        }

        // Document type (0 = DNI)
        if ( $type === null )
        {
            $type = 0;
        }

        /*
        $lemonway_document_model = LemonwayDocument::get()->where([
            'user_id' => $user_id,
            'file_id' => $file_id
        ])->one();
        dd($lemonway_document_model->document_type_label());
        */

        // Upload a document
        d(['user_id' => $user_id, 'file_id' => $file_id, 'type' => $type]);
        d(Yii::app()->lemonway->upload_document($user_id, $file_id, $type));
        dd(Yii::app()->lemonway->get_response());
    }


    /**
     * Test endpoint to get all documents from a Lemonway Account a new document
     */
    public function actionDocuments($user_id)
    {
        if ( $user_id === null )
        {
            $user_id = self::SENDER_ID;
        }

        // Get all the documents related with a Lemonway account
        d(['user_id' => $user_id]);
        d(Yii::app()->lemonway->get_documents($user_id));
        dd(Yii::app()->lemonway->get_response());
    }


    /**
     * Test endpoint to create a new iban
     */
    public function actionCreateIban($user_id = null)
    {
        if ( $user_id === null )
        {
            $user_id = self::SENDER_ID;
        }

        $user_model = User::findOne($user_id);
        if ( $user_model && $user_model->bankAccount )
        {
            d(['user_id' => $user_id]);
            d($user_model->bankAccount->sync_to_lemonway());
            dd(Yii::app()->lemonway->get_response());
        }
    }


    /**
     * Test endpoint to disable an existing IBAN
     */
    public function actionDisableIban($iban_id)
    {
        d(['iban_id' => $iban_id]);
        d(Yii::app()->lemonway->disable_iban($iban_id));
        dd(Yii::app()->lemonway->get_response());
    }


    /**
     * Test endpoint to get all IBANs associated to an account
     */
    public function actionIbans($user_id = null)
    {
        if ( $user_id === null )
        {
            $user_id = self::SENDER_ID;
        }

        d(['user_id' => $user_id]);
        d(Yii::app()->lemonway->get_ibans($user_id));
        dd(Yii::app()->lemonway->get_response());
    }


    /**
     * Test endpoint to create a new VIRTUAL iban
     */
    public function actionCreateVirtualIban($user_id = null)
    {
        if ( $user_id === null )
        {
            $user_id = self::SENDER_ID;
        }

        d(['user_id' => $user_id]);
        d(Yii::app()->lemonway->create_virtual_iban($user_id));
        dd(Yii::app()->lemonway->get_response());
    }


    /**
     * Test endpoint to disable a VIRTUAL IBAN
     */
    public function actionDisableVirtualIban($iban_id)
    {
        d(['iban_id' => $iban_id]);
        d(Yii::app()->lemonway->disable_virtual_iban($iban_id));
        dd(Yii::app()->lemonway->get_response());
    }


    /**
     * Test endpoint to send money out
     */
    public function actionMoneyOut($id = null, $iban_id = null)
    {
        // Sender account
        if ( $id === null )
        {
            $id = self::SENDER_ID;
        }

        // Get a random amount
        if ( $amount === null )
        {
            $amount = $this->_get_random_amount();
        }

        // Send money out --> SUCCESS RESULT
        d(['id' => $id, 'iban_id' => $iban_id]);
        d(Yii::app()->lemonway->send_money_out($id, $amount, $iban_id));
        dd(Yii::app()->lemonway->get_response());

        // Send money out --> ERROR RESULT
        /*
        $amount = 9999999;
        $iban_id = null;
        d(Yii::app()->lemonway->send_money_out($id, $amount, $iban_id));
        dd(Yii::app()->lemonway->get_response());
        */

    }


    /**
     * Test endpoint for money P2P (between Lemonway accounts)
     */
    public function actionMoneyP2P($from = null, $to = null, $amount = null)
    {
        // Sender
        if ( $from === null )
        {
            $from = self::SENDER_ID;
        }

        // Receiver
        if ( $to === null )
        {
            $to = self::RECEIVER_ID;
        }
        
        // Get a random amount
        if ( $amount === null )
        {
            $amount = $this->_get_random_amount();
        }

        // Send money p2p2 --> SUCCESS RESULT
        d(['from' => $from, 'to' => $to, 'amount' => $amount]);
        d(Yii::app()->lemonway->send_money_p2p($from, $to, $amount));
        dd(Yii::app()->lemonway->get_response());

        // Send money p2p2 --> ERROR RESULT
        /*
        $amount = 9999999;
        dd(Yii::app()->lemonway->send_money_p2p($from, $to, $amount));
        dd(Yii::app()->lemonway->get_response());
        */
    }


    /**
     * Test endpoint for money IN
     */
    public function actionMoneyIn($id = null, $amount = null)
    {
        // User
        if ( $id === null )
        {
            $id = self::SENDER_ID;
        }

        // Get a random amount
        if ( $amount === null )
        {
            $amount = $this->_get_random_amount();
        }
        
        d(['id' => $id, 'amount' => $amount]);
        // d(Yii::app()->lemonway->webinit_money_in($id, $amount));
        d(Yii::app()->lemonway->generate_webkit_url($id, $amount));
        dd(Yii::app()->lemonway->get_response());
    }


    /**
     * Test endpoint for Money-In Transaction Information
     */
    public function actionTransactionIn($id = null)
    {
        d(['id' => $id]);
        d(Yii::app()->lemonway->get_transaction_in($id));
        dd(Yii::app()->lemonway->get_response());
    }


    /**
     * Test endpoint for Money-Out Transaction Information
     */
    public function actionTransactionOut($id)
    {
        d(['id' => $id]);
        d(Yii::app()->lemonway->get_transaction_out($id));
        dd(Yii::app()->lemonway->get_response());
    }


    /**
     * Test endpoint for P2P Transaction Information
     */
    public function actionTransactionP2p($id)
    {
        d(['id' => $id]);
        d(Yii::app()->lemonway->get_transaction_p2p($id));
        dd(Yii::app()->lemonway->get_response());
    }


    /**
     * Generate a random amount between 1.00 and 2.00 euros
     */
    private function _get_random_amount($min = 100, $max = 200)
    {
        // Get an random number between 100 and 200
        $amount = mt_rand(100, 200);

        // Return the float
        if ( $amount > 0 )
        {
            return $amount / 100;
        }

        // A static random number
        return 1.23;
    }
}