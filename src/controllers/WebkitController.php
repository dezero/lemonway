<?php
/**
|--------------------------------------------------------------------------
| Controller class to manage Lemonway Webkit server notifications & redirects
|--------------------------------------------------------------------------
 */

namespace dzlab\lemonway\controllers;

use dz\helpers\Log;
use dz\web\Controller;
use Yii;

class WebkitController extends Controller
{
    /**
     * @var string the name of the default action
     */
    public $defaultAction = 'finish';


    /**
     * Main action to manage successfull money-in transactions (returnUrl)
     * 
     * The client terminates its payment SUCCESFULLY.
     * 
     * 2 HTTP requests are received after this SUCCESS action:
     *  - POST request - Server To Server Notification (S2S) request from Lemonway server (invisible to end-user)
     *  - GET request - Client is redirected here after the payment operation has been terminated
     * 
     * @see https://documentation.lemonway.com/en/directkit/money-in%253A-credit-a-wallet/by-card/moneyinwebinit%253A-initiate-a-web-psp-payment/
     * @see https://apidoc.lemonway.com/#section/Finalize-card-payment-indirect-mode
     */
    public function actionFinish($transaction_id = null)
    {
        // Response from Lemonway Server (S2S) via POST
        if ( isset($_POST) && !empty($_POST) )
        {
            Yii::app()->lemonway->process_webkit_response('success', $_POST);
        }

        // Redirect page after client terminates the operation
        else
        {
            d("WebkitController::actionFinish({$transaction_id})");
            dd($_GET);
        }
    }


    /**
     * Main action to manage successfull money-in transactions (errorUrl)
     * 
     * There is an ERROR on the payment
     * 
     * 2 HTTP requests are received after this ERROR action:
     *  - POST request - Server To Server Notification (S2S) request from Lemonway server (invisible to end-user)
     *  - GET request - Client is redirected here after the payment operation has been terminated
     * 
     * @see https://documentation.lemonway.com/en/directkit/money-in%253A-credit-a-wallet/by-card/moneyinwebinit%253A-initiate-a-web-psp-payment/
     * @see https://apidoc.lemonway.com/#section/Finalize-card-payment-indirect-mode
     */
    public function actionError($transaction_id = null)
    {
        // Response from Lemonway Server (S2S) via POST
        if ( isset($_POST) && !empty($_POST) )
        {
            Yii::app()->lemonway->process_webkit_response('error', $_POST);
        }

        // Redirect page after client terminates the operation
        else
        {
            d("WebkitController::actionError({$transaction_id})");
            dd($_GET);
        }
    }


    /**
     * Main action to manage successfull money-in transactions (cancelUrl)
     * 
     * The payment is canceled by the client. We'll receive here 2 different of requests:
     * 
     * 2 HTTP requests are received after this CANCEL action:
     *  - POST request - Server To Server Notification (S2S) request from Lemonway server (invisible to end-user)
     *  - GET request - Client is redirected here after the payment operation has been terminated
     * 
     * @see https://documentation.lemonway.com/en/directkit/money-in%253A-credit-a-wallet/by-card/moneyinwebinit%253A-initiate-a-web-psp-payment/
     * @see https://apidoc.lemonway.com/#section/Finalize-card-payment-indirect-mode
     */
    public function actionCancel($transaction_id = null)
    {
        // Response from Lemonway Server (S2S) via POST
        if ( isset($_POST) && !empty($_POST) )
        {
            Yii::app()->lemonway->process_webkit_response('cancel', $_POST);
        }

        // Redirect page after client terminates the operation
        else
        {
            d("WebkitController::actionCancel({$transaction_id})");
            dd($_GET);
        }
    }
}
