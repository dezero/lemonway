<?php
/*
|--------------------------------------------------------------------------
| Traducción de mensajes - LEMONWAY
|--------------------------------------------------------------------------
|
| NOTE, this file must be saved in UTF-8 encoding.
*/
return [
    // LemonwayAccount - status_type type
    'Unknown' => 'Desconocido',
    'Not Opened' => 'No registrada',
    'Opened, need more documents' => 'Registrada, documentación incompleta',
    'Opened, document rejected' => 'Registrada, documentación incompleta / rechazada',
    'Opened, KYC1' => 'Registrada, KYC1',
    'Opened, KYC2' => 'Registrada, KYC2',
    'Opened, KYC3' => 'Registrada, KYC3',
    'Opened, document expired' => 'Registrada, documentación caducada',
    'Frozen (by backoffice)' => 'Retenida (desde backoffice)',
    'Blocked' => 'Bloqueada',
    'Locked (by Web Service)' => 'Retenida (desde Web Service)',
    'Closed' => 'Cerrada',
    'Pending KYC3' => 'Pendiente conversión a KYC3',
    'One-time customer' => 'Cliente ocasional',
    'CGE. Special account for crowdlending' => 'CGE. Cuenta especial para crowdlending',
    'Technical Payment Account' => 'Cuenta técnica',

    // LemonwayDocuument - status_type
    'ID card (both sides in one file)' => 'Documento de identidad',
    'Proof of address' => 'Justificante de domicilio',
    'Scan of a proof of IBAN' => 'Prueba de cuenta bancaria (IBAN)',
    'Passport (European Union)' => 'Pasaporte UE',
    'Passport (outside the European Union)' => 'Pasaporte extracomunitario',
    'Residence permit (both sides in one file)' => 'Tarjeta de residencia',
    'Official company registration document (Kbis extract or equivalent)' => 'Extracto KBIS o equivalente',
    'Driver licence (both sides in one file)' => 'Permiso de conducir',
    'Status' => 'Estado',
    'Selfie' => 'Selfie',
    'Other document type' => 'Otro',
    'SDD mandate' => 'Orden domiciliación SEPA',

    // LemonwayDocument - status_type
    'Document put on hold, waiting for another document' => 'El documento está en espera',
    'Received, need manual validation' => 'Esperando la verificación manual',
    'Accepted' => 'Aceptado',
    'Rejected' => 'Rechazado',
    'Rejected. Unreadable by human (Cropped, blur, glare…)' => 'Rechazado. Documento ilegible',
    'Rejected. Expired (Expiration Date is passed)' => 'Rechazado. Documento caducado',
    'Rejected. Wrong Type (Document not accepted)' => 'Rechazado. Tipo de documento equivocado',
    'Rejected. Wrong Name (Name not matching user information)' => 'Rechazado. Nombre incorrecto del titular',
    'Rejected. Duplicated Document' => 'Rechazado. Documento duplicado',

    // LemonwayIban - status_type
    'None' => 'N/A',
    'Internal (API)' => 'Registrado a través de la API',
    'Not used' => 'Not utilizado',
    'Waiting to be verified by Lemon Way' => 'En espera de verificación por Lemon Way',
    'Activated' => 'Activado',
    'Rejected by the bank' => 'Rechazado por el banco',
    'Rejected, no owner' => 'Ningún titular',
    'Deactivated' => 'Desactivado',
    'Rejected by Lemon Way' => 'Rechazado por Lemon Way',
    
    // LemonwayTransaction - transaction_type
    'Money In' => 'Entrante',
    'Money Out' => 'Saliente',
    'P2P' => 'P2P',
    'Refund' => 'Reembolso',

    // LemonwayTransaction - status_type
    'Success' => 'Éxito',
    'LemonWay Error' => 'Error LW',
    'Pending' => 'En espera',
    'Error PSP' => 'Error PSP',
    'Cancelled' => 'Cancelado',
    'Scheduled' => 'Programado',
    'Validation Pending' => 'En espera de validación',
    'Success - P2P' => 'Éxito - P2P',
    'Pending - P2P' => 'Pendiente - P2P',

    // LemonwayTransaction - method_type
    'Card' => 'Tarjeta',
    'Bank transfer (Money-In)' => 'Transferencia entrante',
    'Bank transfer (Money-Out)' => 'Transferencia saliente',
    'P2P' => 'Interno',
    'DirectDebit' => 'Adeudo Directo',
    'Cheque' => 'Cheque',
    'PAGARE' => 'Pagaré',
    'WeChat (via PayTend)' => 'WeChat (via PayTend)',
    'Canceled' => 'Anulado',
];