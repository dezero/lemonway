<?php
/**
 * Migration class m210426_113122_lemonway_account_tables
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m210426_113122_lemonway_account_tables extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Create "lemonway_account" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('lemonway_account', true);

        $this->createTable('lemonway_account', [
            'user_id' => $this->integer()->unsigned()->notNull(),
            'lemonway_id' => $this->integer()->unsigned()->notNull(),
            'status_type' => $this->tinyInteger()->unsigned()->notNull()->defaultValue(5),
            'balance_amount' => $this->float()->unsigned()->notNull()->defaultValue(0),
            'is_legal' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'is_blocked' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'is_debtor' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'is_payer' => $this->tinyInteger(1)->unsigned(),
            'is_technical' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);
    
        // Primary key (alternative method)
        $this->addPrimaryKey(null, 'lemonway_account', 'user_id');

        // Create indexes
        $this->createIndex(null, 'lemonway_account', ['lemonway_id'], false);
        $this->createIndex(null, 'lemonway_account', ['status_type'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'lemonway_account', ['user_id'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'lemonway_account', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'lemonway_account', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

        
        // Create "lemonway_document" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('lemonway_document', true);

        $this->createTable('lemonway_document', [
            'user_id' => $this->integer()->unsigned()->notNull(),
            'file_id' => $this->integer()->unsigned()->notNull(),
            'lemonway_id' => $this->integer()->unsigned()->notNull(),
            'name' => $this->string()->notNull(),
            'status_type' => $this->tinyInteger()->unsigned()->notNull(),
            'substatus_type' => $this->tinyInteger()->unsigned()->notNull()->defaultValue(0),
            'document_type' => $this->tinyInteger()->unsigned()->notNull(),
            'validity_date' => $this->date(),
            'comment' => $this->string(512),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Primary key (alternative method)
        $this->addPrimaryKey(null, 'lemonway_document', ['user_id', 'file_id']);

        // Create indexes
        $this->createIndex(null, 'lemonway_document', ['lemonway_id'], false);
        $this->createIndex(null, 'lemonway_document', ['status_type'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'lemonway_document', ['user_id'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'lemonway_document', ['file_id'], 'asset_file', ['file_id'], 'RESTRICT', null);
        $this->addForeignKey(null, 'lemonway_document', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'lemonway_document', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

        // Add new columns for last sync information
        $this->addColumn('lemonway_document', 'last_sync_date', $this->date()->notNull()->after('comment'));
        $this->addColumn('lemonway_document', 'last_sync_endpoint', $this->string(128)->notNull()->after('last_sync_date'));

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
        // $this->dropTable('lemonway_account');
		// $this->dropTable('lemonway_document');
		return false;
	}
}

