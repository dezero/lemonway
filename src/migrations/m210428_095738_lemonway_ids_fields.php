<?php
/**
 * Migration class m210428_095738_lemonway_ids_fields
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m210428_095738_lemonway_ids_fields extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Rename column "lemonway_id" to "lemonway_internal_id"
        $this->renameColumn('lemonway_account', 'lemonway_id', 'lemonway_internal_id');

        // Add new column "lemonway_account_id" with index
        $this->addColumn('lemonway_account', 'lemonway_account_id', $this->string(64)->after('lemonway_internal_id'));
        $this->createIndex(null, 'lemonway_account', ['lemonway_account_id'], false);

        // Add new columns for last sync information
        $this->addColumn('lemonway_account', 'last_sync_date', $this->date()->notNull()->after('is_technical'));
        $this->addColumn('lemonway_account', 'last_sync_endpoint', $this->string(128)->notNull()->after('last_sync_date'));

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

