<?php
/**
 * Migration class m210428_152216_lemonway_iban_table
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m210428_152216_lemonway_iban_table extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Create "lemonway_iban" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('lemonway_iban', true);

        $this->createTable('lemonway_iban', [
            'lemonway_iban_id' => $this->integer()->unsigned()->notNull(),
            'user_id' => $this->integer()->unsigned()->notNull(),
            'status_type' => $this->tinyInteger()->unsigned()->notNull(),
            'iban_type' => $this->tinyInteger()->unsigned()->notNull()->defaultValue(1),
            'holder' => $this->string()->notNull(),
            'bic' => $this->string(16),
            'iban' => $this->string(36)->notNull(),
            'domiciliation1' => $this->string(),
            'domiciliation2' => $this->string(),
            'comment' => $this->string(512),
            'last_sync_date' => $this->date()->notNull(),
            'last_sync_endpoint' => $this->string(128)->notNull(),
            'disable_date' => $this->date(),
            'disable_uid' => $this->integer()->unsigned(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Primary key (alternative method)
        $this->addPrimaryKey(null, 'lemonway_iban', ['lemonway_iban_id']);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'lemonway_iban', ['user_id'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'lemonway_iban', ['disable_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'lemonway_iban', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'lemonway_iban', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('lemonway_iban');
		return false;
	}
}

