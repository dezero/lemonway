<?php
/**
 * Migration class m210429_070724_lemonway_extend_account_table
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m210429_070724_lemonway_extend_account_table extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Add personal, address, birth and company data
		$this->addColumn('lemonway_account', 'email', $this->string()->notNull()->after('balance_amount'));
        $this->addColumn('lemonway_account', 'title', $this->char(1)->after('email'));
        $this->addColumn('lemonway_account', 'firstname', $this->string()->notNull()->after('title'));
        $this->addColumn('lemonway_account', 'lastname', $this->string()->notNull()->after('firstname'));
        $this->addColumn('lemonway_account', 'address_street', $this->string()->after('lastname'));
        $this->addColumn('lemonway_account', 'address_post_code', $this->string(32)->after('address_street'));
        $this->addColumn('lemonway_account', 'address_city', $this->string()->after('address_post_code'));
        $this->addColumn('lemonway_account', 'address_country_code', $this->char(3)->notNull()->after('address_city'));
        $this->addColumn('lemonway_account', 'birth_date', $this->string(16)->notNull()->after('address_country_code'));
        $this->addColumn('lemonway_account', 'birth_city', $this->string()->after('birth_date'));
        $this->addColumn('lemonway_account', 'birth_country_code', $this->char(3)->after('birth_city'));
        $this->addColumn('lemonway_account', 'company_name', $this->string()->after('birth_country_code'));
        $this->addColumn('lemonway_account', 'company_description', $this->string()->after('company_name'));
        $this->addColumn('lemonway_account', 'company_website', $this->string()->after('company_description'));
        $this->addColumn('lemonway_account', 'company_vat_code', $this->string()->after('company_website'));
        $this->addColumn('lemonway_account', 'nationality_country_code', $this->string(19)->notNull()->after('company_vat_code'));
        $this->addColumn('lemonway_account', 'phone_number', $this->string(32)->after('nationality_country_code'));
        $this->addColumn('lemonway_account', 'mobile_number', $this->string(32)->after('phone_number'));
        $this->addColumn('lemonway_account', 'is_one_time_customer', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0)->after('is_payer'));

        // Rename "is_payer" to "is_payer_or_beneficiary"
        $this->renameColumn('lemonway_account', 'is_payer', 'is_payer_or_beneficiary');

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

