<?php
/**
 * Migration class m210429_140715_iban_bank_account
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m210429_140715_iban_bank_account extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Add new column "bank_account_id"
		$this->addColumn('lemonway_iban', 'bank_account_id', $this->integer()->unsigned()->after('user_id'));

        // Create FOREIGN KEYS
        $this->createIndex(null, 'lemonway_iban', ['bank_account_id'], false);
        // $this->addForeignKey(null, 'lemonway_iban', ['bank_account_id'], 'bank_account', ['bank_account_id'], 'SET NULL', null);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

