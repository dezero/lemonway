<?php
/**
 * Migration class m210429_152519_lemonway_transaction_table
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m210429_152519_lemonway_transaction_table extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Create "lemonway_transaction" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('lemonway_transaction', true);

        $this->createTable('lemonway_transaction', [
            'transaction_id' => $this->primaryKey(),
            'lemonway_id' => $this->integer()->unsigned(),
            'transaction_type' => $this->enum('transaction_type', ['in', 'out', 'p2p', 'refund'])->notNull(),
            'receiver_user_id' => $this->integer()->unsigned()->notNull(),
            'receiver_lemonway_account_id' => $this->string(64)->unsigned()->notNull(),
            'receiver_lemonway_iban_id' => $this->integer()->unsigned(),
            'sender_user_id' => $this->integer()->unsigned(),
            'sender_lemonway_account_id' => $this->string(64)->unsigned(),
            'total_amount' => $this->integer()->unsigned()->notNull(),
            'commission_amount' => $this->integer()->unsigned()->notNull()->defaultValue(0),
            'reference' => $this->string(64),
            'method_type' => $this->tinyInteger()->unsigned(),
            'status_type' => $this->tinyInteger()->unsigned(),
            'transaction_date' => $this->date()->notNull(),
            'execution_date' => $this->date(),
            'comment' => $this->string(140),
            'is_auto_commission' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'original_transaction_id' => $this->integer()->unsigned(),
            'commission_transaction_id' => $this->integer()->unsigned(),
            'request_token' => $this->string(),
            'response_json' => $this->text(),
            'error_code' => $this->integer()->unsigned(),
            'error_message' => $this->string(),
            'psp_message' => $this->string(),
            'last_sync_date' => $this->date(),
            'last_sync_endpoint' => $this->string(128),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Create indexes
        $this->createIndex(null, 'lemonway_transaction', ['lemonway_id'], false);
        $this->createIndex(null, 'lemonway_transaction', ['transaction_type'], false);
        $this->createIndex(null, 'lemonway_transaction', ['status_type'], false);
        $this->createIndex(null, 'lemonway_transaction', ['method_type'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'lemonway_transaction', ['receiver_user_id'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'lemonway_transaction', ['sender_user_id'], 'user_users', ['id'], 'SET NULL', null);
        $this->addForeignKey(null, 'lemonway_transaction', ['receiver_lemonway_account_id'], 'lemonway_account', ['lemonway_account_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'lemonway_transaction', ['sender_lemonway_account_id'], 'lemonway_account', ['lemonway_account_id'], 'SET NULL', null);
        $this->addForeignKey(null, 'lemonway_transaction', ['receiver_lemonway_iban_id'], 'lemonway_iban', ['lemonway_iban_id'], 'SET NULL', null);
        $this->addForeignKey(null, 'lemonway_transaction', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'lemonway_transaction', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('lemonway_transaction');
		return false;
	}
}

