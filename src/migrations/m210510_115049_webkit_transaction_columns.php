<?php
/**
 * Migration class m210510_115049_webkit_transaction_columns
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m210510_115049_webkit_transaction_columns extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Add new columns to save requests and responses from Webkit (TPV)
		$this->addColumn('lemonway_transaction', 'request_webkit_json', $this->string(512)->after('response_json'));
        $this->addColumn('lemonway_transaction', 'response_webkit_json', $this->string(512)->after('request_webkit_json'));

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('my_table');
		return false;
	}
}

