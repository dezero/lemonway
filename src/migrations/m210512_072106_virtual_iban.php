<?php
/**
 * Migration class m210512_072106_virtual_iban
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m210512_072106_virtual_iban extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Add new columns for Virtual IBAN
        $this->addColumn('lemonway_iban', 'is_virtual', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0)->after('user_id'));
        $this->addColumn('lemonway_iban', 'pdf_file_id', $this->integer()->unsigned()->after('comment'));
        $this->addColumn('lemonway_iban', 'qr_image_id', $this->integer()->unsigned()->after('pdf_file_id'));
		$this->addColumn('lemonway_iban', 'options_json', $this->string(255)->after('qr_image_id'));

        // Create indexes
        $this->createIndex(null, 'lemonway_iban', ['is_virtual'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'lemonway_iban', ['pdf_file_id'], 'asset_file', ['file_id'], 'SET NULL', null);
        $this->addForeignKey(null, 'lemonway_iban', ['qr_image_id'], 'asset_file', ['file_id'], 'SET NULL', null);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

