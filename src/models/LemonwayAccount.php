<?php
/**
 * @package dzlab\lemonway\models 
 */

namespace dzlab\lemonway\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\modules\api\models\LogApi;
use dzlab\lemonway\models\_base\LemonwayAccount as BaseLemonwayAccount;
use dzlab\lemonway\models\LemonwayDocument;
use dzlab\lemonway\models\LemonwayIban;
use user\models\User;
use Yii;

/**
 * LemonwayAccount model class for "lemonway_account" database table
 *
 * Columns in table "lemonway_account" available as properties of the model,
 * followed by relations of table "lemonway_account" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $user_id
 * @property integer $lemonway_internal_id
 * @property string $lemonway_account_id
 * @property integer $status_type
 * @property double $balance_amount
 * @property string $email
 * @property string $title
 * @property string $firstname
 * @property string $lastname
 * @property string $address_street
 * @property string $address_post_code
 * @property string $address_city
 * @property string $address_country_code
 * @property string $birth_date
 * @property string $birth_city
 * @property string $birth_country_code
 * @property string $company_name
 * @property string $company_description
 * @property string $company_website
 * @property string $company_vat_code
 * @property string $nationality_country_code
 * @property string $phone_number
 * @property string $mobile_number
 * @property integer $is_legal
 * @property integer $is_blocked
 * @property integer $is_debtor
 * @property integer $is_payer_or_beneficiary
 * @property integer $is_one_time_customer
 * @property integer $is_technical
 * @property integer $last_sync_date
 * @property string $last_sync_endpoint
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $createdUser
 * @property mixed $updatedUser
 * @property mixed $user
 */
class LemonwayAccount extends BaseLemonwayAccount
{
    /**
     * Search filter by grouped status
     */
    public $status_filter;


    /**
     * Search filter by account type: legal or individual
     */
    public $type_filter;


    /**
     * Name filter
     */
    public $name_filter;


	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['user_id, lemonway_internal_id, email, firstname, lastname, address_country_code, birth_date, nationality_country_code, last_sync_date, last_sync_endpoint, created_date, created_uid, updated_date, updated_uid', 'required'],
			['user_id, lemonway_internal_id, status_type, is_legal, is_blocked, is_debtor, is_payer_or_beneficiary, is_one_time_customer, is_technical, last_sync_date, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['balance_amount', 'numerical'],
            ['lemonway_account_id', 'length', 'max'=> 64],
            ['email, firstname, lastname, address_street, address_city, birth_city, company_name, company_description, company_website, company_vat_code', 'length', 'max'=> 255],
            ['title', 'length', 'max'=> 1],
            ['address_post_code, phone_number, mobile_number', 'length', 'max'=> 32],
            ['address_country_code, birth_country_code', 'length', 'max'=> 3],
            ['birth_date', 'length', 'max'=> 16],
            ['nationality_country_code', 'length', 'max'=> 19],
            ['last_sync_endpoint', 'length', 'max'=> 128],
			['uuid', 'length', 'max'=> 36],
			['lemonway_account_id, status_type, balance_amount, title, address_street, address_post_code, address_city, birth_city, birth_country_code, company_name, company_description, company_website, company_vat_code, phone_number, mobile_number, is_legal, is_blocked, is_debtor, is_payer_or_beneficiary, is_one_time_customer, is_technical, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['user_id, lemonway_internal_id, lemonway_account_id, status_type, balance_amount, email, title, firstname, lastname, address_street, address_post_code, address_city, address_country_code, birth_date, birth_city, birth_country_code, company_name, company_description, company_website, company_vat_code, nationality_country_code, phone_number, mobile_number, is_legal, is_blocked, is_debtor, is_payer_or_beneficiary, is_one_time_customer, is_technical, last_sync_date, last_sync_endpoint, created_date, created_uid, updated_date, updated_uid, uuid, status_filter, type_filter, name_filter', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'user' => [self::BELONGS_TO, User::class, 'user_id'],
			'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],
            'lemonwayDocuments' => [self::HAS_MANY, LemonwayDocument::class, ['user_id' => 'user_id']],
            'lemonwayIbans' => [self::HAS_MANY, LemonwayIban::class, ['user_id' => 'user_id']],

            // Custom relations
            'logsApi' => [self::HAS_MANY, LogApi::class, ['entity_id' => 'user_id'], 'condition' => 'logsApi.entity_type = "LemonwayAccount"', 'order' => 'logsApi.created_date DESC'],
		];
	}


    /**
     * External behaviors
     */
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'last_sync_date' => 'd/m/Y - H:i'
                ],
            ],

            // Currency format
            /*
            'CurrencyBehavior' => [
                'class' => '\dz\behaviors\CurrencyBehavior',
                'columns' => [
                    'balance_amount'
                ],
            ],
            */

        ], parent::behaviors());
    }

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
            'user_id' => null,
            'lemonway_internal_id' => Yii::t('app', 'Lemonway ID'),
            'lemonway_account_id' => Yii::t('app', 'Lemonway Account ID'),
            'status_type' => Yii::t('app', 'Status'),
            'balance_amount' => Yii::t('app', 'Balance'),
            'email' => Yii::t('app', 'Email'),
            'title' => Yii::t('app', 'Title'),
            'firstname' => Yii::t('app', 'Name'),
            'lastname' => Yii::t('app', 'Lastname'),
            'address_street' => Yii::t('app', 'Address Street'),
            'address_post_code' => Yii::t('app', 'Address Post Code'),
            'address_city' => Yii::t('app', 'Address City'),
            'address_country_code' => Yii::t('app', 'Address Country Code'),
            'birth_date' => Yii::t('app', 'Birth Date'),
            'birth_city' => Yii::t('app', 'Birth City'),
            'birth_country_code' => Yii::t('app', 'Birth Country Code'),
            'company_name' => Yii::t('app', 'Company Name'),
            'company_description' => Yii::t('app', 'Company Description'),
            'company_website' => Yii::t('app', 'Company Website'),
            'company_vat_code' => Yii::t('app', 'Company Vat Code'),
            'nationality_country_code' => Yii::t('app', 'Nationality Country Code'),
            'phone_number' => Yii::t('app', 'Phone Number'),
            'mobile_number' => Yii::t('app', 'Mobile Number'),
            'is_legal' => Yii::t('app', 'Is Legal'),
            'is_blocked' => Yii::t('app', 'Is Blocked'),
            'is_debtor' => Yii::t('app', 'Is Debtor'),
            'is_payer_or_beneficiary' => Yii::t('app', 'Is Payer or Beneficiary'),
            'is_one_time_customer' => Yii::t('app', 'Is One Time Customer'),
            'is_technical' => Yii::t('app', 'Is Technical'),
            'last_sync_date' => Yii::t('app', 'Last Sync'),
            'last_sync_endpoint' => Yii::t('app', 'Last Sync Endpoint'),
            'created_date' => Yii::t('app', 'Created Date'),
            'created_uid' => null,
            'updated_date' => Yii::t('app', 'Updated Date'),
            'updated_uid' => null,
            'uuid' => Yii::t('app', 'Uuid'),
            'createdUser' => null,
            'updatedUser' => null,
            'user' => null,
        ];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        // Type filter: legal or individual
        if ( $this->type_filter )
        {
            if ( $this->type_filter === 'legal' )
            {
                $criteria->compare('t.is_legal', 1);
            }
            else
            {
                $criteria->compare('t.is_legal', 0);   
            }
        }

        // Status filter
        if ( $this->status_filter )
        {
            switch ( $this->status_filter )
            {
                // Opened, KYC2 (6) / Opened, KYC3 (7)
                case 'kyc_confirmed':
                    $criteria->addInCondition('t.status_type', [6, 7]);
                break;

                // Opened, KYC1 (5) / Pending KYC3 (13)
                case 'kyc_pending':
                    $criteria->addInCondition('t.status_type', [5, 13]);
                break;

                // Closed (12)
                case 'closed':
                    $criteria->compare('t.status_type', 12);
                break;

                // Technical (16)
                case 'technical':
                    $criteria->compare('t.status_type', 16);
                break;

                default:
                    $criteria->addNotInCondition('t.status_type', [5, 6, 7, 12, 13, 16]);
                break;

            }
        }
        else
        {
            $criteria->compare('t.status_type', $this->status_type);
        }

        // Filter by name
        if ( $this->name_filter )
        {
            $criteria->addCondition('t.firstname LIKE CONCAT(:name_filter, "%") OR t.lastname LIKE CONCAT(:name_filter, "%")');
            $criteria->addParams([':name_filter' => $this->name_filter]);
        }

        // Filter by email
        $criteria->compare('t.email', $this->email, true);

        // Filter by ID
        if ( $this->lemonway_account_id )
        {
            $criteria->addCondition('t.lemonway_account_id = :lemonway_account_id OR t.lemonway_internal_id = :lemonway_account_id');
            $criteria->addParams([':lemonway_account_id' => $this->lemonway_account_id]);   
        }

        /*
        $criteria->compare('t.lemonway_internal_id', $this->lemonway_internal_id);
        $criteria->compare('t.lemonway_account_id', $this->lemonway_account_id);
        $criteria->compare('t.status_type', $this->status_type);
        $criteria->compare('t.balance_amount', $this->balance_amount);
        $criteria->compare('t.title', $this->title, true);
        $criteria->compare('t.firstname', $this->firstname, true);
        $criteria->compare('t.lastname', $this->lastname, true);
        $criteria->compare('t.address_street', $this->address_street, true);
        $criteria->compare('t.address_post_code', $this->address_post_code, true);
        $criteria->compare('t.address_city', $this->address_city, true);
        $criteria->compare('t.address_country_code', $this->address_country_code, true);
        $criteria->compare('t.birth_date', $this->birth_date, true);
        $criteria->compare('t.birth_city', $this->birth_city, true);
        $criteria->compare('t.birth_country_code', $this->birth_country_code, true);
        $criteria->compare('t.company_name', $this->company_name, true);
        $criteria->compare('t.company_description', $this->company_description, true);
        $criteria->compare('t.company_website', $this->company_website, true);
        $criteria->compare('t.company_vat_code', $this->company_vat_code, true);
        $criteria->compare('t.nationality_country_code', $this->nationality_country_code, true);
        $criteria->compare('t.phone_number', $this->phone_number, true);
        $criteria->compare('t.mobile_number', $this->mobile_number, true);
        $criteria->compare('t.is_legal', $this->is_legal);
        $criteria->compare('t.is_blocked', $this->is_blocked);
        $criteria->compare('t.is_debtor', $this->is_debtor);
        $criteria->compare('t.is_payer_or_beneficiary', $this->is_payer_or_beneficiary);
        $criteria->compare('t.is_one_time_customer', $this->is_one_time_customer);
        $criteria->compare('t.is_technical', $this->is_technical);
        $criteria->compare('t.last_sync_date', $this->last_sync_date);
        $criteria->compare('t.last_sync_endpoint', $this->last_sync_endpoint, true);
        $criteria->compare('t.created_date', $this->created_date);
        $criteria->compare('t.updated_date', $this->updated_date);
        $criteria->compare('t.uuid', $this->uuid, true);
        */

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['user_id' => true]]
        ]);
    }


    /**
     * LemonwayAccount models list
     * 
     * @return array
     */
    public function lemonwayaccount_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['user_id', 'email'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = LemonwayAccount::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('user_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Get "status_type" specific label
     */
    public function status_type_label($status_type = null)
    {
        if ( $status_type === null )
        {
            $status_type = $this->status_type;
        }

        $vec_labels = $this->status_type_labels();
        return isset($vec_labels[$status_type]) ? $vec_labels[$status_type] : '';
    }


    /**
     * Get "status_type" labels
     */
    public function status_type_labels()
    {
        return [
            0   => Yii::t('lemonway', 'Unknown'),
            1   => Yii::t('lemonway', 'Not Opened'),
            2   => Yii::t('lemonway', 'Opened, need more documents'),
            3   => Yii::t('lemonway', 'Opened, document rejected'),
            5   => Yii::t('lemonway', 'Opened, KYC1'),
            6   => Yii::t('lemonway', 'Opened, KYC2'),
            7   => Yii::t('lemonway', 'Opened, KYC3'),
            8   => Yii::t('lemonway', 'Opened, document expired'),
            9   => Yii::t('lemonway', 'Frozen (by backoffice)'),
            10  => Yii::t('lemonway', 'Blocked'),
            11  => Yii::t('lemonway', 'Locked (by Web Service)'),
            12  => Yii::t('lemonway', 'Closed'),
            13  => Yii::t('lemonway', 'Pending KYC3'),
            14  => Yii::t('lemonway', 'One-time customer'),
            15  => Yii::t('lemonway', 'CGE. Special account for crowdlending'),
            16  => Yii::t('lemonway', 'Technical Payment Account')
        ];
    }


    /**
     * Get "status_filter" labels
     */
    public function status_filter_labels()
    {
         return [
            'kyc_confirmed' => Yii::t('lemonway', 'KYC Confirmed'),
            'kyc_pending'   => Yii::t('lemonway', 'KYC Pending'),
            'blocked_error' => Yii::t('lemonway', 'Rejected / Blocked'),
            'closed'        => Yii::t('lemonway', 'Closed'),
            'technical'     => Yii::t('lemonway', 'Tehcnical'),
          ];
    }


    /**
     * Return last LogApi model. It can be filtered by request_endpoint
     */
    public function get_last_log_api_model($request_endpoint = null)
    {
        $vec_conditions = [
            'api_name'      => 'lemonway',
            'entity_id'     => $this->user_id,
            'entity_type'   => 'LemonwayAccount'
        ];

        // Filter by request_endpoint?
        if ( $request_endpoint !== null )
        {
            $vec_conditions['request_endpoint'] = $request_endpoint;
        }

        return LogApi::get()
            ->where($vec_conditions)
            ->orderBy('created_date DESC')
            ->limit(1)
            ->one();
    }


    /**
     * Get balance_amount in raw or formatted value
     */
    public function get_balance_amount($is_raw = false)
    {
        if ( $this->balance_amount > 0 )
        {
            return $is_raw ? $this->balance_amount : Yii::app()->number->formatNumber($this->balance_amount / 100);
        }

        return $is_raw ? 0 : '0,00';
    }


    /**
     * Get full name: first name + last name
     */
    public function fullname()
    {
        $fullname = $this->firstname;
        if ( $this->lastname )
        {
            $fullname .= ' '. $this->lastname;
        }

        return $fullname;
    }


    /**
     * Find a LemonwayAccount model by "lemonway_account_id" attribute
     */
    public function findByAccountId($lemonway_account_id)
    {
        return self::get()->where(['lemonway_account_id' => $lemonway_account_id])->one();
    }


    /**
     * Account URL for the Lemonway Dashboard v2
     */
    public function url($type = 'overview')
    {
        return Yii::app()->lemonway->get_dashboard_url() . '/accounts/'. $this->lemonway_internal_id .'/'. $type;
    }


    /**
     * Title used for this model
     */
    public function title()
    {
        if ( $this->is_legal == 1 )
        {
            return $this->company_name;
        }

        return $this->fullname();
    }
}