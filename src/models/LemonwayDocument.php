<?php
/**
 * @package dzlab\lemonway\models 
 */

namespace dzlab\lemonway\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\modules\asset\models\AssetFile;
use dzlab\lemonway\models\_base\LemonwayDocument as BaseLemonwayDocument;
use dzlab\lemonway\models\LemonwayAccount;
use user\models\User;
use Yii;

/**
 * LemonwayDocument model class for "lemonway_document" database table
 *
 * Columns in table "lemonway_document" available as properties of the model,
 * and there are no model relations.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $user_id
 * @property integer $file_id
 * @property integer $lemonway_id
 * @property string $name
 * @property integer $status_type
 * @property integer $substatus_type
 * @property integer $document_type
 * @property integer $validity_date
 * @property string $comment
 * @property integer $last_sync_date
 * @property string $last_sync_endpoint
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 */
class LemonwayDocument extends BaseLemonwayDocument
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['user_id, file_id, lemonway_id, name, status_type, document_type, last_sync_date, last_sync_endpoint, created_date, created_uid, updated_date, updated_uid', 'required'],
			['user_id, file_id, lemonway_id, status_type, substatus_type, document_type, validity_date, last_sync_date, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['name', 'length', 'max'=> 255],
			['comment', 'length', 'max'=> 512],
            ['last_sync_endpoint', 'length', 'max'=> 128],
			['uuid', 'length', 'max'=> 36],
			['substatus_type, validity_date, comment, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['user_id, file_id, lemonway_id, name, status_type, substatus_type, document_type, validity_date, comment, last_sync_date, last_sync_endpoint, created_date, created_uid, updated_date, updated_uid, uuid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'user' => [self::BELONGS_TO, User::class, 'user_id'],
            'lemonwayAccount' => [self::BELONGS_TO, LemonwayAccount::class, ['user_id' => 'user_id']],
            'file' => [self::BELONGS_TO, AssetFile::class, 'file_id'],
            'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
            'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'validity_date' => 'd/m/Y'
                ],
            ],

        ], parent::behaviors());
    }

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'user_id' => Yii::t('app', 'Account'),
			'file_id' => Yii::t('app', 'File'),
			'lemonway_id' => Yii::t('app', 'Lemonway ID'),
			'name' => Yii::t('app', 'Name'),
			'status_type' => Yii::t('app', 'Status'),
			'substatus_type' => Yii::t('app', 'Substatus Type'),
			'document_type' => Yii::t('app', 'Type'),
			'validity_date' => Yii::t('app', 'Validity Date'),
			'comment' => Yii::t('app', 'Comment'),
            'last_sync_date' => Yii::t('app', 'Last Sync'),
            'last_sync_endpoint' => Yii::t('app', 'Last Sync Endpoint'),
			'created_date' => Yii::t('app', 'Uploaded Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.lemonway_id', $this->lemonway_id);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.status_type', $this->status_type);
        $criteria->compare('t.substatus_type', $this->substatus_type);
        $criteria->compare('t.document_type', $this->document_type);
        $criteria->compare('t.validity_date', $this->validity_date);
        $criteria->compare('t.comment', $this->comment, true);
        $criteria->compare('t.last_sync_date', $this->last_sync_date);
        $criteria->compare('t.last_sync_endpoint', $this->last_sync_endpoint);
        $criteria->compare('t.created_date', $this->created_date);
        $criteria->compare('t.updated_date', $this->updated_date);
        $criteria->compare('t.uuid', $this->uuid, true);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['user_id' => true]]
        ]);
    }


    /**
     * LemonwayDocument models list
     * 
     * @return array
     */
    public function lemonwaydocument_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['user_id', 'user_id', 'file_id'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = LemonwayDocument::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('user_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Get "status_type" specific label
     */
    public function status_type_label($status_type = null)
    {
        if ( $status_type === null )
        {
            $status_type = $this->status_type;
        }

        $vec_labels = $this->status_type_labels();
        return isset($vec_labels[$status_type]) ? $vec_labels[$status_type] : '';
    }


    /**
     * Get "status_type" labels
     */
    public function status_type_labels()
    {
        return [
            0   => Yii::t('lemonway', 'Document put on hold, waiting for another document'),
            1   => Yii::t('lemonway', 'Received, need manual validation'),
            2   => Yii::t('lemonway', 'Accepted'),
            3   => Yii::t('lemonway', 'Rejected'),
            4   => Yii::t('lemonway', 'Rejected. Unreadable by human (Cropped, blur, glare…)'),
            5   => Yii::t('lemonway', 'Rejected. Expired (Expiration Date is passed)'),
            6   => Yii::t('lemonway', 'Rejected. Wrong Type (Document not accepted)'),
            7   => Yii::t('lemonway', 'Rejected. Wrong Name (Name not matching user information)'),
            8   => Yii::t('lemonway', 'Rejected. Duplicated Document')
        ];
    }


    /**
     * Get "substatus_type" specific label
     * 
     * Only applies when status_type = 1
     */
    public function substatus_type_label($substatus_type = null)
    {
        if ( $substatus_type === null )
        {
            $substatus_type = $this->substatus_type;
        }

        $vec_labels = $this->substatus_type_labels();
        return isset($vec_labels[$substatus_type]) ? $vec_labels[$substatus_type] : '';
    }


    /**
     * Get "substatus_type" labels
     * 
     * Only applies when status_type = 1
     */
    public function substatus_type_labels()
    {
        return [
            1   => Yii::t('lemonway', 'Document type not processable automatically'),
            2   => Yii::t('lemonway', 'Unreadable by computer (Cropped, blur, glare…)'),
            3   => Yii::t('lemonway', 'Wrong Type'),
            4   => Yii::t('lemonway', 'Wrong Name (Name not matching user information)'),
            5   => Yii::t('lemonway', 'File too big (File should not be over 4MB to be processed automatically)'),
            6   => Yii::t('lemonway', 'Automatic Controls Failed'),
            7   => Yii::t('lemonway', 'Need 2 sides of the document in one file'),
            8   => Yii::t('lemonway', 'Unknown')
        ];
    }


    /**
     * Get "document_type" specific label
     */
    public function document_type_label($document_type = null)
    {
        if ( $document_type === null )
        {
            $document_type = $this->document_type;
        }

        $vec_labels = $this->document_type_labels();
        return isset($vec_labels[$document_type]) ? $vec_labels[$document_type] : '';
    }


    /**
     * Get "document_type" labels
     */
    public function document_type_labels()
    {
        return [
            0   => Yii::t('lemonway', 'ID card (both sides in one file)'),
            1   => Yii::t('lemonway', 'Proof of address'),
            2   => Yii::t('lemonway', 'Scan of a proof of IBAN'),
            3   => Yii::t('lemonway', 'Passport (European Union)'),
            4   => Yii::t('lemonway', 'Passport (outside the European Union)'),
            5   => Yii::t('lemonway', 'Residence permit (both sides in one file)'),
            6   => Yii::t('lemonway', 'Other document type'),
            7   => Yii::t('lemonway', 'Official company registration document (Kbis extract or equivalent)'),
            11  => Yii::t('lemonway', 'Driver licence (both sides in one file)'),
            12  => Yii::t('lemonway', 'Status'),
            13  => Yii::t('lemonway', 'Selfie'),
            14  => Yii::t('lemonway', 'Other document type'),
            15  => Yii::t('lemonway', 'Other document type'),
            16  => Yii::t('lemonway', 'Other document type'),
            17  => Yii::t('lemonway', 'Other document type'),
            18  => Yii::t('lemonway', 'Other document type'),
            19  => Yii::t('lemonway', 'Other document type'),
            20  => Yii::t('lemonway', 'Other document type'),
            21  => Yii::t('lemonway', 'SDD mandate')
        ];
    }


    /**
     * Document URL for the Lemonway Dashboard v2
     */
    public function url()
    {
        if ( $this->lemonwayAccount )
        {
            return Yii::app()->lemonway->get_dashboard_url() . '/accounts/'. $this->lemonwayAccount->lemonway_internal_id .'/documents';
        }

        return Yii::app()->lemonway->get_dashboard_url();
    }
}