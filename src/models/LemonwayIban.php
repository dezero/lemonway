<?php
/**
 * @package dzlab\lemonway\models 
 */

namespace dzlab\lemonway\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\modules\asset\models\AssetFile;
use dz\modules\asset\models\AssetImage;
use dzlab\lemonway\models\_base\LemonwayIban as BaseLemonwayIban;
use dzlab\lemonway\models\LemonwayAccount;
use dzlab\lemonway\models\LemonwayTransaction;
use user\models\User;
use Yii;

/**
 * LemonwayIban model class for "lemonway_iban" database table
 *
 * Columns in table "lemonway_iban" available as properties of the model,
 * followed by relations of table "lemonway_iban" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $lemonway_iban_id
 * @property integer $user_id
 * @property integer $is_virtual
 * @property integer $bank_account_id
 * @property integer $status_type
 * @property integer $iban_type
 * @property string $holder
 * @property string $bic
 * @property string $iban
 * @property string $domiciliation1
 * @property string $domiciliation2
 * @property string $comment
 * @property integer $pdf_file_id
 * @property integer $qr_image_id
 * @property string $options_json
 * @property integer $last_sync_date
 * @property string $last_sync_endpoint
 * @property integer $disable_date
 * @property integer $disable_uid
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $bankAccount
 * @property mixed $createdUser
 * @property mixed $disabledUser
 * @property mixed $pdfFile
 * @property mixed $qrImage
 * @property mixed $updatedUser
 * @property mixed $user
 * @property mixed $lemonwayTransactions
 */
class LemonwayIban extends BaseLemonwayIban
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['lemonway_iban_id, user_id, status_type, holder, iban, last_sync_date, last_sync_endpoint, created_date, created_uid, updated_date, updated_uid', 'required'],
			['lemonway_iban_id, user_id, is_virtual, bank_account_id, status_type, iban_type, pdf_file_id, qr_image_id, last_sync_date, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['holder, domiciliation1, domiciliation2, options_json', 'length', 'max'=> 255],
			['bic', 'length', 'max'=> 16],
			['iban, uuid', 'length', 'max'=> 36],
			['comment', 'length', 'max'=> 512],
			['last_sync_endpoint', 'length', 'max'=> 128],
            ['is_virtual, bank_account_id, iban_type, bic, domiciliation1, domiciliation2, comment, pdf_file_id, qr_image_id, options_json, disable_date, disable_uid, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
            ['lemonway_iban_id, user_id, is_virtual, bank_account_id, status_type, iban_type, holder, bic, iban, domiciliation1, domiciliation2, comment, pdf_file_id, qr_image_id, options_json, last_sync_date, last_sync_endpoint, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid, uuid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'user' => [self::BELONGS_TO, User::class, 'user_id'],
            'lemonwayAccount' => [self::BELONGS_TO, LemonwayAccount::class, ['user_id' => 'user_id']],
			'user' => [self::BELONGS_TO, User::class, ['user_id' => 'id']],
            'pdfFile' => [self::BELONGS_TO, AssetFile::class, ['pdf_file_id' => 'file_id']],
            'qrImage' => [self::BELONGS_TO, AssetImage::class, ['qr_image_id' => 'file_id']],
            'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
            'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],
            'disabledUser' => [self::BELONGS_TO, User::class, ['disable_uid' => 'id']],
            'transactions' => [self::HAS_MANY, LemonwayTransaction::class, 'receiver_lemonway_iban_id'],

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'last_sync_date' => 'd/m/Y - H:i',
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'lemonway_iban_id' => Yii::t('app', 'Lemonway Iban'),
			'user_id' => null,
            'is_virtual' => Yii::t('app', 'Virtual IBAN?'),
            'bank_account_id' => null,
			'status_type' => Yii::t('app', 'Status Type'),
			'iban_type' => Yii::t('app', 'Iban Type'),
			'holder' => Yii::t('app', 'Holder'),
			'bic' => Yii::t('app', 'Bic'),
			'iban' => Yii::t('app', 'Iban'),
			'domiciliation1' => Yii::t('app', 'Domiciliation1'),
			'domiciliation2' => Yii::t('app', 'Domiciliation2'),
			'comment' => Yii::t('app', 'Comment'),
            'pdf_file_id' => Yii::t('app', 'PDF File'),
            'qr_image_id' => Yii::t('app', 'QR Code Image'),
            'options_json' => Yii::t('app', 'Options Json'),
			'last_sync_date' => Yii::t('app', 'Last Sync'),
			'last_sync_endpoint' => Yii::t('app', 'Last Sync Endpoint'),
			'disable_date' => Yii::t('app', 'Disabled Date'),
			'disable_uid' => null,
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
            'bankAccount' => null,
			'createdUser' => null,
			'disabledUser' => null,
			'pdfFile' => null,
            'qrImage' => null,
            'updatedUser' => null,
            'user' => null,
            'lemonwayTransactions' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.lemonway_iban_id', $this->lemonway_iban_id);
        $criteria->compare('t.user_id', $this->user_id);
        $criteria->compare('t.is_virtual', $this->is_virtual);
        $criteria->compare('t.bank_account_id', $this->bank_account_id);
        $criteria->compare('t.status_type', $this->status_type);
        $criteria->compare('t.iban_type', $this->iban_type);
        $criteria->compare('t.holder', $this->holder, true);
        $criteria->compare('t.bic', $this->bic, true);
        $criteria->compare('t.iban', $this->iban, true);
        $criteria->compare('t.domiciliation1', $this->domiciliation1, true);
        $criteria->compare('t.domiciliation2', $this->domiciliation2, true);
        $criteria->compare('t.comment', $this->comment, true);
        $criteria->compare('t.options_json', $this->options_json, true);
        $criteria->compare('t.last_sync_date', $this->last_sync_date);
        $criteria->compare('t.last_sync_endpoint', $this->last_sync_endpoint, true);
        $criteria->compare('t.disable_date', $this->disable_date);
        $criteria->compare('t.created_date', $this->created_date);
        $criteria->compare('t.updated_date', $this->updated_date);
        $criteria->compare('t.uuid', $this->uuid, true);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['lemonway_iban_id' => true]]
        ]);
    }


    /**
     * LemonwayIban models list
     * 
     * @return array
     */
    public function lemonwayiban_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['lemonway_iban_id', 'holder'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = LemonwayIban::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('lemonway_iban_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Get "status_type" specific label
     */
    public function status_type_label($status_type = null)
    {
        if ( $status_type === null )
        {
            $status_type = $this->status_type;
        }

        $vec_labels = $this->status_type_labels();
        return isset($vec_labels[$status_type]) ? $vec_labels[$status_type] : '';
    }


    /**
     * Get "status_type" labels
     */
    public function status_type_labels()
    {
        return [
            1   => Yii::t('lemonway', 'None') . ' (1)',
            2   => Yii::t('lemonway', 'Internal (API)') . ' (2)',
            3   => Yii::t('lemonway', 'Not used') . ' (3)',
            4   => Yii::t('lemonway', 'Waiting to be verified by Lemon Way') . ' (4)',
            5   => Yii::t('lemonway', 'Activated') . ' (5)',
            6   => Yii::t('lemonway', 'Rejected by the bank') . ' (6)',
            7   => Yii::t('lemonway', 'Rejected, no owner') . ' (7)',
            8   => Yii::t('lemonway', 'Deactivated') . ' (8)',
            9   => Yii::t('lemonway', 'Rejected by Lemon Way') . ' (9)',
        ];
    }


    /**
     * Get "iban_type" specific label
     */
    public function iban_type_label($iban_type = null)
    {
        if ( $iban_type === null )
        {
            $iban_type = $this->iban_type;
        }

        $vec_labels = $this->iban_type_labels();
        return isset($vec_labels[$iban_type]) ? $vec_labels[$iban_type] : '';
    }


    /**
     * Get "iban_type" labels
     */
    public function iban_type_labels()
    {
        return [
            1   => Yii::t('lemonway', 'Merchant IBAN'),
            2   => Yii::t('lemonway', 'IBAN virtual client'),
        ];
    }


    /**
     * Check if IBAN is a virtual IBAN account
     */
    public function is_virtual()
    {
        return $this->iban_type === 2;
    }


    /**
     * Create a document or image file given an encoded content in base64
     */
    public function create_file($base64_content, $file_field = 'pdf_file_id')
    {
        // DO NOT ALLOW TO UPLOAD THE FILE TWICE
        // Check if already exists a document or image file uploaded previously
        $file_model = null;
        if ( $file_field === 'pdf_file_id' && $this->pdfFile )
        {
            $file_model = $this->pdfFile;
        }
        else if ( $file_field === 'qr_image_id' && $this->qrImage )
        {
            $file_model = $this->qrImage;
        }

        if ( !empty($base64_content) && empty($file_model) )
        {
            // Private lemonway directory. By default, /storage/lemonway
            $lemonway_path = Yii::app()->path->get('lemonwayPath');
            $lemonway_dir = Yii::app()->file->set($lemonway_path);
            if ( ! $lemonway_dir->isDir )
            {
                $lemonway_dir->createDir(Yii::app()->file->default_permissions, $lemonway_path);
            }

            // Create a directory for each Lemonway account
            $lemonway_account_path = $lemonway_path . DIRECTORY_SEPARATOR . $this->user_id;
            $lemonway_account_dir = Yii::app()->file->set($lemonway_account_path);
            if ( ! $lemonway_account_dir->isDir )
            {
                $lemonway_account_dir->createDir(Yii::app()->file->default_permissions, $lemonway_account_path);
            }

            // Create a new document or image in the file system
            $file_extension = 'pdf';
            if ( $file_field === 'qr_image_id' )
            {
                $file_extension = 'png';
            }
            $lemonway_iban_file_path = $lemonway_account_path . DIRECTORY_SEPARATOR . 'iban_'. $this->lemonway_iban_id .'.'. $file_extension;
            $lemonway_iban_file = Yii::app()->file->set($lemonway_iban_file_path);
            if ( ! $lemonway_iban_file->getExists() )
            {
                $lemonway_iban_file->create();
            }
            $lemonway_iban_file->setContents(base64_decode($base64_content));            
            // $lemonway_iban_file->setPermissions(644);

            // Finally, save the file in database
            if ( $file_field === 'qr_image_id' )
            {
                return $this->save_image_from_path($file_field, $lemonway_iban_file_path);
            }
            
            return $this->save_file_from_path($file_field, $lemonway_iban_file_path);
        }

        return null;
    }


    /**
     * Create a PDF document file given an encoded content in base64
     */
    public function create_pdf_file($base64_content)
    {
        return $this->create_file($base64_content, 'pdf_file_id');
    }


    /**
     * Create a PNG image file given an encoded content in base64
     */
    public function create_image_file($base64_content)
    {
        return $this->create_file($base64_content, 'qr_image_id');
    }


    /**
     * Bank accounts URL for the Lemonway Dashboard v2
     */
    public function url()
    {
        if ( $this->lemonwayAccount )
        {
            if ( $this->is_virtual() )
            {
                return Yii::app()->lemonway->get_dashboard_url() . '/accounts/'. $this->lemonwayAccount->lemonway_internal_id .'/payment-methods/virtual-ibans';
            }
            
            return Yii::app()->lemonway->get_dashboard_url() . '/accounts/'. $this->lemonwayAccount->lemonway_internal_id .'/bank-accounts';
        }

        return Yii::app()->lemonway->get_dashboard_url();
    }
}