<?php
/**
 * @package dzlab\lemonway\models 
 */

namespace dzlab\lemonway\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\modules\settings\models\StatusHistory;
use dzlab\lemonway\models\_base\LemonwayTransaction as BaseLemonwayTransaction;
use dzlab\lemonway\models\LemonwayAccount;
use user\models\User;
use Yii;

/**
 * LemonwayTransaction model class for "lemonway_transaction" database table
 *
 * Columns in table "lemonway_transaction" available as properties of the model,
 * followed by relations of table "lemonway_transaction" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $transaction_id
 * @property integer $lemonway_id
 * @property string $transaction_type
 * @property integer $receiver_user_id
 * @property string $receiver_lemonway_account_id
 * @property integer $receiver_lemonway_iban_id
 * @property integer $sender_user_id
 * @property string $sender_lemonway_account_id
 * @property integer $total_amount
 * @property integer $commission_amount
 * @property string $reference
 * @property integer $method_type
 * @property integer $status_type
 * @property integer $transaction_date
 * @property integer $execution_date
 * @property string $comment
 * @property integer $is_auto_commission
 * @property integer $original_transaction_id
 * @property integer $commission_transaction_id
 * @property string $request_token
 * @property string $response_json
 * @property string $request_webkit_json
 * @property string $response_webkit_json
 * @property integer $error_code
 * @property string $error_message
 * @property string $psp_message
 * @property integer $last_sync_date
 * @property string $last_sync_endpoint
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $createdUser
 * @property mixed $originalTransaction
 * @property mixed $lemonwayTransactions
 * @property mixed $receiverLemonwayAccount
 * @property mixed $receiverLemonwayIban
 * @property mixed $receiverUser
 * @property mixed $senderLemonwayAccount
 * @property mixed $senderUser
 * @property mixed $updatedUser
 */
class LemonwayTransaction extends BaseLemonwayTransaction
{
    /**
     * @var integer User_id filter
     */
    public $user_id;


	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
            ['transaction_type, receiver_user_id, receiver_lemonway_account_id, total_amount, transaction_date, created_date, created_uid, updated_date, updated_uid', 'required'],
            ['lemonway_id, receiver_user_id, receiver_lemonway_iban_id, sender_user_id, total_amount, commission_amount, method_type, status_type, transaction_date, execution_date, is_auto_commission, original_transaction_id, commission_transaction_id, error_code, last_sync_date, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
            ['receiver_lemonway_account_id, sender_lemonway_account_id, reference', 'length', 'max'=> 64],
            ['comment', 'length', 'max'=> 140],
            ['request_token, error_message, psp_message', 'length', 'max'=> 255],
            ['request_webkit_json, response_webkit_json', 'length', 'max'=> 512],
            ['last_sync_endpoint', 'length', 'max'=> 128],
            ['uuid', 'length', 'max'=> 36],
            ['transaction_type', 'in', 'range' => ['in', 'out', 'p2p', 'refund']],
            ['lemonway_id, receiver_lemonway_iban_id, sender_user_id, sender_lemonway_account_id, commission_amount, reference, method_type, status_type, execution_date, comment, is_auto_commission, original_transaction_id, commission_transaction_id, request_token, response_json, request_webkit_json, response_webkit_json, error_code, error_message, psp_message, last_sync_date, last_sync_endpoint, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
            ['response_json', 'safe'],
            ['transaction_id, lemonway_id, transaction_type, receiver_user_id, receiver_lemonway_account_id, receiver_lemonway_iban_id, sender_user_id, sender_lemonway_account_id, total_amount, commission_amount, reference, method_type, status_type, transaction_date, execution_date, comment, is_auto_commission, original_transaction_id, commission_transaction_id, request_token, response_json, request_webkit_json, response_webkit_json, error_code, error_message, psp_message, last_sync_date, last_sync_endpoint, created_date, created_uid, updated_date, updated_uid, uuid, user_id', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'receiverUser' => [self::BELONGS_TO, User::class, ['receiver_user_id' => 'id']],
            'receiverAccount' => [self::BELONGS_TO, LemonwayAccount::class, ['receiver_lemonway_account_id' => 'lemonway_account_id']],
            'receiverIban' => [self::BELONGS_TO, LemonwayIban::class, ['receiver_lemonway_iban_id' => 'lemonway_iban_id']],
            'senderUser' => [self::BELONGS_TO, User::class, ['sender_user_id' => 'id']],
            'senderAccount' => [self::BELONGS_TO, LemonwayAccount::class, ['sender_lemonway_account_id' => 'lemonway_account_id']],
            'originalTransaction' => [self::BELONGS_TO, LemonwayTransaction::class, ['original_transaction_id' => 'lemonway_transaction_id']],
			'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],
            'transactions' => [self::HAS_MANY, LemonwayTransaction::class, 'original_transaction_id'],

            // Custom relations
            'statusHistory' => [self::HAS_MANY, StatusHistory::class, ['entity_id' => 'transaction_id'], 'condition' => 'statusHistory.entity_type = "LemonwayTransaction"', 'order' => 'statusHistory.history_id DESC'],
		];
	}


    /**
     * External behaviors
     */
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'execution_date' => 'd/m/Y - H:i',
                    'transaction_date' => 'd/m/Y - H:i',
                    'last_sync_date' => 'd/m/Y - H:i',
                ],
            ],

        ], parent::behaviors());
    }

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'transaction_id' => Yii::t('app', 'Transaction'),
            'lemonway_id' => Yii::t('app', 'Lemonway'),
            'transaction_type' => Yii::t('app', 'Type'),
            'receiver_user_id' => Yii::t('app', 'Receiver'),
            'receiver_lemonway_account_id' => Yii::t('app', 'Receiver'),
            'receiver_lemonway_iban_id' => Yii::t('app', 'IBAN'),
            'sender_user_id' => Yii::t('app', 'Sender'),
            'sender_lemonway_account_id' => Yii::t('app', 'Sender'),
            'total_amount' => Yii::t('app', 'Amount'),
            'commission_amount' => Yii::t('app', 'Commission'),
            'reference' => Yii::t('app', 'Reference'),
            'method_type' => Yii::t('app', 'Method'),
            'status_type' => Yii::t('app', 'Status'),
            'transaction_date' => Yii::t('app', 'Date'),
            'execution_date' => Yii::t('app', 'Execution Date'),
            'comment' => Yii::t('app', 'Comment'),
            'is_auto_commission' => Yii::t('app', 'Is Auto Commission'),
            'original_transaction_id' => Yii::t('app', 'Original Transaction'),
            'commission_transaction_id' => Yii::t('app', 'Comission Transaction'),
            'request_token' => Yii::t('app', 'Request Token'),
            'response_json' => Yii::t('app', 'Response Json'),
            'request_webkit_json' => Yii::t('app', 'Request Webkit Json'),
            'response_webkit_json' => Yii::t('app', 'Response Webkit Json'),
            'error_code' => Yii::t('app', 'Error Code'),
            'error_message' => Yii::t('app', 'Error Message'),
            'psp_message' => Yii::t('app', 'Psp Message'),
            'last_sync_date' => Yii::t('app', 'Last Sync'),
            'last_sync_endpoint' => Yii::t('app', 'Last Sync Endpoint'),
            'created_date' => Yii::t('app', 'Created Date'),
            'created_uid' => null,
            'updated_date' => Yii::t('app', 'Updated Date'),
            'updated_uid' => null,
            'uuid' => Yii::t('app', 'Uuid'),
            'createdUser' => null,
            'receiverLemonwayAccount' => null,
            'receiverLemonwayIban' => null,
            'receiverUser' => null,
            'senderLemonwayAccount' => null,
            'senderUser' => null,
            'updatedUser' => null,
            'user_id' => Yii::t('app', 'Account'),
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        // Disable non-synced Lemonway transactions
        $criteria->addCondition('t.lemonway_id IS NOT NULL');

        $criteria->compare('t.transaction_id', $this->transaction_id);
        $criteria->compare('t.lemonway_id', $this->lemonway_id);

        // Filter by TRANSACTION / METHOD TYPE
        if ( $this->transaction_type !== null && $this->transaction_type !== '-' )
        {
            // Filter by METHOD TYPE
            if ( is_numeric($this->transaction_type) )
            {
                // P2P
                if ( $this->transaction_type == 4 )
                {
                    $criteria->compare('t.method_type', $this->transaction_type);
                }
                else
                {
                    $criteria->addCondition('t.transaction_type <> "p2p"');
                    $criteria->compare('t.method_type', $this->transaction_type);
                }
            }

            // Filter by TRANSACTION TYPE
            else
            {
                $criteria->compare('t.transaction_type', $this->transaction_type);
            }
        }

        // Filter by STATUS TYPE
        if ( $this->status_type !== null && $this->status_type !== '-' )
        {
            // Filter by P2P status type
            if ( preg_match("/^p2p\-/", $this->status_type) )
            {
                $criteria->compare('t.status_type', substr($this->status_type, 4));
                $criteria->compare('t.transaction_type', 'p2p');
            }

            // Filter by IN/OUT status type
            else
            {
                $criteria->compare('t.status_type', $this->status_type);
                $criteria->addCondition('t.transaction_type <> "p2p"');
            }
            
        }

        // Filter by user
        if ( $this->user_id )
        {
            $criteria->addCondition('t.sender_user_id LIKE :user_id OR t.receiver_user_id = :user_id');
            $criteria->addParams([
                ':user_id' => $this->user_id
            ]);
        }
        
        // Filter by SENDER
        if ( $this->sender_user_id )
        {
            if ( is_numeric($this->sender_user_id) )
            {
                $criteria->compare('t.sender_user_id', $this->sender_user_id);
            }
            else
            {
                $criteria->with[] = 'senderAccount';
                $criteria->addCondition('senderAccount.company_name LIKE CONCAT("%", :sender_user_id, "%") OR senderAccount.firstname LIKE CONCAT("%", :sender_user_id, "%") OR senderAccount.firstname LIKE CONCAT("%", :sender_user_id, "%") OR senderAccount.lastname LIKE CONCAT("%", :sender_user_id, "%") OR CONCAT(senderAccount.firstname, " ", senderAccount.lastname) LIKE CONCAT(:sender_user_id, "%")');
                $criteria->addParams([
                    ':sender_user_id' => $this->sender_user_id
                ]);
            }
        }

        // Filter by RECEIVER
        if ( $this->receiver_user_id )
        {
            if ( is_numeric($this->receiver_user_id) )
            {
                $criteria->compare('t.receiver_user_id', $this->receiver_user_id);
            }
            else
            {
                $criteria->with[] = 'receiverAccount';
                $criteria->addCondition('receiverAccount.company_name LIKE CONCAT("%", :receiver_user_id, "%") OR receiverAccount.firstname LIKE CONCAT("%", :receiver_user_id, "%") OR receiverAccount.firstname LIKE CONCAT("%", :receiver_user_id, "%") OR receiverAccount.lastname LIKE CONCAT(:receiver_user_id, "%") OR CONCAT(receiverAccount.firstname, " ", receiverAccount.lastname) LIKE CONCAT(:receiver_user_id, "%")');
                $criteria->addParams([
                    ':receiver_user_id' => $this->receiver_user_id
                ]);
            }
        }

        // Filter by DATE
        if ( $this->transaction_date )
        {
            $transaction_unix_date = DateHelper::date_format_to_unix($this->transaction_date);
            $criteria->addCondition('t.transaction_date >= '. $transaction_unix_date);
            $criteria->addCondition('t.transaction_date < '. ($transaction_unix_date + 86400) );
        }

        // Filter by AMOUNT
        if ( $this->total_amount && $this->total_amount != '0' )
        {
            $criteria->compare('t.total_amount', Yii::app()->number->unformatNumber($this->total_amount) * 100);
        }

        // Order by date
        $criteria->order = 't.transaction_date DESC';

        /*
        $criteria->compare('t.total_amount', $this->total_amount);
        $criteria->compare('t.commission_amount', $this->commission_amount);
        $criteria->compare('t.reference', $this->reference, true);
        $criteria->compare('t.method_type', $this->method_type);
        $criteria->compare('t.transaction_date', $this->transaction_date);
        $criteria->compare('t.execution_date', $this->execution_date);
        $criteria->compare('t.comment', $this->comment, true);
        $criteria->compare('t.is_auto_commission', $this->is_auto_commission);
        $criteria->compare('t.original_transaction_id', $this->original_transaction_id);
        $criteria->compare('t.commission_transaction_id', $this->commission_transaction_id);
        $criteria->compare('t.request_token', $this->request_token, true);
        $criteria->compare('t.response_json', $this->response_json, true);
        $criteria->compare('t.request_webkit_json', $this->request_webkit_json, true);
        $criteria->compare('t.response_webkit_json', $this->response_webkit_json, true);
        $criteria->compare('t.error_code', $this->error_code);
        $criteria->compare('t.error_message', $this->error_message, true);
        $criteria->compare('t.psp_message', $this->psp_message, true);
        $criteria->compare('t.last_sync_date', $this->last_sync_date);
        $criteria->compare('t.last_sync_endpoint', $this->last_sync_endpoint, true);
        $criteria->compare('t.created_date', $this->created_date);
        $criteria->compare('t.updated_date', $this->updated_date);
        $criteria->compare('t.uuid', $this->uuid, true);
        */

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['lemonway_transaction_id' => false]]
        ]);
    }


    /**
     * LemonwayTransaction models list
     * 
     * @return array
     */
    public function lemonwaytransaction_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['lemonway_transaction_id', 'uuid'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = LemonwayTransaction::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('lemonway_transaction_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Get "transaction_type" labels
     */
    public function transaction_type_labels()
    {
        return [
            'in'        => Yii::t('lemonway', 'Money In'),
            'out'       => Yii::t('lemonway', 'Money Out'),
            'p2p'       => Yii::t('lemonway', 'P2P'),
            // 'refund'    => Yii::t('lemonway', 'Refund'),
        ];
    }


    /**
     * Get "transaction_type" specific label
     */
    public function transaction_type_label($transaction_type = null)
    {
        if ( $transaction_type === null )
        {
            $transaction_type = $this->transaction_type;
        }

        $vec_labels = $this->transaction_type_labels();
        return isset($vec_labels[$transaction_type]) ? $vec_labels[$transaction_type] : '';
    }

    /**
     * Get "status_type" specific label
     */
    public function status_type_label($status_type = null)
    {
        if ( $status_type === null )
        {
            $status_type = $this->status_type;
        }

        $vec_labels = $this->status_type_labels();
        return isset($vec_labels[$status_type]) ? $vec_labels[$status_type] : '';
    }


    /**
     * Get "status_type" labels
     */
    public function status_type_labels($is_p2p = false)
    {
        // P2P
        if ( $this->method_type == 4 || $this->transaction_type === 'p2p' || $is_p2p )
        {
            return [
                0   => Yii::t('lemonway', 'Pending - P2P'),
                3   => Yii::t('lemonway', 'Success - P2P'),
            ];
        }

        // Money-In / Money-Out
        return [
            0   => Yii::t('lemonway', 'Success'),
            3   => Yii::t('lemonway', 'LemonWay Error'),
            4   => Yii::t('lemonway', 'Pending'),
            6   => Yii::t('lemonway', 'Error PSP'),
            7   => Yii::t('lemonway', 'Cancelled'),
            14  => Yii::t('lemonway', 'Scheduled'),
            16  => Yii::t('lemonway', 'Validation Pending'),
        ];
    }


    /**
     * Get "method_type" specific label
     */
    public function method_type_label($method_type = null)
    {
        if ( $method_type === null )
        {
            $method_type = $this->method_type;
        }

        $vec_labels = $this->method_type_labels();
        return isset($vec_labels[$method_type]) ? $vec_labels[$method_type] : '';
    }


    /**
     * Get "method_type" labels
     */
    public function method_type_labels()
    {
        return [
            0   => Yii::t('lemonway', 'Card'),
            1   => Yii::t('lemonway', 'Bank transfer (Money-In)'),
            3   => Yii::t('lemonway', 'Bank transfer (Money-Out)'),
            4   => Yii::t('lemonway', 'P2P'),
            6   => Yii::t('lemonway', 'VCC'),
            11  => Yii::t('lemonway', 'AGC'),
            12  => Yii::t('lemonway', 'CVB'),
            13  => Yii::t('lemonway', 'IDEAL'),
            14  => Yii::t('lemonway', 'DirectDebit'),
            15  => Yii::t('lemonway', 'Cheque'),
            16  => Yii::t('lemonway', 'Neosurf'),
            17  => Yii::t('lemonway', 'SoFort'),
            18  => Yii::t('lemonway', 'PFS Physical Card'),
            19  => Yii::t('lemonway', 'Multibanco'),
            20  => Yii::t('lemonway', 'Payshop'),
            21  => Yii::t('lemonway', 'MB WAY'),
            22  => Yii::t('lemonway', 'Polish Instant Transfer'),
            23  => Yii::t('lemonway', 'PAGARE'),
            24  => Yii::t('lemonway', 'MobilePay'),
            25  => Yii::t('lemonway', 'PayTrail'),
            26  => Yii::t('lemonway', 'WeChat (via PayTend)'),
            27  => Yii::t('lemonway', 'P24'),
            28  => Yii::t('lemonway', 'MoneyIn by TPE'),
            29  => Yii::t('lemonway', 'Trustly'),
            100 => Yii::t('lemonway', 'Canceled'),
        ];
    }


    /*
    |--------------------------------------------------------------------------
    | CUSTOM FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /**
     * Event "afterSave"
     *
     * This method is invoked after saving a record successfully
     */
    public function afterSave()
    {
        switch ( $this->scenario )
        {
            case 'insert':
            case 'update':
                if ( $this->status_type !== null )
                {
                    $this->save_status_history($this->status_type);
                }
            break;
        }

        return parent::afterSave();
    }


    /**
     * Save new status into status history
     */
    public function save_status_history($new_status, $comments = null)
    {
        // Get last StatusHistory model
        $last_status_history_model = StatusHistory::get()
            ->where([
                'entity_id'     => $this->transaction_id,
                'entity_type'   => 'LemonwayTransaction'
            ])
            ->order('history_id DESC')
            ->one();

        if ( ! $last_status_history_model || $last_status_history_model->status_type !== $new_status )
        {
            // Get status label
            if ( $comments === null )
            {
                $comments = $this->status_type_label($new_status);
            }

            $status_history_model = Yii::createObject(StatusHistory::class);
            $status_history_model->setAttributes([
                'entity_id'     => $this->transaction_id,
                'entity_type'   => 'LemonwayTransaction',
                'status_type'   => $new_status,
                'comments'      => $comments
            ]);

            if ( ! $status_history_model->save() )
            {
                Log::save_model_error($status_history_model);
                return false;
            }
        }

        return true;
    }


    /**
     * Return last StatusHistory model. It can be filtered by status_type
     */
    public function get_last_history_model($status_type = null)
    {
        $vec_conditions = [
            'entity_id'     => $this->transaction_id,
            'entity_type'   => 'LemonwayTransaction'
        ];

        // Filter by status?
        if ( $status_type !== null )
        {
            $vec_conditions['status_type'] = $status_type;
        }

        return StatusHistory::get()
            ->where($vec_conditions)
            ->orderBy('history_id DESC')
            ->limit(1)
            ->one();
    }


    /**
     * Get total_amount in raw or formatted value
     */
    public function get_amount($is_raw = false)
    {
        if ( $this->total_amount > 0 )
        {
            return $is_raw ? $this->total_amount : Yii::app()->number->formatNumber($this->total_amount / 100);
        }

        return $is_raw ? 0 : '0,00';
    }


    /**
     * Transaction description
     */
    public function get_description($user_id = null)
    {
        switch ( $this->transaction_type )
        {
            // Transaction IN
            case 'in':
                return 'Transaction IN';
            break;

            // Transaction OUT
            case 'out':
                return 'Transaction OUT';
            break;

            // Transaction P2P
            case 'p2p':
                if ( $user_id == $this->sender_user_id  )
                {
                    return 'Send to '. $this->receiverUser->fullname() .' ('. $this->receiver_user_id .')';
                }
                else if ( $user_id == $this->receiver_user_id )
                {
                    return 'Receive from '. $this->senderUser->fullname() .' ('. $this->sender_user_id .')';
                }
                else
                {
                    return 'Sender: '. $this->sender_user_id .' / Receiver: '. $this->receiver_user_id;
                }
            break;
        }

        return '';
    }


    /**
     * Update model with last data received from Lemonway API
     * 
     * @see LemonwayComponent::get_transaction_in()
     * @see LemonwayComponent::get_transaction_out()
     * @see LemonwayComponent::get_transaction_p2p()
     */
    public function sync_from_lemonway($vec_data)
    {
        if ( $vec_data['id'] == $this->lemonway_id )
        {
            switch ( $this->transaction_type )
            {
                // Transaction IN
                case 'in':
                    // Validate receiver LemonwayAccount model
                    $is_valid = true;
                    if ( isset($vec_data['receiverAccountId']) )
                    {
                        $receiver_account_model = LemonwayAccount::model()->findByAccountId($vec_data['receiverAccountId']);
                        if ( ! $receiver_account_model )
                        {
                            $is_valid = false;
                            Log::lemonway_error("LemonwayComponent::get_transaction_in({$transaction_id}) - RECEIVER LEMONWAY ACCOUNT does not exist: {$vec_data['receiverAccountId']}");
                        }
                        else if ( $this->receiver_lemonway_account_id !== $receiver_account_model->lemonway_account_id )
                        {
                            $is_valid = false;
                            Log::lemonway_warning("LemonwayComponent::get_transaction_in({$transaction_id}) - Receiver account inconsistency. Receiver saved on Crowdeen database = {$receiver_account_model->receiver_lemonway_account_id} / Receiver on Lemonway API = {$receiver_account_model->lemonway_account_id}");
                        }
                    }
                    else
                    {
                        $is_valid = false;
                        Log::lemonway_dev("LemonwayComponent::get_transaction_in({$transaction_id}) - Incorrect response: ". print_r($vec_data, true));
                    }

                    // Check if transaction type and "method" are the same. Methods values:
                    //   - 0  => Card,
                    //   - 1  => Bank transfer (Money-In),
                    //   - 3  => Bank transfer (Money-Out),
                    //   - 4  => P2P,
                    /*
                    if ( !isset($vec_data['method']) || $vec_data['method'] > 1 )
                    {
                        $is_valid = false;
                        Log::lemonway_warning("LemonwayComponent::get_transaction_in({$transaction_id}) - Transaction type inconsistency. Type saved on Crowdeen database = 'IN' / Type received on Lemonway API = {$vec_data['method']} - {$this->method_type_label($vec_data['method'])}");
                    }
                    */

                    // Transaction has been validated, save the model
                    if ( $is_valid )
                    {
                        $this->setAttributes([
                            'reference'             => ( isset($vec_data['reference']) && !empty($vec_data['reference']) ) ? $vec_data['reference'] : $this->reference,
                            // 'total_amount'           => $vec_data['creditAmount'],
                            'commission_amount'     => isset($vec_data['commissionAmount']) ? $vec_data['commissionAmount'] : $this->raw_attributes['commission_amount'],
                            'method_type'           => $vec_data['method'],
                            'status_type'           => $vec_data['status'],
                            // 'transaction_date'      => isset($vec_data['date']) ? $vec_data['date'] : time(),
                            'execution_date'        => ( isset($vec_data['executionDate']) && !empty($vec_data['executionDate']) ) ? $vec_data['executionDate'] : $this->raw_attributes['execution_date'],
                            'comment'               => ( isset($vec_data['comment']) && !empty($vec_data['comment']) ) ? $vec_data['comment'] : $this->comment,
                            'psp_message'           => ( isset($vec_data['PSP']) && isset($vec_data['PSP']['message']) ) ? $vec_data['PSP']['message'] : $this->psp_message,
                            'response_json'         => Json::encode($vec_data),
                            'last_sync_date'        => time(),
                            'last_sync_endpoint'    => 'GET___'. Yii::app()->lemonwayApi->last_endpoint
                        ]);

                        // Lemonway Commission?
                        if ( isset($vec_data['lemonWayCommission']) && isset($vec_data['lemonWayCommission']['idp2p']) && isset($vec_data['lemonWayCommission']['amount']) && !empty($vec_data['lemonWayCommission']['idp2p']) )
                        {
                            $this->commission_transaction_id = $vec_data['lemonWayCommission']['idp2p'];
                            $this->commission_amount = $vec_data['lemonWayCommission']['amount'];
                        }

                        if ( ! $this->save() )
                        {
                            Log::save_model_error($this);
                        }

                        return $this;
                    }
                break;

                // Transaction OUT
                case 'out':
                    // Validate sender LemonwayAccount model
                    $is_valid = true;
                    if ( isset($vec_data['senderAccountId']) )
                    {
                        // Sender is stored in "sender_lemonway_account_id" and/or "receiver_lemonway_account_id"
                        $sender_lemonway_account_id = $this->sender_lemonway_account_id;
                        if ( empty($sender_lemonway_account_id) )
                        {
                            $sender_lemonway_account_id = $this->receiver_lemonway_account_id;
                        }


                        $sender_account_model = LemonwayAccount::model()->findByAccountId($vec_data['senderAccountId']);
                        if ( ! $sender_account_model )
                        {
                            $is_valid = false;
                            Log::lemonway_error("LemonwayComponent::get_transaction_out({$transaction_id}) - SENDER LEMONWAY ACCOUNT does not exist: {$vec_data['senderAccountId']}");
                        }
                        else if ( $sender_lemonway_account_id !== $sender_account_model->lemonway_account_id )
                        {
                            $is_valid = false;
                            Log::lemonway_warning("LemonwayComponent::get_transaction_out({$transaction_id}) - Sender account inconsistency. Sender saved on Crowdeen database = {$sender_lemonway_account_id} / Sender on Lemonway API = {$sender_account_model->lemonway_account_id}");
                        }
                    }
                    else
                    {
                        $is_valid = false;
                        Log::lemonway_dev("LemonwayComponent::get_transaction_out({$transaction_id}) - Incorrect response: ". print_r($vec_data, true));
                    }

                    // Check if transaction type and "method" are the same. Methods values:
                    //   - 0  => Card,
                    //   - 1  => Bank transfer (Money-In),
                    //   - 3  => Bank transfer (Money-Out),
                    //   - 4  => P2P,
                    /*
                    if ( !isset($vec_data['method']) || $vec_data['method'] != 3 )
                    {
                        $is_valid = false;
                        Log::lemonway_warning("LemonwayComponent::get_transaction_out({$transaction_id}) - Transaction type inconsistency. Type saved on Crowdeen database = 'OUT' / Type received on Lemonway API = {$vec_data['method']} - {$this->method_type_label($vec_data['method'])}");
                    }
                    */

                    // Strange situation - "method" = 0 and "status" = 3, when it should be "method" = 3 and "status" = 0
                    if ( !isset($vec_data['method']) || $vec_data['method'] != 3 )
                    {
                        $vec_data['method'] = 3;
                        if ( $vec_data['status'] == 3 )
                        {
                            $vec_data['status'] = 0;
                        }
                    }

                    // Transaction has been validated, save the model
                    if ( $is_valid )
                    {
                        $this->setAttributes([
                            'reference'                 => ( isset($vec_data['reference']) && !empty($vec_data['reference']) ) ? $vec_data['reference'] : $this->reference,
                            // 'total_amount'           => $vec_data['debitAmount'],
                            'commission_amount'         => isset($vec_data['commissionAmount']) ? $vec_data['commissionAmount'] : $this->raw_attributes['commission_amount'],
                            'status_type'               => $vec_data['status'],
                            'method_type'               => $vec_data['method'],
                            // 'transaction_date'      => isset($vec_data['date']) ? $vec_data['date'] : time(),
                            'execution_date'            => ( isset($vec_data['executionDate']) && !empty($vec_data['executionDate']) ) ? $vec_data['executionDate'] : $this->raw_attributes['execution_date'],
                            'comment'                   => ( isset($vec_data['comment']) && !empty($vec_data['comment']) ) ? $vec_data['comment'] : $this->comment,
                            'original_transaction_id'   => isset($vec_data['originId']) ? $vec_data['originId'] : $this->original_transaction_id,
                            'psp_message'               => ( isset($vec_data['PSP']) && isset($vec_data['PSP']['message']) ) ? $vec_data['PSP']['message'] : $this->psp_message,
                            'response_json'             => Json::encode($vec_data),
                            'last_sync_date'            => time(),
                            'last_sync_endpoint'        => 'GET___'. Yii::app()->lemonwayApi->last_endpoint
                        ]);

                        // Lemonway Commission?
                        if ( isset($vec_data['lemonWayCommission']) && isset($vec_data['lemonWayCommission']['idp2p']) && isset($vec_data['lemonWayCommission']['amount']) && !empty($vec_data['lemonWayCommission']['idp2p']) )
                        {
                            $this->commission_transaction_id = $vec_data['lemonWayCommission']['idp2p'];
                            $this->commission_amount = $vec_data['lemonWayCommission']['amount'];
                        }

                        if ( ! $this->save() )
                        {
                            Log::save_model_error($this);
                        }

                        return $this;
                    }
                break;

                // Transaction P2P
                case 'p2p':
                    // Validate SENDER LemonwayAccount model
                    $is_valid = true;
                    if ( isset($vec_data['senderAccountId']) )
                    {
                        $sender_account_model = LemonwayAccount::model()->findByAccountId($vec_data['senderAccountId']);
                        if ( ! $sender_account_model )
                        {
                            $is_valid = false;
                            Log::lemonway_error("LemonwayComponent::get_transaction_p2p({$transaction_id}) - SENDER LEMONWAY ACCOUNT does not exist: {$vec_data['senderAccountId']}");
                        }
                        else if ( $this->sender_lemonway_account_id !== $sender_account_model->lemonway_account_id )
                        {
                            $is_valid = false;
                            Log::lemonway_warning("LemonwayComponent::get_transaction_p2p({$transaction_id}) - Sender account inconsistency. Sender saved on Crowdeen database = {$this->sender_lemonway_account_id} / Sender on Lemonway API = {$sender_account_model->lemonway_account_id}");
                        }
                    }
                    else
                    {
                        $is_valid = false;
                        Log::lemonway_dev("LemonwayComponent::get_transaction_p2p({$transaction_id}) - Incorrect response: ". print_r($vec_data, true));
                    }


                    // Validate RECEIVER LemonwayAccount model
                    if ( isset($vec_data['receiverAccountId']) )
                    {
                        $receiver_account_model = LemonwayAccount::model()->findByAccountId($vec_data['receiverAccountId']);
                        if ( ! $receiver_account_model )
                        {
                            $is_valid = false;
                            Log::lemonway_error("LemonwayComponent::get_transaction_p2p({$transaction_id}) - RECEIVER LEMONWAY ACCOUNT does not exist: {$vec_data['receiverAccountId']}");
                        }
                        else if ( $this->receiver_lemonway_account_id !== $receiver_account_model->lemonway_account_id )
                        {
                            $is_valid = false;
                            Log::lemonway_warning("LemonwayComponent::get_transaction_p2p({$transaction_id}) - Receiver account inconsistency. Receiver saved on Crowdeen database = {$this->receiver_lemonway_account_id} / Receiver on Lemonway API = {$receiver_account_model->lemonway_account_id}");
                        }
                    }
                    else
                    {
                        $is_valid = false;
                        Log::lemonway_dev("LemonwayComponent::get_transaction_p2p({$transaction_id}) - Incorrect response: ". print_r($vec_data, true));
                    }

                    // Check if transaction type and "method" are the same. Methods values:
                    //   - 0  => Card,
                    //   - 1  => Bank transfer (Money-In),
                    //   - 3  => Bank transfer (Money-Out),
                    //   - 4  => P2P,
                    /*
                    if ( !isset($vec_data['method']) || $vec_data['method'] != 4 )
                    {
                        $is_valid = false;
                        Log::lemonway_warning("LemonwayComponent::get_transaction_p2p({$transaction_id}) - Transaction type inconsistency. Type saved on Crowdeen database = 'P2P' / Type received on Lemonway API = {$vec_data['method']} - {$this->method_type_label($vec_data['method'])}");
                    }
                    */

                    // Strange situation - "method" should be 4
                    if ( !isset($vec_data['method']) || $vec_data['method'] != 4 )
                    {
                        $vec_data['method'] = 4;
                    }

                    // Transaction has been validated, save the model
                    if ( $is_valid )
                    {
                        $this->setAttributes([
                            'reference'                 => ( isset($vec_data['reference']) && !empty($vec_data['reference']) ) ? $vec_data['reference'] : $this->reference,
                            // 'total_amount'           => $vec_data['creditAmount'],
                            'commission_amount'         => isset($vec_data['commissionAmount']) ? $vec_data['commissionAmount'] : $this->raw_attributes['commission_amount'],
                            'method_type'               => $vec_data['method'],
                            'status_type'               => $vec_data['status'],
                            // 'transaction_date'      => isset($vec_data['date']) ? $vec_data['date'] : time(),
                            'execution_date'            => ( isset($vec_data['executionDate']) && !empty($vec_data['executionDate']) ) ? $vec_data['executionDate'] : $this->raw_attributes['execution_date'],
                            'comment'                   => ( isset($vec_data['comment']) && !empty($vec_data['comment']) ) ? $vec_data['comment'] : $this->comment,
                            'commission_transaction_id' => isset($vec_data['feeReference']) ? $vec_data['feeReference'] : $this->commission_transaction_id,
                            'response_json'             => Json::encode($vec_data),
                            'last_sync_date'            => time(),
                            'last_sync_endpoint'        => 'GET___'. Yii::app()->lemonwayApi->last_endpoint
                        ]);

                        // Lemonway Commission?
                        if ( isset($vec_data['lemonWayCommission']) && isset($vec_data['lemonWayCommission']['idp2p']) && isset($vec_data['lemonWayCommission']['amount']) && !empty($vec_data['lemonWayCommission']['idp2p']) )
                        {
                            $this->commission_transaction_id = $vec_data['lemonWayCommission']['idp2p'];
                            $this->commission_amount = $vec_data['lemonWayCommission']['amount'];
                        }

                        if ( ! $this->save() )
                        {
                            Log::save_model_error($this);
                        }

                        return $this;
                    }
                break;
            }
        }

        return null;
    }



    /**
     * Find a LemonwayTransaction model by "lemonway_id" attribute
     */
    public function findByLemonwayId($lemonway_id, $transaction_type = null)
    {
        $vec_attributes = ['lemonway_id' => $lemonway_id];
        if ( $transaction_type !== null )
        {
            $vec_attributes['transaction_type'] = $transaction_type;
        }
        return self::get()->where($vec_attributes)->one();
    }


    /**
     * Trasnsaction (operation) URL for the Lemonway Dashboard v2
     */
    public function url()
    {
        switch ( $this->transaction_type )
        {
            case 'in':
            case 'out';
                return Yii::app()->lemonway->get_dashboard_url() .'/operations/'. $this->lemonway_id;
            break;

            default:
                if ( $this->receiverAccount )
                {
                    return Yii::app()->lemonway->get_dashboard_url() . '/accounts/'. $this->receiverAccount->lemonway_internal_id .'/operations';
                }
            break;
        }

        return Yii::app()->lemonway->get_dashboard_url() .'/operations/all';
    }
}