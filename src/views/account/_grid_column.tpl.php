<?php
/*
|--------------------------------------------------------------------------
| CGridView column partial page for LemonwayAccount model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: LemonwayAccount model
|  - $column: Column name
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;
?>
<?php switch ( $column ) :
    /*
    |--------------------------------------------------------------------------
    | COLUMN "lemonway_account_id"
    |--------------------------------------------------------------------------
    */
    case 'lemonway_account_id':
  ?>
    <a href="<?= Url::to('/lemonway/account/view', ['user_id' => $model->user_id]); ?>"><?= $model->lemonway_account_id; ?></a>
    <br><i>#<?= $model->lemonway_internal_id; ?> (<?= Yii::t('lemonway', 'internal'); ?>)</i>
    <?php if ( $model->is_legal == 1 ) : ?><br><span class="badge badge-dark"><?= Yii::t('lemonway', 'LEGAL'); ?></span><?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "name"
    |--------------------------------------------------------------------------
    */
    case 'name':
  ?>
    <?php
      // Legal account
      if ( $model->is_legal == 1 ) :
    ?>
      <a href="<?= Url::to('/lemonway/account/view', ['user_id' => $model->user_id]); ?>"><?= $model->company_name; ?></a>
      <br><?= $model->fullname(); ?>
    <?php
      // Individual account
      else :
    ?>
      <a href="<?= Url::to('/lemonway/account/view', ['user_id' => $model->user_id]); ?>"><?= $model->fullname(); ?></a>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "transaction_type"
    |--------------------------------------------------------------------------
    */
    case 'transaction_type':
  ?>
    <?php
      $this->renderPartial('//lemonway/transaction/_view_transaction_type', [
        'transaction_type'  => $model->transaction_type
      ]);
    ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "email"
    |--------------------------------------------------------------------------
    */
    case 'email':
  ?>
    <a href="<?= Url::to('/user/admin/update', ['id' => $model->user_id]); ?>" target="_blank"><?= $model->email; ?></a>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "status_type"
    |--------------------------------------------------------------------------
    */
    case 'status_type':
  ?>
    <?php
      $this->renderPartial('//lemonway/account/_view_status', [
        'status_type'  => $model->status_type
      ]);
    ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "balance_amount"
    |--------------------------------------------------------------------------
    */
    case 'balance_amount':
  ?>
    <div class="text-right"><?= $model->get_balance_amount(); ?> &euro;</div>
  <?php break; ?>
<?php endswitch; ?>