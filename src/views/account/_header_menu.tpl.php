<?php
/*
|--------------------------------------------------------------------------
| Header zone for LemonwayAccount page
|--------------------------------------------------------------------------
|
| Available variables:
|  - $lemonway_account_model: LemonwayAccount model
|
*/
  use dz\helpers\StringHelper;
  use dz\helpers\Url;

  $current_controller = StringHelper::strtolower($this->currentControllerName());
  // $current_action = StringHelper::strtolower($this->currentActionName());
?>
<ul class="nav-quick-header nav-quick nav-quick-bordered row">
  <li class="nav-item col-md-3<?php if ( $current_controller === 'account' ) : ?> active<?php endif; ?>">
    <a class="nav-link" href="<?= Url::to('/lemonway/account/view', ['user_id' => $lemonway_account_model->user_id]); ?>">
      <i class="icon wb-user" aria-hidden="true"></i>
      <span><?= Yii::t('lemonway', 'Account'); ?></span>
    </a>
  </li>
  <li class="nav-item col-md-3<?php if ( $current_controller === 'document' ) : ?> active<?php endif; ?>">
    <a class="nav-link" href="<?= Url::to('/lemonway/document', ['user_id' => $lemonway_account_model->user_id]); ?>">
      <i class="icon wb-file" aria-hidden="true"></i>
      <span><?= Yii::t('lemonway', 'Documents'); ?></span>
    </a>
  </li>
  <li class="nav-item col-md-3<?php if ( $current_controller === 'iban' ) : ?> active<?php endif; ?>">
    <a class="nav-link" href="<?= Url::to('/lemonway/iban', ['user_id' => $lemonway_account_model->user_id]); ?>">
      <i class="icon wb-briefcase" aria-hidden="true"></i>
      <span><?= Yii::t('lemonway', 'Bank Accounts'); ?></span>
    </a>
  </li>
  <li class="nav-item col-md-3<?php if ( $current_controller === 'transaction' ) : ?> active<?php endif; ?>">
    <a class="nav-link" href="<?= Url::to('/lemonway/transaction/view', ['user_id' => $lemonway_account_model->user_id]); ?>">
      <i class="icon wb-payment" aria-hidden="true"></i>
      <span><?= Yii::t('lemonway', 'Transactions'); ?></span>
    </a>
  </li>
</ul>