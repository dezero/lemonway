<?php
/*
|--------------------------------------------------------------------------
| Header title zone for a LemonwayAccount model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $lemonway_account_model: LemonwayAccount model
|
*/
  use dz\helpers\Html;
  use dz\helpers\StringHelper;
  use dz\helpers\Url;

  // Page title
  $this->pageTitle = $lemonway_account_model->title();

  $current_controller = StringHelper::strtolower($this->currentControllerName());
  $current_action = StringHelper::strtolower($this->currentActionName());
?>
<div class="page-header">
  <h1 class="page-title">
    <?= $this->pageTitle; ?>
    <?php if ( $lemonway_account_model->is_legal == 1 ) : ?>
      <div class="badge badge-dark mr-5" data-toggle="tooltip" data-placement="bottom" data-original-title="<?= Yii::t('lemonway', 'Legal account'); ?>"><?= Yii::t('lemonway', 'Legal'); ?></div>
    <?php endif; ?>
  </h1>
  <div class="page-header-actions">
    <?=
      Html::link('<i class="wb-chevron-left"></i> '. Yii::t('app', 'Back'), ['/lemonway/account'], [
        'class' => 'btn btn-dark'
      ]);
    ?>
    <?php
      // Sync LemonwayAccount model
      if ( $current_controller === 'account' ) :
    ?>
      <a href="<?= Url::to('/lemonway/account/sync', ['user_id' => $lemonway_account_model->user_id]); ?>" class="btn btn-primary"<?php if ( Yii::app()->user->id == 1 ) :?> target="_blank"<?php endif; ?>>
        <i class="icon wb-loop"></i> Lemonway Sync
      </a>
    <?php
      // Sync LemonwayDocument model
      elseif ( $current_controller === 'document' ) :
    ?>
      <a href="<?= Url::to('/lemonway/document/sync', ['user_id' => $lemonway_account_model->user_id]); ?>" class="btn btn-primary"<?php if ( Yii::app()->user->id == 1 ) :?> target="_blank"<?php endif; ?>>
        <i class="icon wb-loop"></i> Lemonway Sync
      </a>
    <?php
      // Sync LemonwayIban model
      elseif ( $current_controller === 'iban' ) :
    ?>
      <a href="<?= Url::to('/lemonway/iban/sync', ['user_id' => $lemonway_account_model->user_id]); ?>" class="btn btn-primary"<?php if ( Yii::app()->user->id == 1 ) :?> target="_blank"<?php endif; ?>>
        <i class="icon wb-loop"></i> Lemonway Sync
      </a>
    <?php
      // Sync LemonwayTransaction model
      elseif ( $current_controller === 'transaction' ) :
    ?>
      <a href="<?= Url::to('/lemonway/transaction/sync', ['user_id' => $lemonway_account_model->user_id]); ?>" class="btn btn-primary"<?php if ( Yii::app()->user->id == 1 ) :?> target="_blank"<?php endif; ?>>
        <i class="icon wb-loop"></i> Lemonway Sync
      </a>
    <?php endif; ?>
  </div>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => Yii::t('lemonway', 'Lemonway accounts'),
        'url' => ['/lemonway/account'],
      ],
      $this->pageTitle
    ]);
  ?>
</div>