<?php
/*
|--------------------------------------------------------------------------
| Sidebar - LemonwayAccount model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $lemonway_account_model: LemonwayAccount model
|  - $vec_totals: Array with total accounts grouped by status
|
*/

  use dz\helpers\Url;

  // Special STATUS filters
  $vec_status_labels = Yii::app()->lemonwayManager->account_status_filter_labels();
  $vec_status_colors = Yii::app()->lemonwayManager->account_status_filter_colors();
?>
<div class="page-aside">
  <div class="page-aside-switch">
    <i class="icon wb-chevron-left" aria-hidden="true"></i>
    <i class="icon wb-chevron-right" aria-hidden="true"></i>
  </div>
  <div class="page-aside-inner page-aside-scroll">
    <div data-role="container">
      <div data-role="content">
        <div class="page-aside-section">
          <div class="list-group mb-10">
            <a class="list-group-item justify-content-between<?php if ( empty($lemonway_account_model->status_filter) && empty($lemonway_account_model->type_filter) ) : ?> active<?php endif; ?>" href="<?= Url::to('/lemonway/account'); ?>">
              <span><i class="icon wb-dashboard" aria-hidden="true"></i> <?= Yii::t('lemonway', 'All accounts'); ?></span>
              <span class="item-right"><?= $vec_totals['total']; ?></span>
            </a>
          </div>
        </div>
        <section class="page-aside-section">
          <div class="list-group mb-10">
            <a class="list-group-item<?php if ( $lemonway_account_model->type_filter === 'individual' ) : ?> active<?php endif; ?> ( " href="<?= Url::to('/lemonway/account', ['LemonwayAccount[type_filter]' => 'individual']); ?>">
              <span><?= Yii::t('lemonway', 'Individual'); ?></span>
              <span class="item-right"><?= $vec_totals['individual']; ?></span>
            </a>
            <a class="list-group-item<?php if ( $lemonway_account_model->type_filter === 'legal' ) : ?> active<?php endif; ?> ( " href="<?= Url::to('/lemonway/account', ['LemonwayAccount[type_filter]' => 'legal']); ?>">
              <span><?= Yii::t('lemonway', 'Legal'); ?></span>
              <span class="item-right"><?= $vec_totals['legal']; ?></span>
            </a>
          </div>
        </section>
        <section class="page-aside-section">
          <div class="list-group mb-20">
            <?php foreach ( $vec_status_labels as $status_filter => $status_label ) : ?>
              <a class="list-group-item<?php if ( $lemonway_account_model->status_filter === $status_filter ) : ?> active<?php endif; ?> ( " href="<?= Url::to('/lemonway/account', ['LemonwayAccount[status_filter]' => $status_filter]); ?>">
                <span><i class="wb-medium-point <?= $vec_status_colors[$status_filter]; ?>" aria-hidden="true"></i><?= $vec_status_labels[$status_filter]; ?></span>
                <span class="item-right"><?= $vec_totals[$status_filter]; ?></span>
              </a>
            <?php endforeach; ?>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>