<?php
/*
|--------------------------------------------------------------------------
| Account Personal Information (partial)
|--------------------------------------------------------------------------
|
| Available variables:
|  - $lemonway_account_model: LemonwayAccount model
|
*/
  use dz\helpers\Url;
?>
  <div class="panel">
    <header class="panel-heading">
      <h3 class="panel-title"><?= Yii::t('lemonway', 'Personal Data'); ?></h3>
    </header>
    <div class="panel-body panel-view-content">
      <div class="row">
        <div class="col-sm-3">
          <h5><?= $lemonway_account_model->getAttributeLabel('firstname'); ?></h5>
          <div class="item-content"><?= $lemonway_account_model->firstname; ?></div>
        </div>
        
        <div class="col-sm-3">
          <h5><?= $lemonway_account_model->getAttributeLabel('lastname'); ?></h5>
          <div class="item-content"><?= $lemonway_account_model->lastname; ?></div>
        </div>

        <div class="col-sm-3">
          <h5><?= $lemonway_account_model->getAttributeLabel('phone_number'); ?></h5>
          <div class="item-content"><?= !empty($lemonway_account_model->phone_number) ? $lemonway_account_model->phone_number : '-'; ?></div>
        </div>

        <div class="col-sm-3">
          <h5><?= $lemonway_account_model->getAttributeLabel('mobile_number'); ?></h5>
          <div class="item-content"><?= !empty($lemonway_account_model->mobile_number) ? $lemonway_account_model->mobile_number : '-'; ?></div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-3">
          <h5><?= $lemonway_account_model->getAttributeLabel('birth_date'); ?></h5>
          <div class="item-content"><?= $lemonway_account_model->birth_date; ?></div>
        </div>

        <div class="col-sm-3">
          <h5><?= $lemonway_account_model->getAttributeLabel('birth_city'); ?></h5>
          <div class="item-content"><?= !empty($lemonway_account_model->birth_city) ? $lemonway_account_model->birth_city : '-'; ?></div>
        </div>

        <div class="col-sm-3">
          <h5><?= $lemonway_account_model->getAttributeLabel('birth_country_code'); ?></h5>
          <div class="item-content"><?= !empty($lemonway_account_model->birth_country_code) ? $lemonway_account_model->birth_country_code : '-'; ?></div>
        </div>

        <div class="col-sm-3">
          <h5><?= $lemonway_account_model->getAttributeLabel('nationality_country_code'); ?></h5>
          <div class="item-content"><?= !empty($lemonway_account_model->nationality_country_code) ? $lemonway_account_model->nationality_country_code : '-'; ?></div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-3">
          <h5><?= $lemonway_account_model->getAttributeLabel('address_street'); ?></h5>
          <div class="item-content"><?= $lemonway_account_model->address_street; ?></div>
        </div>

        <div class="col-sm-3">
          <h5><?= $lemonway_account_model->getAttributeLabel('address_city'); ?></h5>
          <div class="item-content"><?= $lemonway_account_model->address_city; ?></div>
        </div>

        <div class="col-sm-3">
          <h5><?= $lemonway_account_model->getAttributeLabel('address_post_code'); ?></h5>
          <div class="item-content"><?= $lemonway_account_model->address_post_code; ?></div>
        </div>
      
        <div class="col-sm-3">
          <h5><?= $lemonway_account_model->getAttributeLabel('address_country_code'); ?></h5>
          <div class="item-content"><?= $lemonway_account_model->address_country_code; ?></div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-3">
          <h5><?= $lemonway_account_model->getAttributeLabel('company_name'); ?></h5>
          <div class="item-content"><?= !empty($lemonway_account_model->company_name) ? $lemonway_account_model->company_name : '-'; ?></div>
        </div>

        <div class="col-sm-3">
          <h5><?= $lemonway_account_model->getAttributeLabel('company_description'); ?></h5>
          <div class="item-content"><?= !empty($lemonway_account_model->company_description) ? $lemonway_account_model->company_description : '-'; ?></div>
        </div>

        <div class="col-sm-3">
          <h5><?= $lemonway_account_model->getAttributeLabel('company_website'); ?></h5>
          <div class="item-content"><?= !empty($lemonway_account_model->company_website) ? $lemonway_account_model->company_website : '-'; ?></div>
        </div>

        <div class="col-sm-3">
          <h5><?= $lemonway_account_model->getAttributeLabel('company_vat_code'); ?></h5>
          <div class="item-content"><?= !empty($lemonway_account_model->company_vat_code) ? $lemonway_account_model->company_vat_code : '-'; ?></div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <a href="<?= $lemonway_account_model->url('account-information'); ?>" target="_blank" class="btn btn-dark btn-sm"><?= Yii::t('lemonway', 'View Lemonway Dashboard'); ?> <i class="icon fa-external-link"></i></a>
        </div>
      </div>
    </div>
  </div>