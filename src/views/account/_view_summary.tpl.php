<?php
/*
|--------------------------------------------------------------------------
| Lemonway Account Summary Information (partial)
|--------------------------------------------------------------------------
|
| Available variables:
|  - $lemonway_account_model: LemonwayAccount model
|
*/
  use dz\helpers\StringHelper;
  use dz\helpers\Url;

  $current_controller = StringHelper::strtolower($this->currentControllerName());
  // $current_action = StringHelper::strtolower($this->currentActionName());
?>
<div id="lemonway-wallet-panel" class="panel">
  <header class="panel-heading">
    <?php if ( $current_controller === 'account' ) : ?>
      <h3 class="panel-title"><?= Yii::t('lemonway', 'Lemonway Data'); ?><span class="ml-5 font-size-14 grey-600 font-italic">(<?= Yii::t('lemonway', 'last sync'); ?>: <?= $lemonway_account_model->last_sync_date; ?>)</h3>
    <?php else : ?>
      <h3 class="panel-title"><?= Yii::t('lemonway', 'Lemonway Account'); ?></h3>
    <?php endif; ?>
  </header>
  <div class="panel-body panel-view-content">
    <div class="row">
      <div class="col-sm-3">
        <h5><?= Yii::t('lemonway', 'Account'); ?></h5>
        <div class="item-content">
          <?php
            // Legal account
            if ( $lemonway_account_model->is_legal == 1 ) :
          ?>
            <a href="<?= $lemonway_account_model->url(); ?>" target="_blank"><?= $lemonway_account_model->lemonway_account_id .' - '. $lemonway_account_model->company_name; ?></a>
            <br><?= $lemonway_account_model->fullname(); ?>
          <?php
            // Individual account
            else :
          ?>
            <a href="<?= $lemonway_account_model->url(); ?>" target="_blank"><?= $lemonway_account_model->lemonway_account_id .' - '. $lemonway_account_model->fullname(); ?></a>
          <?php endif; ?>
          <br><i>#<?= $lemonway_account_model->lemonway_internal_id; ?> (<?= Yii::t('lemonway', 'internal'); ?>)</i>
        </div>
      </div>

      <div class="col-sm-3">
        <h5><?= Yii::t('lemonway', 'Balance'); ?></h5>
        <div class="item-content">
          <span class="font-size-32"><?= $lemonway_account_model->get_balance_amount(); ?> &euro;</span>
        </div>
      </div>

      <div class="col-sm-3">
        <h5><?= $lemonway_account_model->getAttributeLabel('status_type'); ?></h5>
        <div class="item-content status-type-content">
          <?php
            $this->renderPartial('//lemonway/account/_view_status', [
              'vec_status_types'  => $lemonway_account_model->status_type_labels(),
              'status_type'       => $lemonway_account_model->status_type
            ]);
          ?>
        </div>
      </div>

      <div class="col-sm-3">
        <h5><?= Yii::t('lemonway', 'Lemownay Dashboard'); ?></h5>
        <div class="item-content">
          <ul>
            <li><a href="<?= $lemonway_account_model->url('operations'); ?>" target="_blank"><?= Yii::t('lemonway', 'View Operations'); ?></a></li>
            <li><a href="<?= $lemonway_account_model->url('documents'); ?>" target="_blank"><?= Yii::t('lemonway', 'View Documents'); ?></a></li>
            <li><a href="<?= $lemonway_account_model->url('bank-accounts'); ?>" target="_blank"><?= Yii::t('lemonway', 'View Bank Accounts'); ?></a></li>
          </ul>
        </div>
      </div>
    </div>

    <?php
      // Additional row
      if ( $current_controller === 'account' ) :
    ?>
      <div class="row">
        <div class="col-sm-3">
          <h5><?= Yii::t('lemonway', 'Email'); ?></h5>
          <div class="item-content"><a href="<?= Url::to('/user/admin/update', ['id' => $lemonway_account_model->user_id]); ?>" target="_blank"><?= $lemonway_account_model->email; ?></a></div>
        </div>

        <div class="col-sm-3">
          <h5><?= $lemonway_account_model->getAttributeLabel('last_sync_date'); ?></h5>
          <div class="item-content">
            <?= $lemonway_account_model->last_sync_date; ?>
            <br><?= $lemonway_account_model->last_sync_endpoint; ?>
          </div>
        </div>

        <div class="col-sm-3">
          <h5><?= $lemonway_account_model->getAttributeLabel('created_date'); ?></h5>
          <div class="item-content"><?= $lemonway_account_model->created_date; ?></div>
        </div>
      </div>
    <?php endif; ?>
  </div>
</div>