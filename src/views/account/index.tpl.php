<?php
/*
|--------------------------------------------------------------------------
| Admin list page for LemonwayAccount models
|--------------------------------------------------------------------------
|
| Available variables:
|  - $lemonway_account_model: LemonwayAccount model class
|  - $vec_totals: Array with totals accounts grouped by status
|
*/  
  use dz\helpers\Url;
  
  // Page title
  $page_title = Yii::t('lemonway', 'Lemonway Accounts');

  if ( $lemonway_account_model->type_filter === 'legal' )
  {
    $page_title = Yii::t('lemonway', 'Legal Accounts');
  }
  else if ( $lemonway_account_model->type_filter === 'individual' )
  {
    $page_title = Yii::t('lemonway', 'Individual Accounts'); 
  }
  else if ( !empty($lemonway_account_model->status_filter) )
  {
    $page_title = Yii::t('lemonway', 'Accounts');
    switch ( $lemonway_account_model->status_filter )
    {
      case 'kyc_confirmed':
        $page_title .= ' - '. Yii::t('lemonway', 'KYC Confirmed');
      break;

      case 'kyc_pending':
        $page_title .= ' - '. Yii::t('lemonway', 'KYC Pending');
      break;

      case 'blocked_error':
        $page_title .= '  - '. Yii::t('lemonway', 'Rejected / Blocked');
      break;

      case 'closed':
        $page_title .= '  - '. Yii::t('lemonway', 'Closed');
      break;

      case 'technical':
        $page_title .= '  - '. Yii::t('lemonway', 'Technical');
      break;
    }
  }

  $this->pageTitle = $page_title;
?>    
<?php
  // Sidebar menu with totals
  $this->renderPartial('//lemonway/account/_sidebar', [
    'lemonway_account_model'  => $lemonway_account_model,
    'vec_totals'              => $vec_totals
  ]);
?>
<div class="page-main">
  <div class="page-header">
    <h1 class="page-title"><?= $this->pageTitle; ?></h1>
    <div class="page-header-actions">
      <a href="<?= Yii::app()->lemonway->get_dashboard_url(); ?>/accounts/" target="_blank" class="btn btn-dark"><i class="fa-external-link"></i> <?= Yii::t('lemonway', 'Lemonway Dashboard'); ?></a>
    </div>
  </div>
  <div class="page-content">
    <div class="row row-lg">
      <div class="col-lg-12">
        <div class="panel">
          <div class="panel-body container-fluid">
            <?php
              /*
              |----------------------------------------------------------------------------------------
              | GridView widget
              |----------------------------------------------------------------------------------------
              */
                $vec_columns = [
                  [
                    'header' => 'ID',
                    'name' => 'lemonway_account_id',
                    'value' => 'trim($this->grid->getOwner()->renderPartial("//lemonway/account/_grid_column", ["column" => "lemonway_account_id", "model" => $data]))',
                  ],
                  [
                    'header' => Yii::t('lemonway', 'Name'),
                    'name' => 'name_filter',
                    'value' => 'trim($this->grid->getOwner()->renderPartial("//lemonway/account/_grid_column", ["column" => "name", "model" => $data]))',
                  ],
                  [
                    'name' => 'email',
                    'value' => 'trim($this->grid->getOwner()->renderPartial("//lemonway/account/_grid_column", ["column" => "email", "model" => $data]))',
                  ],
                  [
                    'name' => 'status_type',
                    'filter' => Yii::app()->lemonwayManager->account_status_filter_labels(),
                    'value' => 'trim($this->grid->getOwner()->renderPartial("//lemonway/account/_grid_column", ["column" => "status_type", "model" => $data]))',
                  ],
                  [
                    'name' => 'balance_amount',
                    'filter' => false,
                    'value' => 'trim($this->grid->getOwner()->renderPartial("//lemonway/account/_grid_column", ["column" => "balance_amount", "model" => $data]))',
                  ],
                  [
                    'class' => 'dz.grid.ButtonColumn',
                    'header' => Yii::t('app', 'ACTION'),
                    'template' => '{view}{user}{lemonway}',
                    'clearButton' => true,
                    'viewButton' => true,
                    'updateButton' => false,
                    'deleteButton' => false,
                    'buttons' => [
                      'view' => [
                        'label' => Yii::t('app', 'View'),
                        'icon' => 'eye',
                        'url' => 'Yii::app()->createAbsoluteUrl("/lemonway/account/view", ["user_id" => $data->user_id])',
                      ],
                      'user' => [
                        'label' => Yii::t('app', 'User'),
                        'icon' => 'user',
                        'url' => 'Yii::app()->createAbsoluteUrl("/user/admin/update", ["id" => $data->user_id])',
                        'options' => [
                          'target' => '_blank'
                        ]
                      ],
                      'lemonway' => [
                        'label' => Yii::t('app', 'Dashboard Lemonway'),
                        'icon' => 'fa-external-link',
                        'url' => '$data->url()',
                        'options' => [
                          'target' => '_blank'
                        ]
                      ]
                    ],
                  ]
                ];

                $this->widget('dz.grid.GridView', [
                  'id'                => 'lemonway-account-grid',
                  'dataProvider'      => $lemonway_account_model->search(),
                  'filter'            => $lemonway_account_model,
                  'emptyText'         => Yii::t('lemonway', 'No accounts have been found'),
                  'enableHistory'     => false,
                  'loadModal'         => true,
                  'enableSorting'     => false,
                  'enableSelect2'     => false,
                  'type'              => ['striped', 'hover'],
                  'beforeAjaxUpdate'  => 'js:function(id, options){ $("html, body").animate({scrollTop: 0}, 100); }',
                  'afterAjaxUpdate'   => 'js:function() { $.lemonwayAccountGridUpdate(); }',
                  'columns'           => $vec_columns
                ]);
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div><!-- .page-content -->
</div><!-- .page-main -->