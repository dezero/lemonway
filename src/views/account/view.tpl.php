<?php
/*
|--------------------------------------------------------------------------
| LemonwayAccount VIEW page
|--------------------------------------------------------------------------
|
| Available variables:
|  - $lemonway_account_model: LemonwayAccount model
|
*/
  use dz\helpers\Url;

  // Page title
  $this->pageTitle = $lemonway_account_model->title();
?>
<?php
  // Header title
  $this->renderPartial('//lemonway/account/_header_title', [
    'lemonway_account_model'  => $lemonway_account_model
  ]);
?>
<div class="page-content container-fluid">
  <?php
    // Header menu
    $this->renderPartial('//lemonway/account/_header_menu', [
      'lemonway_account_model' => $lemonway_account_model
    ]);
  ?>
  
  <?php
    /*
    |--------------------------------------------------------------------------
    | ACCOUNT INFORMATION
    |--------------------------------------------------------------------------
    */
  ?>
  <?php
    // Account summary
    $this->renderPartial('//lemonway/account/_view_summary', [
      'lemonway_account_model'   => $lemonway_account_model
    ]);

    // Personal information (address)
    $this->renderPartial('//lemonway/account/_view_address', [
      'lemonway_account_model'   => $lemonway_account_model
    ]);
  ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | LAST LOG API
    |--------------------------------------------------------------------------
    */
    if ( !empty($log_api_model) ) :
  ?>
    <?php
      $this->renderPartial('//lemonway/account/_view_lemonway_api', [
        'lemonway_account_model'  => $lemonway_account_model
      ]);
    ?>
  <?php endif; ?>
</div>