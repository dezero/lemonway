<?php
/*
|--------------------------------------------------------------------------
| CGridView column partial page for LemonwayDocument model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: LemonwayDocument model
|  - $column: Column name
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;
?>
<?php switch ( $column ) :
    /*
    |--------------------------------------------------------------------------
    | COLUMN "file_id"
    |--------------------------------------------------------------------------
    */
    case 'file_id':
  ?>
    <?php if ( $model->file ) : ?>
      <a href="<?= $model->file->download_url(); ?>" target="_blank"><?= $model->file->title(); ?></a>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "document_type"
    |--------------------------------------------------------------------------
    */
    case 'document_type':
  ?>
    <?= $model->document_type_label(); ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "status_type"
    |--------------------------------------------------------------------------
    */
    case 'status_type':
  ?>
    <?php
      $this->renderPartial('//lemownay/document/_view_status', [
        'status_type'  => $model->status_type
      ]);
    ?>
    <?php if ( !empty($model->substatus_type) ) : ?>
      <p><?= $model->substatus_type_label(); ?></p>
    <?php endif; ?>
    <?php if (!empty($model->comment) ) : ?>
      <p><?= $model->comment; ?></p>
    <?php endif; ?>
  <?php break; ?>
<?php endswitch; ?>