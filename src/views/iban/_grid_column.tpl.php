<?php
/*
|--------------------------------------------------------------------------
| CGridView column partial page for LemonwayIban model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: LemonwayIban model
|  - $column: Column name
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;
?>
<?php switch ( $column ) :
    /*
    |--------------------------------------------------------------------------
    | COLUMN "holder"
    |--------------------------------------------------------------------------
    */
    case 'holder':
  ?>
    <?= $model->holder; ?>
    <?php if ( $model->is_virtual == 1 ) : ?>
      <br><span class="badge badge-success">VIRTUAL</span>
      <?php if ( $model->pdfFile ) : ?>
        <a href="<?= $model->pdfFile->download_url(); ?>" target="_blank" class="ml-10"><?= Yii::t('app', 'Download PDF'); ?></a>
      <?php endif; ?>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "iban"
    |--------------------------------------------------------------------------
    */
    case 'iban':
  ?>
    <?= $model->iban; ?>
    <?php if ( !empty($model->bic) ) : ?>
      <br>BIC / SWIFT: <?= $model->bic; ?>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "status_type"
    |--------------------------------------------------------------------------
    */
    case 'status_type':
  ?>
    <?php
      $this->renderPartial('//lemownay/iban/_view_status', [
        'status_type'  => $model->status_type
      ]);
    ?>
  <?php break; ?>
<?php endswitch; ?>