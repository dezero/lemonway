<?php
/*
|--------------------------------------------------------------------------
| Sidebar - LemonwayIban model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $lemonway_iban_model: LemonwayIban model
|  - $vec_totals: Array with total documents grouped by status
|
*/

  use dz\helpers\Url;

  // Special DOCUMENT TYPE filters
  $vec_document_labels = Yii::app()->lemonwayManager->document_type_filter_labels();

  // Special STATUS filters
  $vec_status_labels = Yii::app()->lemonwayManager->document_status_filter_labels();
  $vec_status_colors = Yii::app()->lemonwayManager->document_status_filter_colors();
?>
<div class="page-aside">
  <div class="page-aside-switch">
    <i class="icon wb-chevron-left" aria-hidden="true"></i>
    <i class="icon wb-chevron-right" aria-hidden="true"></i>
  </div>
  <div class="page-aside-inner page-aside-scroll">
    <div data-role="container">
      <div data-role="content">
        <div class="page-aside-section">
          <div class="list-group mb-10">
            <a class="list-group-item justify-content-between<?php if ( empty($lemonway_iban_model->status_filter) && empty($lemonway_iban_model->type_filter) ) : ?> active<?php endif; ?>" href="<?= Url::to('/lemonway/iban'); ?>">
              <span><i class="icon wb-dashboard" aria-hidden="true"></i> <?= Yii::t('lemonway', 'All documents'); ?></span>
              <span class="item-right"><?= $vec_totals['total']; ?></span>
            </a>
          </div>
        </div>
        <section class="page-aside-section">
          <div class="list-group mb-10">
            <?php foreach ( $vec_document_labels as $document_type_filter => $document_type_label ) : ?>
              <a class="list-group-item<?php if ( $lemonway_iban_model->document_type_filter === $document_type_filter ) : ?> active<?php endif; ?> ( " href="<?= Url::to('/lemonway/iban', ['LemonwayIban[document_type_filter]' => $status_filter]); ?>">
                <span><?= $document_type_label; ?></span>
                <span class="item-right"><?= $vec_totals[$document_type_filter]; ?></span>
              </a>
            <?php endforeach; ?>
          </div>
        </section>
        <section class="page-aside-section">
          <div class="list-group mb-20">
            <?php foreach ( $vec_status_labels as $status_filter => $status_label ) : ?>
              <a class="list-group-item<?php if ( $lemonway_iban_model->status_filter === $status_filter ) : ?> active<?php endif; ?> ( " href="<?= Url::to('/lemonway/iban', ['LemonwayIban[status_filter]' => $status_filter]); ?>">
                <span><i class="wb-medium-point <?= $vec_status_colors[$status_filter]; ?>" aria-hidden="true"></i><?= $vec_status_labels[$status_filter]; ?></span>
                <span class="item-right"><?= $vec_totals[$status_filter]; ?></span>
              </a>
            <?php endforeach; ?>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>