<?php
/*
|--------------------------------------------------------------------------
| LemonwayIban list page for a LemonwayAccount model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $user_model: User model
|  - $lemonway_account_model: LemonwayAccount model
|  - $lemonway_iban_model: LemonwayIban model
|
*/
  use dz\helpers\Url;

  // Page title
  $this->pageTitle = $lemonway_account_model->title();
?>
<?php
  // Header title
  $this->renderPartial('//lemonway/account/_header_title', [
    'lemonway_account_model'  => $lemonway_account_model
  ]);
?>
<div class="page-content container-fluid">
  <?php
    // Header menu
    $this->renderPartial('//lemonway/account/_header_menu', [
      'lemonway_account_model' => $lemonway_account_model
    ]);
  ?>
  <div class="row row-lg">
    <div class="col-lg-12">
      <div class="panel">
        <div class="panel-body container-fluid">
          <?php
            /*
            |----------------------------------------------------------------------------------------
            | GridView widget
            |----------------------------------------------------------------------------------------
            */
              $vec_columns = [
                [
                  'header' => '#',
                  'name' => 'lemonway_iban_id',
                ],
                [
                  'name' => 'holder',
                  'value' => 'trim($this->grid->getOwner()->renderPartial("//lemonway/iban/_grid_column", ["column" => "holder", "model" => $data]))',
                ],
                [
                  'name' => 'iban',
                  'value' => 'trim($this->grid->getOwner()->renderPartial("//lemonway/iban/_grid_column", ["column" => "iban", "model" => $data]))',
                ],
                [
                  'name' => 'status_type',
                  'value' => 'trim($this->grid->getOwner()->renderPartial("//lemonway/iban/_grid_column", ["column" => "status_type", "model" => $data]))',
                ],
                'last_sync_date',
                'created_date',
                [
                  'class' => 'dz.grid.ButtonColumn',
                  'header' => Yii::t('app', 'ACTION'),
                  'template' => '{lemonway}',
                  'clearButton' => false,
                  'viewButton' => false,
                  'updateButton' => false,
                  'deleteButton' => false,
                  'buttons' => [
                    'lemonway' => [
                      'label' => Yii::t('app', 'Dashboard Lemonway'),
                      'icon' => 'fa-external-link',
                      'url' => '$data->url()',
                      'options' => [
                        'target' => '_blank'
                      ]
                    ]
                  ],
                ]
              ];

              $this->widget('dz.grid.GridView', [
                'id'                => 'lemonway-document-grid',
                'dataProvider'      => $lemonway_iban_model->search(),
                // 'filter'            => $lemonway_iban_model,
                'emptyText'         => Yii::t('lemonway', 'No bank accounts have been found'),
                'enableHistory'     => false,
                'loadModal'         => true,
                'enableSorting'     => false,
                'enableSelect2'     => false,
                'type'              => ['striped', 'hover'],
                'beforeAjaxUpdate'  => 'js:function(id, options){ $("html, body").animate({scrollTop: 0}, 100); }',
                // 'afterAjaxUpdate'   => 'js:function() { $.lemonwayIbanGridUpdate(); }',
                'columns'           => $vec_columns
              ]);
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div><!-- .page-content -->