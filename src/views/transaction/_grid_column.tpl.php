<?php
/*
|--------------------------------------------------------------------------
| CGridView column partial page for LemonwayTransaction model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: LemonwayTransaction model
|  - $column: Column name
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;
?>
<?php switch ( $column ) :
    /*
    |--------------------------------------------------------------------------
    | COLUMN "lemonway_id"
    |--------------------------------------------------------------------------
    */
    case 'lemonway_id':
  ?>
    <a href="<?= $model->url(); ?>" target="_blank"><?= $model->lemonway_id; ?></a>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "transaction_date"
    |--------------------------------------------------------------------------
    */
    case 'transaction_date':
  ?>
    <?php if ( !empty($model->transaction_date) ) : ?>
      <?= $model->transaction_date; ?>
      <?php if ( !empty($model->raw_attributes['execution_date']) && $model->execution_date !== $model->transaction_date ) : ?>
        <br><i><?= $model->execution_date; ?> (finalizado)</i>
      <?php endif; ?>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "transaction_type"
    |--------------------------------------------------------------------------
    */
    case 'transaction_type':
  ?>
    <?php
      $this->renderPartial('//lemonway/transaction/_view_transaction_type', [
        'transaction_type'  => $model->transaction_type
      ]);
    ?>
    <?php if ( $model->transaction_type !== 'p2p' ) : ?>
      <?php
        $method_label = $model->method_type_label();
        if ( !empty($method_label) ) :
      ?>
        <?= $method_label; ?> (<?= $model->method_type; ?>)
      <?php endif; ?>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "status_type"
    |--------------------------------------------------------------------------
    */
    case 'status_type':
  ?>
    <?php
      $this->renderPartial('//lemonway/transaction/_view_status_type', [
        'model'  => $model
      ]);
    ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "total_amount"
    |--------------------------------------------------------------------------
    */
    case 'total_amount':
  ?>
    <div class="text-right mr-5"><strong><?= $model->get_amount(); ?> &euro;</strong></div>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "sender_user_id"
    |--------------------------------------------------------------------------
    */
    case 'sender_user_id':
  ?>
    <?php if ( $model->senderAccount ) : ?>
      <a href="<?= Url::to('/lemonway/account/view', ['user_id' => $model->sender_user_id]); ?>" target="_blank"><?= $model->senderAccount->lemonway_account_id; ?></a>
      <br><?= $model->senderAccount->title(); ?>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "receiver_user_id"
    |--------------------------------------------------------------------------
    */
    case 'receiver_user_id':
  ?>
    <?php if ( $model->transaction_type !== 'out' && $model->receiverAccount ) : ?>
      <a href="<?= Url::to('/lemonway/account/view', ['user_id' => $model->receiver_user_id]); ?>" target="_blank"><?= $model->receiverAccount->lemonway_account_id; ?></a>
      <br><?= $model->receiverAccount->title(); ?>
    <?php endif; ?>
  <?php break; ?>
<?php endswitch; ?>