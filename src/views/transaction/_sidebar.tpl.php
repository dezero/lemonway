<?php
/*
|--------------------------------------------------------------------------
| Sidebar - LemonwayTransaction model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $lemonway_transaction_model: LemonwayTransaction model
|  - $vec_totals: Array with total transactions grouped by status
|
*/

  use dz\helpers\Url;

  // Transaction type filters
  $vec_transaction_type_labels = Yii::app()->lemonwayManager->transaction_labels();
  unset($vec_transaction_type_labels['refund']);
  $vec_transaction_type_colors = Yii::app()->lemonwayManager->transaction_colors();
?>
<div class="page-aside">
  <div class="page-aside-switch">
    <i class="icon wb-chevron-left" aria-hidden="true"></i>
    <i class="icon wb-chevron-right" aria-hidden="true"></i>
  </div>
  <div class="page-aside-inner page-aside-scroll">
    <div data-role="container">
      <div data-role="content">
        <div class="page-aside-section">
          <div class="list-group mb-10">
            <a class="list-group-item justify-content-between<?php if ( empty($lemonway_transaction_model->transaction_type) ) : ?> active<?php endif; ?>" href="<?= Url::to('/lemonway/transaction'); ?>">
              <span><i class="icon wb-dashboard" aria-hidden="true"></i> <?= Yii::t('lemonway', 'All transactions'); ?></span>
              <span class="item-right"><?= $vec_totals['total']; ?></span>
            </a>
          </div>
        </div>
        <section class="page-aside-section">
          <div class="list-group mb-20">
            <?php foreach ( $vec_transaction_type_labels as $transaction_type => $transaction_type_label ) : ?>
              <a class="list-group-item<?php if ( $lemonway_transaction_model->transaction_type === $transaction_type ) : ?> active<?php endif; ?> ( " href="<?= Url::to('/lemonway/transaction', ['LemonwayTransaction[transaction_type]' => $transaction_type]); ?>">
                <span><i class="wb-medium-point <?= $vec_transaction_type_colors[$transaction_type]; ?>" aria-hidden="true"></i><?= $vec_transaction_type_labels[$transaction_type]; ?></span>
                <span class="item-right"><?= $vec_totals[$transaction_type]; ?></span>
              </a>
            <?php endforeach; ?>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>