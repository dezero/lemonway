<?php
/*
|--------------------------------------------------------------------------
| Status type (fragment HTML)
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: LemonwayTransaction model
|
*/
  use lemonway\models\LemonwayTransaction;

  // Get status type label
  $status_label = $model->status_type_label();

  // Get colors
  $vec_status_colors = [
     0   => 'green-800',    // Success
     3   => 'red-800',      // LemonWay Error
     4   => 'blue-800',     // Pending
     6   => 'red-800',      // Error PSP
     7   => 'red-800',      // Cancelled
     14  => 'purple-800',   // Scheduled
     16  => 'orange-800',   // Validation Pending
  ];

  // P2P transaction
  if ( $model->method_type == 4 || $model->transaction_type === 'p2p' )
  {
    $vec_status_colors = [
      0   => 'blue-800',      // Pending
      3   => 'green-800',    // Success
    ];
  }
?>
<div class="status-type-wrapper lemonway-transaction-status">
  <?php if ( isset($vec_status_colors[$model->status_type]) ) : ?>
    <span class="<?= $vec_status_colors[$model->status_type]; ?>"> <?= $status_label; ?> (<?= $model->status_type; ?>)</span>
  <?php else : ?>
    <span><?= $status_label; ?> (<?= $model->status_type; ?>)</span>
  <?php endif; ?>
</div>