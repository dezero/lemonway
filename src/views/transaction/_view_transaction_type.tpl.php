<?php
/*
|--------------------------------------------------------------------------
| Transaction type (fragment HTML)
|--------------------------------------------------------------------------
|
| Available variables:
|  - $transaction_type: Current transaction type
|
*/
  // Get transaction type labels & colors
  $vec_transaction_labels = Yii::app()->lemonwayManager->transaction_labels();
  $vec_transaction_colors = Yii::app()->lemonwayManager->transaction_colors();
?>
<div class="status-type-wrapper">
  <?php if ( isset($vec_transaction_labels[$transaction_type]) ) : ?>
    <?php if ( isset($vec_transaction_colors[$transaction_type]) ) : ?>
      <span class="<?= $vec_transaction_colors[$transaction_type]; ?>"><i class="wb-medium-point <?= $vec_transaction_colors[$transaction_type]; ?>" aria-hidden="true"></i> <?= $vec_transaction_labels[$transaction_type]; ?></span>
    <?php else : ?>
      <span><i class="wb-medium-point"></i> <?= $vec_transaction_labels[$transaction_type]; ?></span>
    <?php endif; ?>
  <?php else : ?>
    <span><?= $transaction_type; ?></span>
  <?php endif; ?>
</div>