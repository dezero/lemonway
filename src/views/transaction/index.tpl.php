<?php
/*
|--------------------------------------------------------------------------
| Admin list page for LemonwayTransaction models
|--------------------------------------------------------------------------
|
| Available variables:
|  - $lemonway_transaction_model: LemonwayTransaction model class
|  - $vec_totals: Array with totals accounts grouped by status
|
*/  
  use dz\helpers\Url;
  use dzlab\lemonway\models\LemonwayTransaction;
  
  // Page title
  $page_title = Yii::t('lemonway', 'Lemonway Transactions');

  if ( !empty($lemonway_transaction_model->transaction_type) )
  {
    $page_title = Yii::t('lemonway', 'Transactions');

    switch ( $lemonway_transaction_model->transaction_type )
    {
      case 'in':
        $page_title .= ' - '. Yii::t('lemonway', 'IN');
      break;

      case 'out':
        $page_title .= ' - '. Yii::t('lemonway', 'OUT');
      break;

      case 'p2p':
        $page_title .= '  - '. Yii::t('lemonway', 'P2P');
      break;
    }
  }

  $this->pageTitle = $page_title;
?>    
<?php
  // Sidebar menu with totals
  $this->renderPartial('//lemonway/transaction/_sidebar', [
    'lemonway_transaction_model'  => $lemonway_transaction_model,
    'vec_totals'                  => $vec_totals
  ]);
?>
<div class="page-main">
  <div class="page-header">
    <h1 class="page-title"><?= $this->pageTitle; ?></h1>
    <div class="page-header-actions">
      <a href="<?= Yii::app()->lemonway->get_dashboard_url(); ?>/operations/all" target="_blank" class="btn btn-dark"><i class="fa-external-link"></i> <?= Yii::t('lemonway', 'Lemonway Dashboard'); ?></a>
    </div>
  </div>
  <div class="page-content">
    <div class="row row-lg">
      <div class="col-lg-12">
        <div class="panel">
          <div class="panel-body container-fluid">
            <?php
              /*
              |----------------------------------------------------------------------------------------
              | GridView widget
              |----------------------------------------------------------------------------------------
              */
                // Join transaction and method types
                $vec_transaction_type_labels = LemonwayTransaction::model()->transaction_type_labels();
                $vec_transaction_type_labels['-'] = '---';
                $vec_transaction_type_labels = \CMap::mergeArray($vec_transaction_type_labels, LemonwayTransaction::model()->method_type_labels());

                // Join P2P and IN/OUT transaction types
                $vec_status_type_labels = LemonwayTransaction::model()->status_type_labels();
                $vec_p2p_type_labels = LemonwayTransaction::model()->status_type_labels(true);
                $vec_status_type_labels['-'] = '---';
                $vec_status_type_labels['p2p-0'] = $vec_p2p_type_labels[0];
                $vec_status_type_labels['p2p-3'] = $vec_p2p_type_labels[3];

                $vec_columns = [
                  [
                    'header' => '#',
                    'name' => 'lemonway_id',
                    'value' => 'trim($this->grid->getOwner()->renderPartial("//lemonway/transaction/_grid_column", ["column" => "lemonway_id", "model" => $data]))',
                  ],
                  [
                    'name' => 'transaction_date',
                    'value' => 'trim($this->grid->getOwner()->renderPartial("//lemonway/transaction/_grid_column", ["column" => "transaction_date", "model" => $data]))',
                  ],
                  [
                    'name' => 'sender_user_id',
                    'value' => 'trim($this->grid->getOwner()->renderPartial("//lemonway/transaction/_grid_column", ["column" => "sender_user_id", "model" => $data]))',
                  ],
                  [
                    'name' => 'receiver_user_id',
                    'value' => 'trim($this->grid->getOwner()->renderPartial("//lemonway/transaction/_grid_column", ["column" => "receiver_user_id", "model" => $data]))',
                  ],
                  [
                    'name' => 'transaction_type',
                    'value' => 'trim($this->grid->getOwner()->renderPartial("//lemonway/transaction/_grid_column", ["column" => "transaction_type", "model" => $data]))',
                    'filter' => $vec_transaction_type_labels,
                  ],
                  [
                    'name' => 'total_amount',
                    'value' => 'trim($this->grid->getOwner()->renderPartial("//lemonway/transaction/_grid_column", ["column" => "total_amount", "model" => $data]))',
                  ],
                  [
                    'name' => 'status_type',
                    'value' => 'trim($this->grid->getOwner()->renderPartial("//lemonway/transaction/_grid_column", ["column" => "status_type", "model" => $data]))',
                    'filter' => $vec_status_type_labels,
                  ],
                  // [
                  //   'name' => 'comment',
                  //   'value' => 'trim($this->grid->getOwner()->renderPartial("//lemonway/transaction/_grid_column", ["column" => "comment", "model" => $data]))',
                  // ],
                  [
                    'class' => 'dz.grid.ButtonColumn',
                    'header' => Yii::t('app', ''),
                    'template' => '{lemonway}',
                    'clearButton' => true,
                    'viewButton' => false,
                    'updateButton' => false,
                    'deleteButton' => false,
                    'buttons' => [
                      'lemonway' => [
                        'label' => Yii::t('app', 'Dashboard Lemonway'),
                        'icon' => 'fa-external-link',
                        'url' => '$data->url()',
                        'options' => [
                          'target' => '_blank'
                        ]
                      ]
                    ],
                  ]
                ];

                $this->widget('dz.grid.GridView', [
                  'id'                => 'lemonway-transaction-grid',
                  'dataProvider'      => $lemonway_transaction_model->search(),
                  'filter'            => $lemonway_transaction_model,
                  'emptyText'         => Yii::t('lemonway', 'No transactions have been found'),
                  'enableHistory'     => false,
                  'loadModal'         => true,
                  'enableSorting'     => false,
                  'enableSelect2'     => false,
                  'type'              => ['striped', 'hover'],
                  'beforeAjaxUpdate'  => 'js:function(id, options){ $("html, body").animate({scrollTop: 0}, 100); }',
                  'afterAjaxUpdate'   => 'js:function() { $.lemonwayTransactionGridUpdate(); }',
                  'columns'           => $vec_columns
                ]);
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div><!-- .page-content -->
</div><!-- .page-main -->