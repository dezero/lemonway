<?php
/*
|--------------------------------------------------------------------------
| LemonwayTransaction list page for a LemonwayAccount model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $user_model: User model
|  - $lemonway_account_model: LemonwayAccount model
|  - $lemonway_transaction_model: LemonwayTransaction model
|
*/
  use dz\helpers\Url;

  // Page title
  $this->pageTitle = $lemonway_account_model->title();
?>
<?php
  // Header title
  $this->renderPartial('//lemonway/account/_header_title', [
    'lemonway_account_model'  => $lemonway_account_model
  ]);
?>
<div class="page-content container-fluid">
  <?php
    // Header menu
    $this->renderPartial('//lemonway/account/_header_menu', [
      'lemonway_account_model' => $lemonway_account_model
    ]);
  ?>
  <div class="row">
    <div class="col-lg-12">
      <?php
        /*
        |--------------------------------------------------------------------------
        | ACCOUNT INFORMATION
        |--------------------------------------------------------------------------
        */

        // Account summary
        $this->renderPartial('//lemonway/account/_view_summary', [
          'lemonway_account_model'   => $lemonway_account_model
        ]);
      ?>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="panel">
        <div class="panel-body container-fluid">
          <?php
            /*
            |----------------------------------------------------------------------------------------
            | GridView widget
            |----------------------------------------------------------------------------------------
            */
              $vec_columns = [
                [
                  'header' => '#',
                  'name' => 'lemonway_id',
                  'value' => 'trim($this->grid->getOwner()->renderPartial("//lemonway/transaction/_grid_column", ["column" => "lemonway_id", "model" => $data]))',
                ],
                [
                  'name' => 'transaction_date',
                  'value' => 'trim($this->grid->getOwner()->renderPartial("//lemonway/transaction/_grid_column", ["column" => "transaction_date", "model" => $data]))',
                ],
                [
                  'name' => 'sender_user_id',
                  'value' => 'trim($this->grid->getOwner()->renderPartial("//lemonway/transaction/_grid_column", ["column" => "sender_user_id", "model" => $data]))',
                ],
                [
                  'name' => 'receiver_user_id',
                  'value' => 'trim($this->grid->getOwner()->renderPartial("//lemonway/transaction/_grid_column", ["column" => "receiver_user_id", "model" => $data]))',
                ],
                [
                  'name' => 'transaction_type',
                  'value' => 'trim($this->grid->getOwner()->renderPartial("//lemonway/transaction/_grid_column", ["column" => "transaction_type", "model" => $data]))',
                ],
                [
                  'name' => 'total_amount',
                  'value' => 'trim($this->grid->getOwner()->renderPartial("//lemonway/transaction/_grid_column", ["column" => "total_amount", "model" => $data]))',
                ],
                [
                  'name' => 'status_type',
                  'value' => 'trim($this->grid->getOwner()->renderPartial("//lemonway/transaction/_grid_column", ["column" => "status_type", "model" => $data]))',
                ],
                'last_sync_date',
                [
                  'class' => 'dz.grid.ButtonColumn',
                  'header' => Yii::t('app', 'ACTION'),
                  'template' => '{lemonway}',
                  'clearButton' => false,
                  'viewButton' => false,
                  'updateButton' => false,
                  'deleteButton' => false,
                  'buttons' => [
                    'lemonway' => [
                      'label' => Yii::t('app', 'Dashboard Lemonway'),
                      'icon' => 'fa-external-link',
                      'url' => '$data->url()',
                      'options' => [
                        'target' => '_blank'
                      ]
                    ]
                  ],
                ]
              ];

              $this->widget('dz.grid.GridView', [
                'id'                => 'lemonway-document-grid',
                'dataProvider'      => $lemonway_transaction_model->search(),
                // 'filter'            => $lemonway_transaction_model,
                'emptyText'         => Yii::t('lemonway', 'No transactions have been found'),
                'enableHistory'     => false,
                'loadModal'         => true,
                'enableSorting'     => false,
                'enableSelect2'     => false,
                'type'              => ['striped', 'hover'],
                'beforeAjaxUpdate'  => 'js:function(id, options){ $("html, body").animate({scrollTop: 0}, 100); }',
                // 'afterAjaxUpdate'   => 'js:function() { $.lemonwayTransactionGridUpdate(); }',
                'columns'           => $vec_columns
              ]);
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div><!-- .page-content -->